# T_nsi_23





* Adresse mail  : cecile.ghesquiere@ac-lille.fr 
* [Lien vers le réseau du lycée](https://194.167.100.29:8443/owncloud/index.php) : mettre vos identifiants comme au lycée

* Lien vers la banque des épreuves pratiques 2024 : [ici](https://cyclades.education.gouv.fr/delos/public/listPublicECE)

* Vers la préparation du [grand oral](./grand oral/go.md)  
* passage oraux blancs : ![](./img/GO_blanc.png) 

<table class="tftable" border="1">
<tr><th>chapitre</th><th>programme</th><th>déroulé</th></tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-008.gif"></td>
<td style="color:#e06666" > algorithmique </span></td>
<td> <b> Programmation dynamique</b> </br>
<ul>
    <li> Cours  <a href ="./Chapitres/C17_paradigmes_dynamique/Cours/C17_programmation_dynamique.md"> md </a>  </li>
    <li> Chemins dans un tableau <a href ="./Chapitres/C17_paradigmes_dynamique/TP/Nb_chemins.zip"> à télécharger </a></li>
    <li> Découpes dans une barre <a href ="./Chapitres/C17_paradigmes_dynamique/TP/Découpe de barres_a_comp.ipynb"> à télécharger </a></li>
    <li> Alignement de séquences <a href ="./Chapitres/C17_paradigmes_dynamique/TP/Alignement de séquences_tp.ipynb"> à télécharger </a></li>
</ul>
</td>
</tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-007.gif"></td>
<td style="color:#e06666" > algorithmique </span></td>
<td> <b> Recherche Textuelle</b> </br>
<ul>
    <li> TP découverte de la recherche textuelle à télécharger et dézipper <a href ="./Chapitres/C16_recherche_textuelle/recherche_textuelle_decouverte.zip"> à télécharger et dézipper </a> </li>
    <li>Cours  <a href ="./Chapitres/C16_recherche_textuelle/C16_cours/C16_cours.md"> md </a> et  <a href ="./Chapitres/C16_recherche_textuelle/C16_cours/algorithme_Boyer_Moore.pdf"> diaporama </a> </li>
    <li> Exercices <a href ="./Chapitres/C16_recherche_textuelle/exos/C16_exos.md"> md </a></li>
</ul>
</td>
</tr>

<tr>
<td><img src="./img/35426.gif"> </td>
<td> DS 9 Crypto</td>
<td>  <a href = "./Evaluations/DS9 crypto/DS9.pdf"> Sujet </a> et le  <a href = "./Evaluations/DS9 crypto/correction_DS9.pdf"> Corrigé </a> </td>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-006.gif"></td>

<td style="color:#e06666" > Architecture matérielle </span></td>

<td> <b> Sécurité des communications</b> </br>
 <ul>
 <li><a href ="./Chapitres/C15_crypto/C15_securisation_communication.pdf"> Cours</a> </li>
 <li> <a href ="./Chapitres/C15_crypto/cryptage_sym_eleve.7z"> Cryptage symétrique </a> </li>
 <li> <a href ="./Chapitres/C15_crypto/TP_RSA_eleve.7z"> RSA </a> </li>
 <li> <a href ="./Chapitres/C15_crypto/integrite_signature/Integrite_signature_num_1.ipynb"> Intégrité d'un fichier et signature </a> </li>

 </ul>
</td>
</tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-005.gif"></td>
<td style="color:#e06666" > algorithmique </td>
<td> <b> Calculabilité Décidabilité</b> </br>
 <a href ="./Chapitres/C14_calculabilite"> Cours</a> 
</td>
</tr>
<tr>
<td><img src="./img/35426.gif"> </td>
<td> DS 8</td>
<td>  <a href = "./Evaluations/DS8/DS8.pdf"> Sujet </a> et le  <a href = "./Evaluations/DS8/correction_DS8.pdf"> Corrigé </a> </td>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-004.gif"></td>
<td style="color:#e06666" >Structure de données ; algorithmique </span></td>
<td> <b> Les graphes</b> </br>
<ul>
<li> Cours partie 1 <a href ="./Chapitres/C13_graphes/C13_1_cours.md"> md </a>  ou <a href ="./Chapitres/C13_graphes/C13_1_cours.pdf"> pdf </a>  </li>
<li> TP partie 1 :  <a href ="./Chapitres/C13_graphes/C13_1_TP_el.md"> md </a> ou <a href ="./Chapitres/C13_graphes/C13_1_TP_el.pdf"> pdf </a>  </li>
<li> Cours : partie 2  <a href ="./Chapitres/C13_graphes/C13_2_cours.md"> md </a>  ou <a href ="./Chapitres/C13_graphes/C13_2_cours.pdf"> pdf </a>  </li>
<li> Lien vers animations pour comprendre les parcours en profondeur et en largeur <a href ="http://fred.boissac.free.fr/AnimsJS/DariushGraphes/index.html"> lien (M Boissac) </a>
<li> Parcours de graphes  <a href ="./Chapitres/C13_graphes/Parcours_de_graphes.pdf"> pdf </a>  </li>
<li> Ours en cage : <a href ="./Chapitres/C13_graphes/ours_en_cage/ours_en_cage_el.md" > le sujet </a> et <a href ="./Chapitres/C13_graphes/ours_en_cage/ours_en_cage_el.zip" > le dossier à télécharger </li>
<li> Détection de cycle <a href ="./Chapitres/C13_graphes/detection _cycle.zip">à télécharger et dézipper </a></li>
<li> Routage : parcours de cycles <a href ="./Chapitres/C13_graphes/routage_graphe.zip">à télécharger et dézipper  </a></li>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-003.gif"> </td>
<td style="color:#8973c1" >Architecture matérielle</span></td>
<td><ul>
<li> <b>Microcontrôleur et SoC </b> <a href ="./Chapitres/C6_microcontroleur_SoC/C6_microcontroleur_SoCs.md"> md </a>  </li>
<li> TP  <a href ="./Chapitres/C6_microcontroleur_SoC/C6_TP_el.md"> md </a>  </li>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"></td>
<td style="color:#17657D" >Langages et  programmation </span></td>
<td> <b>Mise au point et optimisation </b> </br>
<ul>
<li> Cours  <a href ="./Chapitres/C11_mise_au point/cours/C11_mise_au_point.md"> md </a>  ou <a href ="./Chapitres/cours/C11_mise_au point.pdf"> pdf </a>  </li>
<li> TP :  <a href ="./Chapitres/C11_mise_au point/tp/C11_TP.md"> md </a> ou <a href ="./Chapitres/C11_mise_au point/tp/C11_TP.pdf"> pdf </a>  </li> 
</td>
</tr>
<td><img src="./img/35419.gif"> </td>
<td> DS 7</td>
<td>  <a href = "./Evaluations/DS7/DS7_div_reg.pdf"> Sujet </a> et le  <a href = "./Evaluations/DS7/correction_DS7.pdf"> Corrigé </a> </td>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"></td>
<td style="color:#17657D" >Langages et  programmation </span></td>
<td> <b>Mise au point et optimisation </b> </br>
<ul>
<li> Cours  <a href ="./Chapitres/C11_mise_au point/cours/C11_mise_au_point.md"> md </a>  ou <a href ="./Chapitres/cours/C11_mise_au point.pdf"> pdf </a>  </li>
<li> TP :  <a href ="./Chapitres/C11_mise_au point/tp/C11_TP.md"> md </a> ou <a href ="./Chapitres/C11_mise_au point/tp/C11_TP.pdf"> pdf </a>  </li> 
</td>
</tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-001.gif"> </td>
<td style="color:#17657D" >Algorithmique</span></td>

<td> <b>Diviser pour régner </b> </br>
<ul>
<li> Cours  <a href ="./Chapitres/C10_div_reg/C10_diviser_regner.md"> md </a>  ou <a href ="./Chapitres/C10_div_reg/C10_diviser_regner.pdf"> pdf </a>  </li>
<li> TP :  <a href ="./Chapitres/C10_div_reg/C10_TP_div_reg.md"> md </a> ou <a href ="./Chapitres/C10_div_reg/C10_TP_div_reg.pdf"> pdf </a>  </li> 
<li>  <a href = "./Chapitres/C10_div_reg/exo_bac">Exercices de bac </a> </li>
</td>
</tr>
<td><img src="./img/35426.gif"> </td>
<td> DS 6</td>
<td>  <a href = "./Evaluations/DS6/DS6.pdf"> Sujet </a> et le  <a href = "./Evaluations/DS6/correctionDS6.pdf"> Corrigé </a> </td>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-010.gif"></td>
<td style="color:#8973c1" >Architecture matérielle</span></td>
<td>
 <b>Routage   <img src="./img/noel_sa02.gif " style="width: 60px; height: auto;"></b> </br>
<ul><li> Cours  <a href ="./Chapitres/C9_routage/C9_protocole_RIP.md"> md </a>  ou <a href ="./Chapitres/C9_routage/C9_protocole_RIP.pdf"> pdf </a>  </li>
<li> Exercices :  <a href ="./Chapitres/C9_routage/Exercices.md"> md </a> ; vous pouvez télécharger Filius ensuivant ce [lien](https://ent2d.ac-bordeaux.fr/disciplines/sti-college/2019/09/25/filius-un-logiciel-de-simulation-de-reseau-simple-et-accessible/)</li>
<li> TP :  <a href ="./Chapitres/C9_routage/TP_routage_filius/TP_routage.md"> md </a> </li>
<li> différences hub switch routeur : lire <a href= "https://community.fs.com/fr/article/whats-the-difference-hub-vs-switch-vs-router.html"> ici </a>
</td>
</tr>
<tr>
<td><img src="./img/35419.gif"> </td>
<td> DS Pile processus </td>
<td>  <a href = "./Evaluations/DS_pile_processus/DS5.pdf"> Sujet </a> et le  <a href = "./Evaluations/DS_arbre/DS5_correction.pdf"> Corrigé </a> <a href = "./Evaluations/DS_pile_processus/DS5_rat.pdf"> Sujet bis </a> et le  <a href = "./Evaluations/DS_arbre/DS5_rat_correction.pdf"> Corrigé bis </a> </td>
</td>
</tr>
<tr>

<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-009.gif"> </td>
<td style="color: #e06666">Structure de données</span></td>
<td><ul>
<li> <b>Pile File </b> <a href ="./Chapitres/C8_pile_file/C8_pile_file.md"> md </a>  ou <a href ="./Chapitres/C8_pile_file/C8_pile_file.pdf"> pdf </a></li>
<li> TP  <a href ="./Chapitres/C8_pile_file/C8_TP_eleve.md"> md </a>  ou <a href ="./Chapitres/C8_pile_file/exos/C8_TP_eleve.pdf"> pdf </a> </li>
<li style="color:#6aa84f"> sujet Bac 2021 Amérique du Nord  <a href ="./Chapitres/C8_pile_file/exos/sujet_bac_amerique2021_ex5.pdf"> : sujet </a>  </li>

<li> <img src="./img/ordi.gif" style="width: 60px; height: auto;"> EP : on s'entraîne sur les files <a href="https://e-nsi.forge.aeif.fr/pratique/N2/770-file_avec_liste/sujet/"> ici </a> </li>
<li> <img src="./img/ordi.gif" style="width: 60px; height: auto;"> EP : on s'entraîne sur les piles <a href="https://e-nsi.forge.aeif.fr/pratique/N2/770-filtre_pile/sujet/"> ici </a> </li>

</td>
</tr>
<tr>
<td><img src="./img/35419.gif"> </td>
<td> DS 3 </td>
<td>  <a href = "./Evaluations/DS_arbre/DS3.pdf"> Sujet </a> et le  <a href = "./Evaluations/DS_arbre/correction.pdf"> Corrigé </a> </td>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-008.gif"> </td>
<td style="color:#e06666" ><p>Structure de données</p>  <p> Algorithmique </p> </td>
<td><ul>
<li> <b>Les arbres </b>Cours partie 1 <a href ="./Chapitres/C7_arbres/cours/C7_1.md"> md </a>  

<li> TP 1 :  <a href ="./Chapitres/C7_arbres/TP/C7_TP1.md"> md </a> 
<li> Cours partie 2 <a href ="./Chapitres/C7_arbres/cours/C7_2_ABR.md"> md </a>  ou <a href ="./Chapitres/C7_arbres/cours/C7_2_ABR.pdf"> pdf </a>  </li>
<li> TP 2 :  <a href ="./Chapitres/C7_arbres/TP/C7_TP2.md"> md </a> ou <a href ="./Chapitres/C7_arbres/TP/C7_TP2_.pdf"> pdf </a>  </li>
<li> <img src="./img/ordi.gif" style="width: 60px; height: auto;"> EP : on s'entraîne sur les arbres <a href="https://e-nsi.forge.aeif.fr/pratique/N2/800-arbre_bin/sujet/"> ici </a> </li>
<li> <img src="./img/ordi.gif"> EP et sur les ABR <a href="https://e-nsi.forge.aeif.fr/pratique/N2/876-ABR/sujet/" > ici </a> </li>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-007.gif"> </td>
<td style="color:#8973c1" >Architecture matérielle</span></td>
<td><ul>
<li> <b>Processus Ressources </b> <a href ="./Chapitres/C6_processus/6_processus_ressource.md"> md </a>  ou <a href ="./Chapitres/C6_processus/6_processus_ressource_cours.pdf"> pdf </a></li>
<li> TP 1  <a href ="./Chapitres/C6_processus/TP/TP_C6_processus_def_el.md"> md </a>  ou <a href ="./Chapitres/C6_processus/TP/TP_C6_processus_def_el.pdf"> pdf </a> </li>
<li> TP 2  <a href ="./Chapitres/C6_processus/TP/TP2_C6_interblocage.md"> md </a>  ou <a href ="./Chapitres/C6_processus/TP/TP2_C6_interblocage.pdf"> pdf </a> </li>
<li >   <a href ="./Chapitres/C6_processus/metro_2021_ex2_2.pdf">sujet Bac 2021 sujet candidats libres </a> </li>
<li > <a href = "./Chapitres/C6_processus/2022_AS_J1_4.pdf"> sujet Amérique du Sud 2022 ex 4 </a>  </li>
 </td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-006.gif"> </td>
<td style="color: #e06666" >Structure de données</span></td>
<td><ul>
<li> <b>Liste chainée </b><a href ="./Chapitres/C5_liste_chainee/C5_liste_chainee.md"> md </a>  ou <a href ="./Chapitres/C5_liste_chainee/C5_liste_chainee.pdf"> pdf </a></li>
<li> TP  <a href ="./Chapitres/C5_liste_chainee/exos/C5_TP.md"> md </a>  ou <a href ="./Chapitres/C5_liste_chainee/exos/C5_TP.pdf"> pdf </a> </li>

</td>
</tr>

<td><img src="./img/35426.gif"> </td>
<td> DS 2</td>
<td>  <a href = "./Evaluations/DS2/DS2.pdf"> Sujet </a> et le  <a href = "./Evaluations/DS2/Correction DS2.pdf"> Corrigé </a> </td>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-005.gif"> </td>
<td style="color:#09fa14" >Base de données</span></td>
<td>
 <b>Base de données relationnelles </b> </br>
<ul><li> Cours partie 1 <a href ="./Chapitres/C4_BDD/cours/C4_1.md"> md </a>  ou <a href ="./Chapitres/C4_BDD/cours/C4_1.pdf"> pdf </a>  </li>
<li> TP 1A :  <a href ="./Chapitres/C4_BDD/TP/C4_1A_TP.md"> md </a> ou <a href ="./Chapitres/C4_BDD/TP/C4_1A_TP.pdf"> pdf </a>  </li>
<li> TP 1B :  <a href ="./Chapitres/C4_BDD/TP/C4_1B_TP.md"> md </a> ou <a href ="./Chapitres/C4_BDD/TP/C4_1B_TP.pdf"> pdf </a>  </li>
<li> Cours partie 2 <a href ="./Chapitres/C4_BDD/cours/C4_2.md"> md </a>  ou <a href ="./Chapitres/C4_BDD/cours/C4_2.pdf"> pdf </a> <a href="./Chapitres/C4_BDD/cours/mementoSQL_LE_COUPANEC_JACQUES.pdf"> Memento SQL </a> </li>
<li> TP 2 :  <a href ="./Chapitres/C4_BDD/TP/C4_2_TP.md"> md </a> ou <a href ="./Chapitres/C4_BDD/TP/C4_2_TP.pdf"> pdf </a> </li>
<li> <a href = './Chapitres/C4_BDD/exos_bac'> Exercices de bac </a>
<li> <img src="./img/ordi.gif" style="width: 40px; height: auto;"><a href = 'https://e-nsi.forge.aeif.fr/2-exercices/#langage-sql'> Pour s'entrainer en ligne </a>
</td>
</tr>
<tr>
<td><img src="./img/35419.gif"> </td>
<td> DS 1 </td>
<td>  <a href = "./Evaluations/DS1/DS1.pdf"> Sujet </a> et le  <a href = "./Evaluations/DS1/DS1_correction.pdf"> Corrigé </a> </td>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-004.gif"> </td>
<td style="color: #e06666" >Structure de données</span></td>
<td><ul>
<li> <b>La programation orientée objet </b><a href ="./Chapitres/C3_POO/C3_PPO_cours.md"> md </a>  ou <a href ="./Chapitres/C3_PPO/C3_poo_cours.pdf"> pdf </a></li>
<li> TP POO <a href ="./Chapitres/C3_POO/tp/C3_TP_POO_el.md"> md </a>  ou <a href ="./Chapitres/C3_POO/tp/C3_poo_tp.pdf"> pdf </a> </li>
<li> Sujet <a href = '/Chapitres/C3_POO/Bac_Sujet2_Exercice_1.pdf'> Bac sujet 2 </a> </li>
<li> Sujet <a href = './Chapitres/C3_POO/23_ME_J1_poo.pdf'> Métropole 2023 J1 exercice 3 </a> </li>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-003.gif"> </td>
<td style="color:#17657D" >Langages, programmation et algorithmique</span></td>
<td><ul>
<li><b>La récursivité </b><a href ="./Chapitres/C2/C2_Recursivite_cours.md"> md </a> </li>
<li> TP Récursivité <a href ="./Chapitres/C2/tp/2_TP_recursivite_el.md"> md </a>  </li>
<li> TP Notion de mathématiques <a href ="./Chapitres/C2/tp/2_recurrence_math_el.md"> md </a>  </li>
<li>  <a href = "./Chapitres/C2/Ex2_zero_2021.pdf"> Sujet de bac 0 2021 </a> </li>
<li>  <a href = "./Chapitres/C2/polynesie_2022_ex1.pdf"> Exercice 1 Polynésie 2022 </a> </li>
</td>
</tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"> </td>
<td style="color:#17657D" >Langages, programmation et algorithmique</span></td>
<td><ul>
<li> <b>Modularité Interface </b><a href ="./Chapitres/C1/C1_modularite.md"> md </a> </li>
<li> TP 1 modules<a href ="./Chapitres/C1/TP/TP_modularité_eleve.md"> md </a>  </li>
<li> TP 2 interface <a href ="./Chapitres/C1/TP/TP2_interface/C1_TP_interface.md"> md </a>  </li>
</td>
</tr>

<tr><td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-001.gif"></td><td>révision</td><td>boucle bornée, non bornée, comparaisons, listes, tuples, dictionnaires,.....
<a href ="./Chapitres/revisions.md"> lien vers la feuille de travail </a>  </a>  <a href ="./Chapitres/revis.py"> lien vers corrigé </a>   
</td>
</tr>

<tr><td>1_NSI</td><td></td><td><a href="https://framagit.org/CecGhesq/1_nsi_22"> git_premiere_nsi </a></td></tr>
</table>


* Exposés pour le 11/12 :   
3 minutes maximum : pas d'usage du tableau ; Support sur une feuille A4 possible.
('Jordan', 'logiciel libre propriétaire'),  
 ('Oscar', "rôle d'un système d'exploitation"),  
  ('Dorian', 'chemin absolu relatif'),  
   ('Alexandre', 'commandes de base shell UNIX'),  
    ('Florian', 'droits utilisateurs UNIX'),  
     ('Camille', 'portes logiques'),  
      ('Amaury', 'conversions decimal binaire'),  
       ('Thomas', 'architecture von Neuman'),  
        ('Valentin', 'protocole UDP TCP')  

* Partie pratique ARBRES BINAIRES : 7/12:
Veuillez cliquer sur votre prénom afin d'atteindre votre sujet :

||||||||
|---|---|---|---|---|---|---|
|[Justine](./EP/DS4_A.md)|[Camille](./EP/DS4_B.md) |[Florian](./EP/DS4_A.md)|-|[Dorian](./EP/DS4_B.md)|[Léni](./EP/DS4_A.md)|[Léo](./EP/DS4_B.md)|
|[Amaury](./EP/DS4_A.md)|[Luc](./EP/DS4_B.md)|[Valentin](./EP/DS4_A.md)|-|[Jordan](./EP/DS4_B.md)|[Oscar](./EP/DS4_A.md)|[Benjamin](./EP/DS4_A.md)|
|[Alexandre](./EP/DS4_A.md)|[Raphaël](./EP/DS4_B.md)|[Jérémy](./EP/DS4_A.md)|-|[Noa](./EP/DS4_B.md)|[Ethann](./EP/DS4_B.md)|[Mathis](./EP/DS4_B.md)|
|-|-|-|-|-|-|[Thomas](./EP/DS4_B.md)|
|-|-|-|-|-|[Taha](./EP/DS4_A.md)|[Lilian](./EP/DS4_B.md)|


Dépôt de fichiers de __terminale NSI__ Lycée M de Flandre à Gondecourt (C. Ghesquiere)

* [11](./EP/ece_nsi/SUJET_11) :Jordan  
* [12](./EP/ece_nsi/SUJET_12) : Lilian  
* [13](./EP/ece_nsi/SUJET_13) : Camille, Justine, Valentin  
* [14](./EP/ece_nsi/SUJET_14) : Dorian, Léo , Oscar  
* [15](./EP/ece_nsi/SUJET_15) : Florian  
* [16](./EP/ece_nsi/SUJET_16) : Ethann , Jérémy, Luc, Thomas 
* [17](./EP/ece_nsi/SUJET_17) : Taha  
* [18](./EP/ece_nsi/SUJET_18) : Léni  
* [19](./EP/ece_nsi/SUJET_19) : Alexandre, Mathis  
* [20](./EP/ece_nsi/SUJET_20) : Amaury, Raphaël  
* [21](./EP/ece_nsi/SUJET_21) : Benjamin, Noa


<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Les  documents sont mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licence Creative Commons Attribution 4.0 International</a>.
