![](https://www.gifsanimes.com/data/media/1124/course-de-velo-image-animee-0002.gif)

👁️‍🗨️Informations générales : [ici](2023_grand_oral_0.pdf)  
👁️‍🗨️Les modalités :[là](infog_epreuve_orale_terminale_grandoral_v7.pdf)  
✍ Les textes officiels : [ici](voie-g_go_version-consolidee-2024_1.pdf)  


__Lien vers le tableur pour renseigner votre sujet__ : [ici](https://lite.framacalc.org/ltjocznhhw-a73m)


A faire pour le 6 mai : [preparation orale](https://www.quiziniere.com/diffusions/6KX36M)