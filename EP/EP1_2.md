Réaliser les deux exercices suivants dans un seul et même fichier.  

Déposer ensuite votre fichier sur __pronote__.

On considère des tables (des tableaux de dictionnaires) qui contiennent des enregistrements relatifs à des animaux hébergés dans un refuge. Les attributs des enregistrements sont 'nom', 'espece', 'age', 'enclos'. Voici un exemple d'une telle table :
```python
animaux = [ {'nom':'Medor', 'espece':'chien', 'age':5, 'enclos':2},
{'nom':'Titine', 'espece':'chat', 'age':2, 'enclos':5},
{'nom':'Tom', 'espece':'chat', 'age':7, 'enclos':4},
{'nom':'Belle', 'espece':'chien', 'age':6, 'enclos':3},
{'nom':'Mirza', 'espece':'chat', 'age':6, 'enclos':5}]
```
Programmer une fonction selection_enclos qui :
- prend en paramètres :
    - une table table_animaux contenant des enregistrements relatifs à des animaux (comme dans l'exemple ci-dessus),
    - un numéro d'enclos num_enclos ;
- renvoie une table contenant les enregistrements de table_animaux dont l'attribut 'enclos' est num_enclos.

Exemples avec la table animaux ci-dessus :
```python
>>> selection_enclos(animaux, 5)
[{'nom':'Titine', 'espece':'chat', 'age':2, 'enclos':5},
{'nom':'Mirza', 'espece':'chat', 'age':6, 'enclos':5}]
>>> selection_enclos(animaux, 2)
[{'nom':'Medor', 'espece':'chien', 'age':5, 'enclos':2}]
>>> selection_enclos(animaux, 7)
[]
```

## Fibonacci
On s’intéresse à la suite d’entiers définie par :  
- les deux premiers termes sont égaux à 1,  
- ensuite, chaque terme est obtenu en faisant la somme des deux termes qui le précèdent.  
En mathématiques, on le formule ainsi :  
u1 = 1, u2 = 1 et, pour tout entier naturel non nul n, un+2 = un+1 + un.
Cette suite est connue sous le nom de suite de Fibonacci.
Écrire en Python une fonction fibonacci qui prend en paramètre un entier n supposé strictement positif et qui renvoie le terme d’indice n de cette suite.
Exemples :
```python
>>> fibonacci(1)
1
>>> fibonacci(2)
1
>>> fibonacci(25)
75025
>>> fibonacci(45)
1134903170
```