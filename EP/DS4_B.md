# Partie pratique sur les arbres

A réaliser dans un IDLE ( Thonny ou VScode) dans un __seul et unique__ fichier:

## Exercice 1 (sur 12 points)
Soit la classe Noeud_1 ci-dessous :
```python
class Noeud_1:
    '''
    classe implémentant un noeud d'arbre binaire
    '''

    def __init__(self, g, v, d):
        '''
        un objet Noeud possède 3 attributs :
        - gauche : le sous-arbre gauche,
        - valeur : la valeur de l'étiquette,
        - droit : le sous-arbre droit.
        '''
        self.gauche = g
        self.valeur = v
        self.droit = d
```
1. Copier la classe `Noeud_1` dans l'application puis écrire l'instruction à l'aide de la class Noeud, pour créer l'arbre `arbre1` dont la représentation est la suivante :  

![](tree-02.svg)

2. Ecrire la fonction `parcours_prefixe(ab)` permettant d'afficher les valeurs de l'arbre par un parcours prefixe.
La tester sur l'arbre précédent.

3. On appelle hauteur h d'un arbre le plus grand nombre de noeuds rencontrés en descendant de la racine jusqu'à une feuille.  
Ecrire la fonction `hauteur(ab)`  renvoyant la hauteur de l'arbre binaire.
La tester sur l'arbre précédent.

## Exercice 2  ( sur 8 pts)
Une expression arithmétique ne comportant que les quatre opérations +, −,×,÷  peut 
être représentée sous forme d’arbre binaire. Les nœuds internes sont des opérateurs et les feuilles sont des nombres. Dans un tel arbre, la disposition des nœuds joue le rôle des parenthèses que nous connaissons bien.   

![](arbre_ope.png)  

En parcourant en profondeur infixe l’arbre binaire ci-dessus, 
on retrouve l’expression notée habituellement : 

 ( 3 × (8 + 7)) − (2 + 1). 

La classe Noeud ci-après permet d’implémenter une structure d’arbre binaire.  

Compléter la fonction récursive expression_infixe qui prend en paramètre un objet de la classe Noeud et qui renvoie l’expression arithmétique représentée par l’arbre 
binaire passé en paramètre, sous forme d’une chaîne de caractères contenant des parenthèses. 

Résultat attendu avec l’arbre ci-dessus : 
>>> e = Noeud(Noeud(Noeud(None, 3, None),  
       '*', Noeud(Noeud(None, 8, None), '+', Noeud(None, 7, None))),  
       '-', Noeud(Noeud(None, 2, None), '+', Noeud(None, 1, None))) 

>>> expression_infixe(e) 
'((3*(8+7))-(2+1))' 

 2 / 3 

 
 
 
 
 
 
class Noeud: 
    ''' 
    classe implémentant un noeud d'arbre binaire 
    ''' 

    def __init__(self, g, v, d): 
        ''' 
        un objet Noeud possède 3 attributs : 
        - gauche : le sous-arbre gauche, 
        - valeur : la valeur de l'étiquette, 
        - droit : le sous-arbre droit. 
        ''' 
        self.gauche = g 
        self.valeur = v 
        self.droit = d 

    def __str__(self): 
        ''' 
        renvoie la représentation du noeud en chaine de 
caractères 
        ''' 
        return str(self.valeur) 

    def est_une_feuille(self): 
        ''' 
        renvoie True si et seulement si le noeud est une feuille 
        ''' 
        return self.gauche is None and self.droit is None 

def expression_infixe(e): 
    s = ... 
    if e.gauche is not None: 
        s = '(' + s + expression_infixe(...) 
    s = s + ... 
    if ... is not None: 
        s = s + ... + ... 
    return s 

__DEPOSER VOTRE CODE SUR PRONOTE__