# Partie pratique sur les arbres

A réaliser dans un IDLE ( Thonny ou VScode) dans un __seul et unique__ fichier:

## Exercice 1 (sur 12 points)
Soit la classe Noeud_1 ci-dessous :
```python
class Noeud_1:
    '''
    classe implémentant un noeud d'arbre binaire
    '''

    def __init__(self, g, v, d):
        '''
        un objet Noeud possède 3 attributs :
        - gauche : le sous-arbre gauche,
        - valeur : la valeur de l'étiquette,
        - droit : le sous-arbre droit.
        '''
        self.gauche = g
        self.valeur = v
        self.droit = d
```
1. Copier la classe `Noeud_1` dans l'application puis écrire l'instruction à l'aide de la class Noeud, pour créer l'arbre `arbre1` dont la représentation est la suivante :  
![](tree-01.svg)

2. Ecrire la fonction `parcours_infixe(ab)` permettant d'afficher les valeurs de l'arbre par un parcours infixe.
La tester sur l'arbre précédent.

3. Ecrire la fonction `taille(ab)`  donnant le nombre de noeuds de l'arbre binaire.
La tester sur l'arbre précédent.

## Exercice 2  ( sur 8 pts)
La classe Noeud ci-dessous permet d'implémenter une structure d'arbre binaire de 
recherche (ABR). On considère que la clé d’un nœud est un entier et qu’elle est 
unique dans l’ABR.  

```python
class Noeud: 
    def __init__(self, valeur): 
        '''Méthode constructeur pour la classe Noeud. 
        Paramètre d'entrée : valeur (int)''' 
        self.valeur = valeur 
        self.gauche = None 
        self.droit = None 

    def getValeur(self): 
        '''Méthode accesseur pour obtenir la valeur du noeud 
        Aucun paramètre en entrée''' 
        return self.valeur 

    def droitExiste(self): 
        '''Méthode renvoyant True si le sous-arbre droit est non vide 
        Aucun paramètre en entrée''' 
        return (self.droit is not None) 

    def gaucheExiste(self): 
        '''Méthode renvoyant True si le sous-arbre gauche est non 
vide 
        Aucun paramètre en entrée''' 
        return (self.gauche is not None) 

    def inserer(self, cle): 
        '''Méthode d'insertion de clé dans un arbre binaire de recherche 
        Paramètre d'entrée : cle (int)''' 
        if cle < ... : 
            # on insère à gauche 
            if self.gaucheExiste(): 
                # on descend à gauche et on recommence le test initial 
                 ...  
            else: 
                # on crée un fils gauche 
                self.gauche =  ...  
        elif cle >  ... : 
            # on insère à droite 
            if  ... : 
                # on descend à droite et on recommence le test initial 
                 ...  
            else: 
                # on crée un fils droit 
                 ...  = Noeud(cle) 
```

Compléter la fonction récursive `inserer` afin qu'elle permette d’insérer un noeud dans l’arbre binaire de recherche proposé, à l’aide de sa clé.  
Voici un exemple d'utilisation __à placer dans votre code__:
```python
>>> arbre = Noeud(7)
>>>for cle in (3, 9, 1, 6):
arbre.inserer(cle)#
>>> arbre.gauche.getValeur()
3
>>> arbre.droit.getValeur()
9
>>> arbre.gauche.gauche.getValeur()
1
>>> arbre.gauche.droit.getValeur()
6
``` 

__DEPOSER VOTRE CODE SUR PRONOTE__