# Exercices de la première partie de l'épreuve pratique en 2023
## Moyennes
1. 
Dans cet exercice, les nombres sont des entiers ou des flottants.
Écrire une fonction moyenne renvoyant la moyenne pondérée d’une liste non vide, passée en paramètre, de tuples à deux éléments de la forme (valeur, coefficient) où valeur et coefficient sont des nombres positifs ou nuls.
Si la somme des coefficients est nulle, la fonction renvoie None, si la somme des coefficients est non nulle, la fonction renvoie, sous forme de flottant, la moyenne des valeurs affectées de leur coefficient.
Exemples :
```python
>>> moyenne([(8, 2), (12, 0), (13.5, 1), (5, 0.5)])
9.142857142857142
>>> moyenne([(3, 0), (5, 0)])
None
```
2. 
Écrire une fonction `moyenne(liste_notes)` qui renvoie la moyenne pondérée des résultats contenus dans la liste liste_notes, non vide, donnée en paramètre. Cette liste contient des couples (note, coefficient) dans lesquels :
- note est un nombre de type float compris entre 0 et 20 ;
- coefficient est un nombre entier strictement positif.
Ainsi, l’expression moyenne([(15, 2), (9, 1), (12, 3)]) devra renvoyer
12.5 :

$` \frac{15*2 + 9*1 + 12*3}{2 + 1 + 3} `$

3. 
Ecrire une fonction qui prend en paramètre un tableau d'entiers non vide et qui renvoie la moyenne de ces entiers. La fonction est spécifiée ci-après et doit passer les assertions fournies.
```python
def moyenne (tab):
'''
moyenne(list) -> float
Entrée : un tableau non vide d'entiers
Sortie : nombre de type float
Correspondant à la moyenne des valeurs présentes dans le
tableau
'''

assert moyenne([1]) == 1
assert moyenne([1, 2, 3, 4, 5, 6, 7]) == 4
```

4. 
Écrire une fonction `moyenne` qui prend en paramètre un tableau non vide de nombres flottants et qui renvoie la moyenne des valeurs du tableau. Les tableaux seront représentés sous forme de liste Python.
Exemples :
```python
 >>> moyenne([
1.0
>>> moyenne([1.0, 2.0,
2.3333333333333335
```

5. 
Programmer la fonction `moyenne` prenant en paramètre un tableau d'entiers tab (de type list) qui renvoie la moyenne de ses éléments si le tableau est non vide. Proposer une façon de traiter le cas où le tableau passé en paramètre est vide.
Dans cet exercice, on s’interdira d’utiliser la fonction Python sum.
Exemples :
```python
>>> moyenne([5, 3, 8])
5.333333333333333
>>> moyenne([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
5.5
>>> moyenne([]) Comportement différent suivant le traitement proposé.
```


## Multiplication
Programmer la fonction multiplication prenant en paramètres deux nombres entiers
relatifs n1 et n2, et qui renvoie le produit de ces deux nombres.
Les seules opérations autorisées sont l’addition et la soustraction.
Exemples :
```python
>>> multiplication(3, 5)
15
>>> multiplication(-4, -8)
32
>>> multiplication(-2, 6)
-12
>>> multiplication(-2, 0)
0
```

## Maxi mini
1. 
Écrire une fonction indices_maxi qui prend en paramètre une liste tab, non vide, de nombres entiers et renvoie un couple donnant d’une part le plus grand élément de cette liste et d’autre part la liste des indices de la liste tab où apparaît ce plus grand élément.
Exemples :
```python
>>> indices_maxi([1, 5, 6, 9, 1, 2, 3, 7, 9, 8])
(9, [3, 8])
>>> indices_maxi([7])
(7, [0])
```

2. 
Écrire la fonction maxliste, prenant en paramètre un tableau non vide de nombres tab
(de type list) et renvoyant le plus grand élément de ce tableau.
Exemples :
```python
>>> maxliste([98, 12, 104, 23, 131, 9])
131
>>> maxliste([-27, 24, -3, 15])
24
```

3. 
On a relevé les valeurs moyennes annuelles des températures à Paris pour la période allant de 2013 à 2019. Les résultats ont été récupérés sous la forme de deux listes : l’une pour les températures, l’autre pour les années :
```python
t_moy = [14.9, 13.3, 13.1, 12.5, 13.0, 13.6, 13.7]
annees = [2013, 2014, 2015, 2016, 2017, 2018, 2019]
```
Écrire la fonction mini qui prend en paramètres un tableau releve des relevés et un tableau date des dates et qui renvoie la plus petite valeur relevée au cours de la période et l’année correspondante. On suppose que la température minimale est atteinte une seule fois.
Exemple :
```python
>>> mini(t_moy, annees)
(12.5, 2016)
```

4. 
Écrire une fonction `max_et_indice` qui prend en paramètre une liste non vide tab de nombres entiers et qui renvoie la valeur du plus grand élément de cette liste ainsi que l’indice de sa première apparition dans cette liste.
L’utilisation de la fonction native max n’est pas autorisée.
Ne pas oublier d’ajouter au corps de la fonction une documentation et une ou plusieurs assertions pour vérifier les pré-conditions.
Exemples :
```python
>>> max_et_indice([1, 5, 6, 9, 1, 2, 3, 7, 9, 8])
(9, 3)
>>> max_et_indice([-2])
(-2, 0)
>>> max_et_indice([-1, -1, 3, 3, 3])
(3, 2)
>>> max_et_indice([1, 1, 1, 1])
(1, 0)
```

5. 
Écrire une fonction `recherche_min` qui prend en paramètre un tableau, non vide, de nombres non trié tab, et qui renvoie l'indice de la première occurrence du minimum de ce tableau. Les tableaux seront représentés sous forme de listes Python.

Exemples :
```python
>>> recherche_min([5])
0
>>> recherche_min([2, 4, 1])
2
>>> recherche_min([5, 3, 2, 2, 4])
2
```

## Recherche 
1. 
Programmer la fonction `recherche`, prenant en paramètre un tableau non vide tab (de type list) d'entiers et un entier n, et qui renvoie l'indice de la dernière occurrence de l'élément cherché. Si l'élément n'est pas présent, la fonction renvoie la longueur du tableau.
Exemples :
```python
>>> recherche([5, 3], 1)
2
>>> recherche([2, 4], 2)
0
>>> recherche([2, 3, 5, 2, 4], 2)
3
```

2. 
Écrire en langage Python une fonction `recherche` prenant comme paramètres une variable a de type numérique (*float* ou *int*) et un tableau *tab* (de type *list*) et qui renvoie le nombre d'occurrences de *a* dans *tab*.
Exemples d'utilisations de la fonction recherche :
```python
>>> recherche(5, [])
0
>>> recherche(5, [-2, 3, 4, 8])
0
>>> recherche(5, [-2, 3, 1, 5, 3, 7, 4])
1
>>> recherche(5, [-2, 5, 3, 5, 4, 5])
3
```

3. 
Écrire une fonction `recherche` qui prend en paramètres un nombre entier *elt* et un tableau *tab* de nombres entiers, et qui renvoie l’indice de la première occurrence de *elt* dans *tab* si *elt* est dans *tab* et -1 sinon.
Ne pas oublier d’ajouter au corps de la fonction une documentation et une ou plusieurs assertions pour vérifier les pré-conditions.

Exemples :
```python
>>> recherche(1, [2, 3, 4])
-1
>>> recherche(1, [10, 12, 1, 56])
2
>>> recherche(50, [1, 50, 1])
1
>>> recherche(15, [8, 9, 10, 15])
3
>>> recherche(50, [])
-1
>>> recherche(4, [2, 4, 4, 3, 4])
1
```

4. 
Écrire une fonction recherche_indices_classement qui prend en paramètres un entier elt et une liste d’entiers tab, et qui renvoie trois listes :
- la première liste contient les indices des valeurs de la liste tab strictement inférieures à elt ;
- la deuxième liste contient les indices des valeurs de la liste tab égales à elt ;
- la troisième liste contient les indices des valeurs de la liste tab strictement supérieures à elt.
Exemples :
```python
>>> recherche_indices_classement(3, [1, 3, 4, 2, 4, 6, 3, 0])
([0, 3, 7], [1, 6], [2, 4, 5])
>>> recherche_indices_classement(3, [1, 4, 2, 4, 6, 0])
([0, 2, 5], [], [1, 3, 4])
>>>recherche_indices_classement(3, [1, 1, 1, 1])
([0, 1, 2, 3], [], [])
>>> recherche_indices_classement(3, [])
([], [], [])
```

5. Recherche dichotomique
Écrire une fonction `recherche` qui prend en paramètres un tableau tab de nombres entiers triés par ordre croissant et un nombre entier *n*, et qui effectue une recherche dichotomique du nombre entier *n* dans le tableau non vide *tab*.
Cette fonction doit renvoyer un indice correspondant au nombre cherché s’il est dans le tableau, -1 sinon.
Exemples:

```python
>>> recherche([2, 3, 4, 5, 6], 5)
3
>>> recherche([2, 3, 4, 6, 7], 5)
-1
```
6. 
Écrire une fonction `recherche(caractere, chaine) `qui prend en paramètres caractere, un unique caractère (c’est-à-dire une chaîne de caractère de longueur 1), et chaine, une chaîne de caractères. Cette fonction renvoie le nombre d’occurrences de caractere dans chaine, c’est-à-dire le nombre de fois où caractere apparaît dans chaine.
Exemples :  
```python
>>> recherche('e', "sciences")
2
>>> recherche('i', "mississippi")
4
>>> recherche('a', "mississippi")
0
```
## Rangement
On veut trier par ordre croissant les notes d’une évaluation qui sont des nombres entiers compris entre 0 et 10 (inclus).
Ces notes sont contenues dans une liste `notes_eval`.
Écrire une fonction `rangement_valeurs` prenant en paramètre la liste *notes_eva*l et renvoyant une liste de longueur 11 telle que la valeur de cette liste à chaque rang est égale au nombre de notes valant ce rang. Ainsi le terme de rang 0 indique le nombre de note 0, le terme de rang 1 le nombre de note 1, etc.  
Écrire ensuite une fonction `notes_triees` prenant en paramètre la liste des effectifs des notes et renvoyant une liste contenant la liste, triée dans l’ordre croissant, des notes des élèves.
Exemple :  

```python
>>> notes_eval [2, 0, 5, 9, 6, 9, 10, 5, 7, 9, 9, 5, 0, 9, 6, 5, 4]
>>> effectifs_notes = rangement_valeurs(notes_eval)
>>> effectifs_notes
[2, 0, 1, 0, 1, 4, 2, 1, 0, 5, 1]
>>> notes_triees(effectifs_notes)
[0, 0, 2, 4, 5, 5, 5, 5, 6, 6, 7, 9, 9, 9, 9, 9, 10]
```
## Couple consécutifs  
Écrire une fonction `couples_consecutifs` qui prend en paramètre une liste de nombres entiers tab non vide, et qui renvoie la liste (éventuellement vide) des couples d'entiers consécutifs successifs qu'il peut y avoir dans tab.  
Ne pas oublier d’ajouter au corps de la fonction une documentation et une ou plusieurs assertions pour vérifier les pré-conditions.
Exemples : 
```python
>>> couples_consecutifs([1, 4, 3, 5])
[]
>>> couples_consecutifs([1, 4, 5, 3])
[(4, 5)]
>>> couples_consecutifs([1, 1, 2, 4])
[(1, 2)]
>>> couples_consecutifs([7, 1, 2, 5, 3, 4])
[(1, 2), (3, 4)]
>>> couples_consecutifs ([5, 1, 2, 3, 8, -5, -4, 7])
[(1, 2), (2, 3), (-5, -4)]
```

## Chaine de caractères
1. Le nombre d’occurrences d’un caractère dans une chaîne de caractère est le nombre d’apparitions de ce caractère dans la chaîne. 
Exemples :
- le nombre d’occurrences du caractère ‘o’ dans ‘bonjour’ est 2 ;  le nombre d’occurrences du caractère ‘b’ dans ‘Bébé’ est 1 ;
- le nombre d’occurrences du caractère ‘B’ dans ‘Bébé’ est 1 ;
- le nombre d’occurrences du caractère ‘ ‘ dans ‘Hello world !’ est 2. On cherche le nombre d’occurrences des caractères dans une chaîne de caractères. On souhaite stocker ces nombres d’occurrences dans un dictionnaire dont les clefs seraient les caractères de la chaîne et les valeurs le nombre d’occurrences de ces caractères. 

Par exemple : avec la phrase 'Hello world !' le dictionnaire est le suivant : 
{'H': 1, 'e': 1, 'l': 3, 'o': 2, ' ': 2, 'w': 1, 'r': 1, 'd': 1, '!': 1}

L’ordre des clefs n’a pas d’importance. 

Écrire une fonction `nbr_occurrences` prenant comme paramètre une chaîne de caractères `chaine` et renvoyant le dictionnaire des nombres d’occurrences des caractères de cette chaîne.

2. 
On considère des mots à trous : ce sont des chaînes de caractères contenant uniquement des majuscules et des caractères '*'. 
Par exemple 'INFO*MA*IQUE', '***I***E**' et '*S*' sont des mots à trous.
Programmer une fonction correspond qui :
- prend en paramètres deux chaînes de caractères mot et mot_a_trous où mot_a_trous est un mot à trous comme indiqué ci-dessus,
- renvoie :
    o True si on peut obtenir mot en remplaçant convenablement les caractères '*' de mot_a_trous.
    o False sinon.
Exemples :
```python
>>> correspond('INFORMATIQUE', 'INFO*MA*IQUE')
True
>>> correspond('AUTOMATIQUE', 'INFO*MA*IQUE')
False
>>> correspond('STOP', 'S*')
False
>>> correspond('AUTO', '*UT*')
True
```
3. 
Pour cet exercice :  
- On appelle « mot » une chaîne de caractères composée de caractères choisis parmi les 26 lettres minuscules ou majuscules de l'alphabet,  
- On appelle « phrase » une chaîne de caractères :  
    - composée d’un ou de plusieurs « mots » séparés entre eux par un seul caractère espace ' ',  
    - se finissant :  
- soit par un point '.' qui est alors collé au dernier mot,  
- soit par un point d'exclamation '!' ou d'interrogation '?' qui est alors séparé du dernier mot par un seul caractère espace ' '.  

Voici deux exemples de phrases :
- 'Cet exercice est simple.'  
- 'Le point d exclamation est separe !' 

Après avoir remarqué le lien entre le nombre de mots et le nombres de caractères espace dans une phrase, programmer une fonction nombre_de_mots qui prend en paramètre une phrase et renvoie le nombre de mots présents dans celle-ci.
Exemples :
```
>>> nombre_de_mots('Cet exercice est simple.')  
4
>>> nombre_de_mots('Le point d exclamation est separe !')
6
>>> nombre_de_mots('Combien de mots y a t il dans cette phrase ?')
10
>>> nombre_de_mots('Fin.')
1

```
4. 
Programmer une fonction `renverse`, prenant en paramètre une chaîne de caractères non vide, mot, et qui renvoie une chaîne de caractères en inversant ceux de la chaîne mot.  
Exemple :
```python
>>> renverse("informatique")
'euqitamrofni'
```

## Vérifications
Programmer la fonction `verifie` qui prend en paramètre un tableau de valeurs numériques non vide et qui renvoie True si ce tableau est trié dans l’ordre croissant, False sinon.
Exemples :

```python
>>> verifie([0, 5, 8, 8, 9])
True
>>> verifie([8, 12, 4])
False
>>> verifie([-1, 4])
True
>>> verifie([5])
True
```
## Ou exclusif
L'opérateur « ou exclusif » entre deux bits renvoie 0 si les deux bits sont égaux et 1 s'ils sont différents. Il est symbolisé par le caractère $`\bigoplus `$`.

Ainsi :
- $` 0 \oplus 0 = 0 `$
- $` 0 \oplus 1 = 1 `$
- $` 1 \oplus 0 = 1 `$
- $` 1 \oplus 1 = 0 `$  

On représente ici une suite de bits par un tableau contenant des 0 et des 1.
Exemples :
a = [1, 0, 1, 0, 1, 1, 0, 1]
b = [0, 1, 1, 1, 0, 1, 0, 0]
c = [1, 1, 0, 1]
d = [0, 0, 1, 1]

Écrire la fonction `ou_exclusif` qui prend en paramètres deux tableaux de même longueur et qui renvoie un tableau où l’élément situé à position i est le résultat, par l’opérateur « ou exclusif », des éléments à la position i des tableaux passés en paramètres.
En considérant les quatre exemples ci-dessus, cette fonction donne :
```python
>>> ou_exclusif(a, b)
[1, 1, 0, 1, 1, 0, 0, 1]
>>> ou_exclusif(c, d)
[1, 1, 1, 0]
```

## Doublons
Écrire une fonction `a_doublon` qui prend en paramètre une liste triée de nombres et
renvoie True si la liste contient au moins deux nombres identiques, False sinon.
Par exemple :
```python
>>> a_doublon([])
False
>>> a_doublon([1])
False
>>> a_doublon([1, 2, 4, 6, 6])
True
>>> a_doublon([2, 5, 7, 7, 7, 9])
True
>>> a_doublon([0, 2, 3])
False
```

## nombre d'occurrences
Écrire une fonction python appelée `nb_repetitions` qui prend en paramètres un élément elt et une liste tab et renvoie le nombre de fois où l’élément apparaît dans la liste.
Exemples :
```python
>>> nb_repetitions(5, [2, 5, 3, 5, 6, 9, 5])
3
>>> nb_repetitions('A', [ 'B', 'A', 'B', 'A', 'R'])
2
>>> nb_repetitions(12, [1, '! ', 7, 21, 36, 44])
0
```

## Conversion binaire
1. On modélise la représentation binaire d'un entier non signé par un tableau d'entiers dont les éléments sont 0 ou 1. Par exemple, le tableau [1, 0, 1, 0, 0, 1, 1] représente l'écriture binaire de l'entier dont l'écriture décimale est
$`2^6 + 2^4 + 2^1 + 2^0 = 83. `$
À l'aide d'un parcours séquentiel, écrire la fonction convertir répondant aux spécifications suivantes :

```python
def convertir(tab):
"""
tab est un tableau d'entiers, dont les éléments sont 0 ou 1,
et représentant un entier écrit en binaire.
Renvoie l'écriture décimale de l'entier positif dont la
représentation binaire est donnée par le tableau tab
"""
Exemple :
>>> convertir([1, 0, 1, 0, 0, 1, 1])
83
>>> convertir([1, 0, 0, 0, 0, 0, 1, 0])
130
```  
2. 
Écrire une fonction `ecriture_binaire_entier_positif` qui prend en paramètre un entier positif n et renvoie une liste d'entiers correspondant à l‘écriture binaire de n.  
Ne pas oublier d’ajouter au corps de la fonction une documentation et une ou plusieurs assertions pour vérifier les pré-conditions.  
Exemples :
```python
>>> ecriture_binaire_entier_positif(0)
[0]
>>> ecriture_binaire_entier_positif(2)
[1, 0]
>>> ecriture_binaire_entier_positif(105)
[1, 1, 0, 1, 0, 0, 1]
```
Aide :
- l'opérateur // donne le quotient de la division euclidienne : 5//2 donne 2 ;  
- l'opérateur % donne le reste de la division euclidienne : 5%2 donne 1 ;  
- append est une méthode qui ajoute un élément à une liste existante :
Soit T=[5,2,4], alors T.append(10) ajoute 10 à la liste T. Ainsi, T devient [5,2,4,10].  
- reverse est une méthode qui renverse les éléments d'une liste.
Soit T=[5,2,4,10]. Après T.reverse(), la liste devient [10,4,2,5].  

On remarquera qu’on récupère la représentation binaire d’un entier n en partant de la gauche en appliquant successivement les instructions :  
b = n%2  
n = n//2
répétées autant que nécessaire.

## Tirage aléatoire
Écrire en python deux fonctions :
- `lancer` de paramètre n, un entier positif, qui renvoie un tableau de type list de n entiers obtenus aléatoirement entre 1 et 6 (1 et 6 inclus) ;
-  `paire_6` de paramètre tab, un tableau de type list de n entiers entre 1 et 6 obtenus aléatoirement, qui renvoie un booléen égal à True si le nombre de 6 est supérieur ou égal à 2, False sinon.
On pourra utiliser la fonction randint(a,b)du module random pour laquelle la documentation officielle est la suivante :
*Renvoie un entier aléatoire N tel que a <=N <= b.*  
Exemples :
```python
>>> lancer1 = lancer(5)
[5, 6, 6, 2, 2]
>>> paire_6(lancer1)
True
>>> lancer2 = lancer(5)
[6, 5, 1, 6, 6]
>>> paire_6(lancer2)
True
>>> lancer3 = lancer(3)
[2, 2, 6]
>>> paire_6(lancer3)
False
>>> lancer4 = lancer(0)
[]
>>> paire_6(lancer4)
False
```
## Fibonacci
On s’intéresse à la suite d’entiers définie par :  
- les deux premiers termes sont égaux à 1,  
- ensuite, chaque terme est obtenu en faisant la somme des deux termes qui le précèdent.  
En mathématiques, on le formule ainsi :  
u1 = 1, u2 = 1 et, pour tout entier naturel non nul n, un+2 = un+1 + un.
Cette suite est connue sous le nom de suite de Fibonacci.
Écrire en Python une fonction fibonacci qui prend en paramètre un entier n supposé strictement positif et qui renvoie le terme d’indice n de cette suite.
Exemples :
```python
>>> fibonacci(1)
1
>>> fibonacci(2)
1
>>> fibonacci(25)
75025
>>> fibonacci(45)
1134903170
```

## Delta Encoding
Le codage par différence (delta encoding en anglais) permet de compresser un tableau de données en indiquant pour chaque donnée, sa différence avec la précédente (plutôt que la donnée elle-même). On se retrouve alors avec un tableau de données plus petit, nécessitant donc moins de place en mémoire. Cette méthode se révèle efficace lorsque les valeurs consécutives sont proches.
Programmer la fonction delta(liste) qui prend en paramètre un tableau non vide de nombres entiers et qui renvoie un tableau contenant les valeurs entières compressées à l’aide de cette technique.
Exemples :
```python
>>> delta([1000, 800, 802, 1000, 1003])
[1000, -200, 2, 198, 3]
>>> delta([42])
[42]
```

## Puissances
On rappelle que :
- le nombre $` a^{n} `$  est le nombre $` a \times a \times a \times a \times ... `$  , où le facteur 𝑎 apparaît 𝑛 fois,
- en langage Python, l’instruction t[-1] permet d’accéder au dernier élément du tableau t.
Dans cet exercice, l’opérateur ** et la fonction pow ne sont pas autorisés.
Programmer en langage Python une fonction liste_puissances qui prend en argument un nombre entier a, un entier strictement positif n et qui renvoie la liste de ses puissances [a1, a2, ... , an ].
Programmer également une fonction liste_puisssances_borne qui prend en argument un nombre entier a supérieur ou égal à 2 et un entier borne, et qui renvoie la liste de ses puissances, à l’exclusion de a0, strictement inférieures à borne.
Exemples :
```python
>>> liste_puissances(3, 5)
[3, 9, 27, 81, 243]
>>> liste_puissances(-2, 4)
[-2, 4, -8, 16]
>>> liste_puissances_borne(2, 16)
[2, 4, 8]
>>> liste_puissances_borne(2, 17)
[2, 4, 8, 16]
>>> liste_puissances_borne(5, 5)
[]
```

## Dictionnaire
1. 
Sur le réseau social TipTop, on s’intéresse au nombre de « like » des abonnés. Les données sont stockées dans des dictionnaires où les clés sont les pseudos et les valeurs correspondantes sont les nombres de « like » comme ci-dessous :  
{'Bob': 102, 'Ada': 201, 'Alice': 103, 'Tim': 50}
Écrire une fonction max_dico qui :  
-  prend en paramètre un dictionnaire dico non vide dont les clés sont des chaînes de
caractères et les valeurs associées sont des entiers positifs ou nuls ;
-  renvoie un tuple dont :
    - la première valeur est une clé du dictionnaire associée à la valeur maximale ;
    - la seconde valeur est la valeur maximale présente dans le dictionnaire.

Exemples :
```python
>>> max_dico({'Bob': 102, 'Ada': 201, 'Alice': 103, 'Tim': 50})
('Ada', 201)
>>> max_dico({'Alan': 222, 'Ada': 201, 'Eve': 220, 'Tim': 50})
('Alan', 222)
```
2. 
Écrire une fonction ajoute_dictionnaires qui prend en paramètres deux dictionnaires d1 et d2 dont les clés sont des nombres et renvoie le dictionnaire d défini de la façon suivante :
- Les clés de d sont celles de d1 et celles de d2 réunies.
- Si une clé est présente dans les deux dictionnaires d1 et d2, sa valeur associée dans le dictionnaire d est la somme de ses valeurs dans les dictionnaires d1 et d2.
- Si une clé n’est présente que dans un des deux dictionnaires, sa valeur associée dans le dictionnaire d est la même que sa valeur dans le dictionnaire où elle est présente.

Exemples :
```python
>>> ajoute_dictionnaires({1: 5, 2: 7}, {2: 9, 3: 11})
{1: 5, 2: 16, 3: 11}
>>> ajoute_dictionnaires({}, {2: 9, 3: 11})
{2: 9, 3: 11}
>>> ajoute_dictionnaires({1: 5, 2: 7}, {})
{1: 5, 2: 7}
```
3. 
On considère des tables (des tableaux de dictionnaires) qui contiennent des enregistrements relatifs à des animaux hébergés dans un refuge. Les attributs des enregistrements sont 'nom', 'espece', 'age', 'enclos'. Voici un exemple d'une telle table :
```python
animaux = [ {'nom': 'Medor', 'espece': 'chien', 'age': 5, 'enclos': 2},
{'nom': 'Titine', 'espece': 'chat', 'age': 2, 'enclos': 5},
{'nom': 'Tom', 'espece': 'chat', 'age': 7, 'enclos': 4},
{'nom': 'Belle', 'espece': 'chien', 'age': 6, 'enclos': 3},
{'nom': 'Mirza', 'espece': 'chat', 'age': 6, 'enclos': 5}]
```
Programmer une fonction selection_enclos qui :
- prend en paramètres :
    - une table table_animaux contenant des enregistrements relatifs à des animaux (comme dans l'exemple ci-dessus),
    - un numéro d'enclos num_enclos ;
- renvoie une table contenant les enregistrements de table_animaux dont l'attribut 'enclos' est num_enclos.

Exemples avec la table animaux ci-dessus :
```python
>>> selection_enclos(animaux, 5)
[{'nom':'Titine', 'espece':'chat', 'age':2, 'enclos':5},
{'nom':'Mirza', 'espece':'chat', 'age':6, 'enclos':5}]
>>> selection_enclos(animaux, 2)
[{'nom':'Medor', 'espece':'chien', 'age':5, 'enclos':2}]
>>> selection_enclos(animaux, 7)
[]
```

4. 
Écrire une fonction `enumere` qui prend en paramètre une liste L et renvoie un dictionnaire d dont les clés sont les éléments de L avec pour valeur associée la liste des indices de l’élément dans la liste L.
Exemple :
```python
>>> enumere([1, 1, 2, 3, 2, 1])
{1: [0, 1, 5], 2: [2, 4], 3: [3]}
```

5. 
Écrire une fonction `min_et_max` qui prend en paramètre un tableau de nombres tab non vide, et qui renvoie la plus petite et la plus grande valeur du tableau sous la forme d’un dictionnaire à deux clés 'min' et 'max'.
Les tableaux seront représentés sous forme de liste Python.
L’utilisation des fonctions natives min, max et sorted, ainsi que la méthode sort n’est pas autorisée.
Ne pas oublier d’ajouter au corps de la fonction une documentation et une ou plusieurs assertions pour vérifier les pré-conditions.
Exemples : 
```python
>>> min_et_max([0, 1, 4, 2, -2, 9, 3, 1, 7, 1])
{'min': -2, 'max': 9} >>> min_et_max([0, 1, 2, 3]) {'min': 0, 'max': 3} >>> min_et_max([3])
{'min': 3, 'max': 3} >>> min_et_max([1, 3, 2, 1, 3])
{'min': 1, 'max': 3} >>> min_et_max([-1, -1, -1, -1, -1])
{'min': -1, 'max': -1}
```
## Tri sélection
Écrire une fonction `tri_selection` qui prend en paramètre une liste *tab* de nombres entiers et qui renvoie la liste triée par ordre croissant. Il est demandé de ne pas créer de nouvelle liste mais de modifier celle fournie.
On utilisera l’algorithme suivant :
- on recherche le plus petit élément de la liste, en la parcourant du rang 0 au dernier rang, et on l'échange avec l'élément d'indice 0 ;  
- on recherche ensuite le plus petit élément de la liste restreinte du rang 1 au dernier rang, et on l'échange avec l'élément d'indice 1 ;  
- on continue de cette façon jusqu'à ce que la liste soit entièrement triée.  
Exemple :
```python
>>> tri_selection([1, 52, 6, -9, 12])
[-9, 1, 6, 12, 52]
```

## arbre
1. 
![arbre](./img/arbre.png)

2. 
![arbre binaire](./img/arbre_cara.png)

## ABR
![ABR](./img/ABR.png)

## Fusion de tableaux
Programmer la fonction fusion prenant en paramètres deux tableaux non vides tab1 et tab2 (de type list) d'entiers, chacun dans l’ordre croissant, et renvoyant un tableau trié dans l’ordre croissant et contenant l’ensemble des valeurs de tab1 et tab2.
Exemples
```python
>>> fusion([3, 5], [2, 5])
[2, 3, 5, 5]
>>> fusion([-2, 4], [-3, 5, 10])
[-3, -2, 4, 5, 10]
>>> fusion([4], [2, 6])
[2, 4, 6]
``` 
