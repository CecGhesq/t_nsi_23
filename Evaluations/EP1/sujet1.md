Réaliser les deux exercices suivants dans un seul et même fichier : celui de base de l'[exercice 2](23-NSI-01.py).
Déposer ensuite votre fichier sur pronote.


# Exercice 1 :
Programmer la fonction multiplication prenant en paramètres deux nombres entiers
relatifs n1 et n2, et qui renvoie le produit de ces deux nombres.
Les seules opérations autorisées sont l’addition et la soustraction.
Exemples :
```python
>>> multiplication(3, 5)
15
>>> multiplication(-4, -8)
32
>>> multiplication(-2, 6)
-12
>>> multiplication(-2, 0)
0
```
# Exercice 2 :
![](EP1_A.png)
![](EP1_B.png)
