Réaliser les deux exercices suivants dans un seul et même fichier : celui de base de l'[exercice 2](23-NSI-06.py).

__Déposer ensuite votre fichier sur pronote.__


# Exercice 1 :
Écrire une fonction `recherche` qui prend en paramètres un nombre entier *elt* et un tableau *tab* de nombres entiers, et qui renvoie l’__indice de la première occurrence__ de *elt* dans *tab* si *elt* est dans *tab* et -1 sinon.
Ne pas oublier d’ajouter au corps de la fonction une documentation et une ou plusieurs assertions pour vérifier les pré-conditions.

Exemples :
```python
>>> recherche(1, [2, 3, 4])
-1
>>> recherche(1, [10, 12, 1, 56])
2
>>> recherche(50, [1, 50, 1])
1
>>> recherche(15, [8, 9, 10, 15])
3
>>> recherche(50, [])
-1
>>> recherche(4, [2, 4, 4, 3, 4])
1
```
# Exercice 2 :
![](EP6_A.png)
![](EP6_B.png)
![](EP6_C.png)
