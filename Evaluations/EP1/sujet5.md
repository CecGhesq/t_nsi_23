Réaliser les deux exercices suivants dans un seul et même fichier : celui de base de l'[exercice 2](23-NSI-17.py).

__Déposer ensuite votre fichier sur pronote.__


# Exercice 1 :
Écrire une fonction `recherche(caractere, chaine) `qui prend en paramètres `caractere`, un unique caractère (c’est-à-dire une chaîne de caractère de longueur 1), et `chaine`, une chaîne de caractères. Cette fonction renvoie le nombre d’occurrences de caractere dans chaine, c’est-à-dire le nombre de fois où caractere apparaît dans chaine.
Exemples :  
```python
>>> recherche('e', "sciences")
2
>>> recherche('i', "mississippi")
4
>>> recherche('a', "mississippi")
0
```

# Exercice 2 :
![](EP17_A.png)
![](EP17_B.png)
