Réaliser les deux exercices suivants dans un seul et même fichier : celui de base de l'[exercice 2](23-NSI-05.py).
Déposer ensuite votre fichier sur pronote.


# Exercice 1 :
Écrire une fonction indices_maxi qui prend en paramètre une liste tab, non vide, de nombres entiers et renvoie un couple donnant d’une part le plus grand élément de cette liste et d’autre part la liste des indices de la liste tab où apparaît ce plus grand élément.
On ne peut utiliser la fonctions natives max().
Exemples :
```python
>>> indices_maxi([1, 5, 6, 9, 1, 2, 3, 7, 9, 8])
(9, [3, 8])
>>> indices_maxi([7])
(7, [0])
```
# Exercice 2 :
![](EP5_A.png)
![](EP5_B.png)
![](EP5_C.png)
