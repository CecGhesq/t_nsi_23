# Revisions : on remuscle ! 

## boucle for
 En utilisant __obligatoirement__ la boucle bornée `for` : 

1. Ecrire une fonction `for_1()` qui demande un entier _n_ à l'utilisateur, puis calcule et affiche le résultat de la multiplication 2 x 2 x 2 .... x 2 ( où on a _n_ occurences de 2 )
Rappel : input() ; int()

2. Ecrire une fonction `for_2()` qui demande un entier _n_ à l'utilisateur, puis calcule et affiche 1 + 2 + ..... + n ainsi que l'entier __n*(n+1)//2__. Que remarque-t-on?

3. Ecrire une fonction `for_3(car,chaine)`qui renvoie le nombre d'occurrences d'un caractère dans une chaine.  

## Boucle while

En utilisant la boucle non bornée _while_:

1. Ecrire une fonction `while_1(n)`qui prend en entier positif ou nul en argument et renvoie son nombre de chiffres.

2. Ecrire un programme `while_2(n)` qui prend un entier strictement positif en argument puis, tant que cet entier n'est pas égal à 1, le divise par deux s'il est pair ou le multiplie par trois et lui ajoute un s'il est impair. Afficher tous les nombres obtenus.  
Information : ceci est la conjecture de Syracuse, on en reparlera...  

## Comparaisons

Ecrire une fonction `triangle()` qui demande trois longueurs à l'utilisateur, indique si ces trois longueurs peuvent être les longueurs des trois côtés d'un triangle et, le cas échéant s'il s'agit d'un triangle équilatéral, isocèle ou scalène ( trois côtés de longueurs différentes).  
Information: il s'agit d'un triangle si aucun côté n'est strictement supérieur à la somme des deux autres.

## Listes  

liste = [1, 2, 4, 7, 3, 6, 8, 2]  
Quelle est LA propriété des listes à connaître?
1. Ecrire une fonction `liste_1(v, liste)` qui renvoie le nombre d'occurrences de la valeur _v_ dans une liste en itérant sur les indices puis sur les valeurs. (on n'utilisera pas la méthode count()) 

2. Ecrire une fonction `liste_2()` qui construit une liste de 20 entiers tirés au hasard entre 1 et 1000, puis l'affiche.  

__aide__ : import random  
random.randint(0, 3) : entier entre 0 et 3 inclus.

3. Ecrire une fonction `liste_3(n)`retournant une liste en compréhension contenant _n_ caractères "*".  

4. Ecrire une fonction `liste_4()`retournant une liste en compréhension contenant les 10 premiers nombres pairs à partir du chiffre 2 en utilisant une condition dans la syntaxe.  

## Tuples

t = (2,4,6,7,1)

1. Ecrire une fonction `tup_1(chaine)` qui transforme une chaine de caractère en tuple.

2. Ecrire une fonction `tup_2(t)` qui renvoie un couple donnant la valeur maximale avec son indice.  

## Dictionnaires

1. Créer quelques dictionnaires  avec pour clés les noms et prénoms et lieu d'habitation.

2. Afficher pour l'un d'entre eux toutes les clés.

3. Regrouper ses dictionnaires dans un seul nommé __amis__ ayant une lettre comme clé.

4. Créer la fonction dico_1(amis)permettant d'afficher tous les prénoms des amis.

5. Créer la fonction dico_2(amis) créant uneliste avec tous les lieux d'habitation de vos amis en évitant les doublons.
