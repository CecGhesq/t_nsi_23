####modules tri de premiere NSI
tab = [5,7,9,2,7,0]
def __recherche_indice_min(tab, i_depart):
    '''renvoie l'indice de l'élément le plus petit
    entre i_depart et la fin du tableau
    : tab : (list)
    : i_depart : (int)
    @return (int)
    '''
    valeur_min = tab[i_depart]
    i_min = i_depart
    for i in range(i_depart+1, len(tab)):
        if tab[i]< valeur_min :
            valeur_min = tab[i]
            i_min = i
    return i_min
        
def __echange(tab, indice1, indice2):
    '''échange les éléments d'indice 1 et 2 en place
    : tab : (list)
    : indice_1, indice_2 : (int)
    :CU: effet de bord
    '''
    tab[indice1], tab[indice2] = tab[indice2], tab[indice1]

def tri_selection(tab):
    '''réalise le tri par selection du tableau
    : tab : (list)
    '''
    for i in range(len(tab)-1):
        i_min = __recherche_indice_min(tab, i)
        __echange(tab, i_min, i)
        
#### tri insertion
    #### tri par insertion
tab1 = [4,2,3,5,1]  
def insere(tab, index) :
    '''insère à la bonne place l'élt d'indice = index dans le tableau trié jusque index - 1
    : tab : (list) triée jusque index - 1
    : index : (int) indice de l'élt à inserer
    '''
    a_inserer = tab[index]
    j = index
    while j >= 1 and a_inserer < tab[j-1] :
        tab[j] = tab[j-1]
        j = j-1
    tab[j] = a_inserer
    return tab

def tri_i(tab) :
    for i in range (1,len(tab)):
        tab_trié = insere(tab, i)
