---
title: " : Algorithmique : 10 Diviser pour régner"
subtitle: "cours"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
||||
|:---|---|---|
|Méthode "diviser pour régner"|Ecrire un algorithme utilisant la méthode "diviser pour régner"|La rotation d'une image bitmap d'un quart de tour avec un  oût constant est un bon exemple. L'exemple du tri fusion permet également d'exploiter la récursivité et d'exhiber un algorithme de coût en $`n.log_{2}n `$ dans le pire des cas| 

# La méthode  

## Principe de l’algorithme  

Un algorithme de type « diviser pour régner » est un algorithme qui, pour résoudre un problème de taille n, utilise la résolution d’un ou plusieurs problèmes de taille inférieure à n.
En règle générale, ces algorithmes utilisent le principe de la __récursivité__, la solution finale étant la recombinaison des petits problèmes résolus.

Ce paradigme « diviser pour régner » repose donc sur trois étapes :
- __Diviser__ : le problème d’origine est divisé en un certain nombre de sous problèmes,
- __Régner__ : chaque sous problème est résolu,
- __Combiner__ : les solutions sont combinées afin d’obtenir la solution au problème d’origine.

## Caractéristiques générales :
Basé sur un algorithme récursif, ces algorithmes peuvent produire un débordement de pile.  
Souvent efficaces, cette efficacité dépend de la décomposition en sous problèmes qui est faite.  
Ces algorithmes se prêtent souvent bien à un traitement parallèle de chacun des sous problèmes.  

Quelques exemples classiques :

* Détermination du maximum de 4 nombres :

max(15, 2020, 560, 24) = max(max(15, 2020), max(560, 24))  

* Détermination du plus grand élément d’une liste :  

max([a1, a2, ... an]) = max (a1, max([a2, ... an]))

  
# Recherche dichotomique

## Traitement itératif, version 1_NSI 

Le principe de cette recherche est de placer un indice pivot sur l’élément central de la liste ou d’un intervalle de la liste, de le déplacer jusqu’à rencontrer la valeur recherchée, tant que ce pivot puisse être le milieu d’un intervalle non nul.

```python
def recherche_dichotomique(tab, val, gauche , droite):
    """ 
    Recherche la position d'un entier dans un tableau trié
    :tab: list
    :val : int
    :gauche , droite : int indices des extrémités du tableau
    @ return indice de position de la valeur dans le tableau ; None sinon
    """
    while gauche <= droite:
        milieu = (gauche + droite) // 2
        if tab[milieu] < val :
            gauche = milieu + 1
        elif tab[milieu] > val:
            droite = milieu - 1
        else :
            return milieu
    return None
```

## Traitement récursif, version diviser pour régner :

```python
def recherche_dichotomique(tab, val, gauche , droite):
    if gauche > droite : 
        return None
    milieu = (gauche + droite) // 2
    if tab[milieu] < val :
        return recherche_dichotomique(tab, val, milieu + 1 , droite) 
    elif tab[milieu] > val:
        return recherche_dichotomique(tab, val, gauche , milieu - 1)
    else :
        return milieu 
```
La terminaison de l’algorithme se produit lorsque que la valeur de l'indice gauche devient supérieur à celui de l'indice droite, ce qui se produit que le variant "droite – gauche" est décroissant.

## Complexité de l’algorithme 

Pour une liste de n éléments, à chaque étape, on réduit la recherche à une liste de taille au plus égale à n // 2.  
A l’étape 1, on réduit la recherche à une liste de taille au plus égale à n//2,  
A l’étape 2, on réduit la recherche à une liste de taille au plus égale à n//($`2^{2}`$),  
A l’étape 3, on réduit la recherche à une liste de taille au plus égale à n//($`2^{3}`$),  
…… jusqu’à n//($`2^{n}`$) > 1  

Notons T (n) le nombre d’itérations de l’algorithme sur une entrée de taille n au pire cas. On a : 
$`\left\{\begin{matrix}
 T(\frac{n}{2}) + 1& si\ n\geqslant 2& T(\frac{n}{2})\ correspondant\  à\  l'appel\  récursif\\
1 & sinon \\
\end{matrix}\right.`$  
On peut donc dominer la complexité sur une entrée de taille n par celle sur une entrée de taille $`2^{log_{2}(n)}`$ . En se ramenant à ce dernier cas (tableau dont la taille est une puissance de 2), on prouve par récurrence que la complexité de cet algorithme est $`O(log_{2}(n))`$.  

En effet, la suite $`T(2^{n})_{n\ \epsilon  \ \mathbb{N}}`$  est égale à la suite 1, 2, 3, . . . n par récurrence, donc $`T(2^{n})`$ = O(n), et donc T (n) = $`O(log_{2}(n))`$ pour n puissance de 2.  ( on applique la fonction réciproque :  $`log_{2}(2^{n}) = n`$).  

__La complexité est ainsi en__ $`O(log_{2}(n))`$

![](./img/Figure_1.png)  

# Tri fusion  

Soit une liste de nombres non triée. L’objectif est de fournir cette liste de manière que tous les éléments de cette liste soit classé dans l’ordre croissant.  
[5, 2, 8, 6, 4, 1] --> [1, 2, 4, 5, 6, 8]

Le principe du tri fusion est que lorsque la liste est de longueur au plus de 1, elle est triée, sinon on fusionne le tri de la première moitié de la liste avec le tri de la seconde moitié.  

## Algorithme de fusion de deux listes en Python

```python
def fusion(liste_a , liste_b):
    '''Retourne une liste triée dans l'ordre croissant
    :liste_a, liste_b: list
    @return : list
    '''
    l = [] # initialisation liste vide
    while liste_a !=[] and liste_b !=[] :
     # comparaison des deux premiers éléments des deux listes
        if liste_a[0] < liste_b[0] :
            l.append(liste_a.pop(0)) # elt retiré à liste_a et ajoute à l
        else :
            l.append(liste_b.pop(0)) # elt retiré à liste_b et ajoute à l
    return l + liste_a + liste_b # ajout du dernier élément éventuel dans les listes initiales


>>> fusion([5,2,8],[6,4,1])
[5, 2, 6, 4, 1, 8]
```

## Algorithme récursif du tri fusion  

```python
def tri_fusion(liste) :
    '''retourne une liste triée dans l'ordre croissant des valeurs
    :liste : list
    @return : list
    '''
    if len(liste) < 2 :  # liste avec 0 ou 1 élément
        return liste
    else :
        milieu = len(liste)//2
        return fusion(tri_fusion(liste[:milieu]), tri_fusion(liste[milieu:]))


>>> tri_fusion([5,2,8,6,4,1])
[1, 2, 4, 5, 6, 8]
```
Observez le fonctionnement avec le lien [ Python Tutor](https://urlz.fr/gK9x)  et réaliser un arbre du déroulement :  

```



























``` 

## Complexité de l’algorithme

La phase _Diviser_ correspond à couper la liste en deux listes :  si la liste comporte n éléments, la liste devra être divisée en $`log_{2}(n)`$.  
La phase _Fusionner_ correspond à comparer les premiers éléments de listes triées. Pour n éléments, il devrait donc y avoir __n__ comparaisons.  
L’ordre de grandeur de la complexité du tri-fusion semble donc en $`O(n.log_{2}(n))`$.

On retient donc les complexités typiques :  

| relation de récurrence|Complexité|
|:---:|:---:|
|T(n/2) + O(n)| $`O(log_{2}(n))`$|
|2T(n/2) + O(1)|$`O(n)`$|
|2T(n/2) + O(n)|$`O(n.log_{2}(n))`$|

Sources :  

* Cours NSI P Lucas lycée Pasteur Hénin Beaumont
* NSI Terminale Ellipses T. Balabonski,S. Conchon, JC. Filliatre, K Nguyen
