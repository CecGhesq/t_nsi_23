#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 15:17:34 2021

@author: frederic
"""
from random import randint, sample
from copy import copy
def quiTireSurQui(taille):
    """
    Etant donné un tableau d'entiers strictement positifs tous différents,
    renvoie pour chaque élément du tableau l'indice de l'entier le plus grand "visible" à
    sa droite, ainsi qu'à sa gauche. Un élément du tableau ne peut pas "voir" les
    entiers cachés par des entiers plus grands que lui.
    Exemple :
        taille = [185, 173, 116, 54, 150, 51, 60, 180, 229, 170]
        L'élémént 150 d'indice 4 verra 
        à sa gauche 173 d'indice 1 
        et à sa droite 180 d'indice 7
        d'où gauche[4] =  1 et droite[4] = 7
    @param taille : tableau d'entiers strictement positifs
    @param gauche, droite : tableau d'indices du tableau taille, ou None
    """
    n = len(taille)
    gauche = [None]*n
    droite = [None]*n
    for j in range(n):
        for i in range(j):          
            if taille[i] > taille[j]:
                gauche[j] = i
        for k in range(n - 1, j, -1):
            if taille[k] > taille[j]:
                droite[j] = k
    return gauche, droite

def qTsQ(taille, tire_gauche, tire_droite, g, d):
    """
    Etant donné un tableau d'entiers strictement positifs 
    tous différents, renvoie pour chaque élément du tableau l'indice de l'entier le plus
    grand "visible" à sa droite, ainsi qu'à sa gauche. Un élément du tableau ne peut pas 
    "voir" les entiers cachés par des entiers plus grands que lui.
    Exemple :
    taille = [185, 173, 116, 54, 150, 51, 60, 180, 229, 170]
    L'élément 150 d'indice 4 verra 
    à sa gauche 173 d'indice 1 
    et à sa droite 180 d'indice 7
    d'où gauche[4] =  1 et droite[4] = 7
    @param taille : tableau d'entiers strictement positifs
    @param tire_gauche, tire_droite : tableaux d'indices du tableau taille, ou None
    @param g, d : indices du tableau taille avec g ≤ d
    @return : tire_gauche,tire_droite
     """
    if g == d:
        return
    elif d == g + 1:
        if taille[g] < taille[d]:
            tire_droite[g] = d
        else:
            tire_gauche[d] = g
        return(tire_gauche, tire_droite)
    else:
        m  = (g + d)//2
        qTsQ(taille, tire_gauche, tire_droite, g, m)
        qTsQ(taille, tire_gauche, tire_droite, m + 1, d)
        return fusion(taille, tire_gauche, tire_droite, g, d)
    
def fusion(taille, tire_gauche, tire_droite, g, d):
    """
    Combine les résultats obtenus lors de la phase de division
    @param taille : tableau d'entiers strictement positifs
    @param tire_gauche, tire_droite : tableaux d'indices du tableau taille, ou None
    @param g, d : indices du tableau taille avec g ≤ d
    @return : (tire_gauche, tire_droite)
    """
    m = (g + d)//2
    i = m
    j = m + 1
    #mise à jour de tire_droite
    while i >= g and j <= d:
        if tire_droite[i] is None:
            if taille[j] > taille[i]:
                tire_droite[i] = j
                i = i - 1
            else :
                j = j + 1
        else:
            i = i - 1
    #mise à jour de tire_gauche
    i = m
    j = m + 1
    while i >= g and j <= d:
        if tire_gauche[j] is None:
            if taille[i] > taille[j]:
                tire_gauche[j] = i
                j = j + 1
            else :
                i = i - 1
        else:
            j = j + 1    
    return (tire_gauche, tire_droite)


taille = [185, 173, 116, 54, 150, 51, 60, 180, 229, 170]
nom = ['A','B','C','D','E','F','G','H','I','J']
print("tailles : ", taille)
#taille = [185,  116, 54, 150,  60, 229, 170]
print("\nVersion force brute")
g, d = quiTireSurQui(taille)
print("gauche = ",g)
print("droite = ", d)
print("\nVersion diviser pour régner")
tire_gauche = [None]*len(taille)
tire_droite = [None]*len(taille)
# 
# g1, d1 = qTsQ(taille, tire_gauche, tire_droite, 0, len(taille) - 1)
# print("gauche = ",g1)
# print("droite = ", d1)

def final(taille):
    mort = []
    mini = min(taille) 
    n_tour = 1
    while sum(taille) > mini :
        tire_gauche = [None]*len(taille)
        tire_droite = [None]*len(taille)
        g1, d1 = qTsQ(taille, tire_gauche, tire_droite, 0, len(taille) - 1)
        print()
        print(taille,'avant', g1, d1)
        for i in range(len(taille)):
            if g1[i] != None :
                if nom[g1[i]] not in mort :
                    mort.append(nom[g1[i]])
                    print('abattu(g1) au tour ',n_tour,nom[g1[i]],end=" ")
                    taille[g1[i]]= 0
                  
            if d1[i] != None :
                if nom[d1[i]] not in mort :
                    mort.append(nom[d1[i]])
                    taille[d1[i]]= 0
                    print('abattu(d1) au tour ',n_tour,nom[d1[i]],end=" ")
             
        n_tour = n_tour + 1
    print()
    return 
    
    
print(final(taille))     
    
        
            
        
        
       
        
# taille = sample(range(50,300),20)
# print("\ntailles : ", taille)
# #taille = [185,  116, 54, 150,  60, 229, 170]
# print("\nVersion force brute")
# g, d = quiTireSurQui(taille)
# print("gauche = ",g)
# print("droite = ", d)
# print("\nVersion diviser pour régner")
# taille = [185, 173, 116, 54, 150, 51, 60, 180, 229, 170]
# tire_gauche = [None]*len(taille)
# tire_droite = [None]*len(taille)
# qTsQ(taille, tire_gauche, tire_droite, 0, len(taille) - 1)
# print("gauche = ", g)
# print("droite = ", d)