def for_1():
    ''' demande le nbre d'occurrences n et calcule 2^n et affiche le résultat
    '''
    valeur = 1
    n = int(input("quelle est la valeur de n ?" ))
    for _ in range(n) :
        valeur= valeur * 2
    print(valeur)

def for_2():
    '''calcule et affiche les n premiers entiers (n demandé à l'utilisateur) et affiche un autre entier
    CU : n > 0
    '''
    somme = 0
    n = int(input("quelle est la valeur de n ?" ))
    entier = (n*(n+1)//2)
    for i in range(1,n+1):
        somme = somme + i
    print("la somme  de 1 à ", n , "vaut ", somme)
    print ("l'entier vaut ",entier)

def for_3(car, chaine):
    '''renvoie le nombre d'occurrences dans la chaine
    : car : ( str) caractère recherché
    : chaine : (str) chaine étudiée
    @ return (int) nbre d'occurrences
    '''
    occu = 0
    for elt in chaine :
        if elt == car :
            occu = occu + 1
    return occu

def while_1(n):
    '''renvoie le nombre de chiffres d'un nombre
    : n: (int) entier positif ou nul
    @ return (int)
    '''
    n_chiffres= 1
    while n>= 10 :
        n = n//10
        n_chiffres = n_chiffres + 1
    return n_chiffres

def while_2(n):
    '''affiche les nombres de la conjecture de Syracuse
    : n: (int) n >0
    '''
    assert n > 0 , " n doit être strictement positif"
    while n != 1 :
        if n%2 :
            n = n * 3 +1
            print(n)
        else :
            n = n // 2
            print(n)

def triangle():
    '''indique si les 3 longueurs données peuvent former un triangle et lequel si c'est possible
    '''
    l1 = int(input("quelle est la valeur du premier côté ?"))
    l2 = int(input("quelle est la valeur du deuxième côté ?"))
    l3 = int(input("quelle est la valeur du troisième côté ?"))
    if l1 <= l2 + l3 and l2 <= l1 + l3 and l3 <= l1 + l2 :
        if l1 == l2 and l2 ==l3 :
            print("le triangle est équilatéral")
        elif l1 == l2 or l2 ==l3 or l1==l3 :
            print("le triangle est isocèle")
        else:
            print("le triangle est scalène")
    else :
        print( "ces valeurs ne peuvent former un triangle")

liste = [1, 2, 4, 7, 3, 6, 8, 2]
def liste_1_i(v,liste):
    '''renvoie le nbre d'occurrences de v dans la liste
    : v : (int) valeur recherchée
    : liste : (list)
    @ return (int)
    '''
    occ = 0
    for i in range(len(liste)):
        if liste[i] ==v :
            occ = occ + 1
    return occ

def liste_1_s(v,liste):
    '''renvoie le nbre d'occurrences de v dans la liste
    : v : (int) valeur recherchée
    : liste : (list)
    @ return (int)
    '''
    occ = 0
    for elt in liste:
        if elt ==v :
            occ = occ + 1
    return occ

import random
def liste_2():
    '''construit une liste de 20 entiers aléatoires compris entre 1 et 1000
    
    '''
    l= []
    for _ in range(20):
        l.append(random.randint(1,1000))
    print(l)

def liste_3(n):
    '''renvoie une liste contenant n '*'
    : n: (int) nombre de symboles
    @ return (list)
    '''
    return ['*' for _ in range(n)]

def liste_4() :
    '''liste de 10 nbres pairs
    @ return (list)
    '''
    return [i for i in range(1,21) if i%2 == 0 ]

def tup_1(chaine) : 
    '''renvoie un tuple à partir d'une chaine de caractères
    : chaine : (str)
    @ return (tuple)
    '''
    return tuple(chaine)

t = (2,4,6,7,1)
def tup_2(t):
    '''renvoie la valeur maximale et son indice dans t
    '''
    maxi = t[0]
    indice = 0
    for i in range(len(t)):
        if t[i] > maxi :
            maxi = t[i]
            indice = i
    return maxi , indice

d1 = {'nom' : 'Flah' , 'prenom' : 'Rémi' , 'lieu' : 'Paris'}
d2 = {'nom' : 'Bertri' , 'prenom' : 'Jean-Paul' , 'lieu' : 'Surques'}
d3 = {'nom' : 'Colin' , 'prenom' : 'Sandrine' , 'lieu' : 'Paris'}

print(d1.keys())

amis = {'a' : d1 , 'b' : d2 , 'c' : d3}

def dico1_amis(amis):
    '''affiche tous les prénoms des amis
    : amis : (dict (dict))
    '''
    for dico in amis.values() :
        print(dico["prenom"])

def dico2(amis) :
    '''renvoie une liste avec tous les lieux sans doublons
    : amis : (dict (dict))
    '''
    liste_lieux = []
    for dico in amis.values() :
        hab = dico["lieu"]
        if hab not in liste_lieux :
            liste_lieux.append(hab)
    return liste_lieux