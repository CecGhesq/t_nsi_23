#### pile bornée
class Pile_bornee:
    def __init__(self,c):
        self.contenu = [None]*c
        self.prochain = 0
    
    def __str__(self):
        ch='\n Etat pile bornée\n'
        contenu = self.contenu
        for i in range(len(contenu)-1,-1,-1) :
            if contenu[i] is not None :
                ch = ch + ' |\t '+ str(contenu[i]) + '\t |\n'
            else :
                ch = ch + ' |\t '+ '*' + '\t |\n'
        return ch
        
    def est_vide(self):
        return self.prochain == 0
    
    def est_pleine(self):
        return self.prochain == len(self.contenu)
    
    def empiler(self,e):
        if self.est_pleine():
            raise IndexError('la pile est pleine')
        else :
            self.contenu[self.prochain] = e
            self.prochain = self.prochain + 1
    
    def depiler(self):
        if self.est_vide():
            raise IndexError('la pile est vide')
        else :
            valeur = self.contenu[self.prochain -1]
            self.contenu[self.prochain -1] = None
            self.prochain = self.prochain - 1
            return valeur
            

p_b = Pile_bornee(6)
for i in range(6):
    p_b.empiler(i)
print(p_b)
print(p_b.depiler())
print(p_b)
p_b.empiler(10)
print(p_b)


    

    