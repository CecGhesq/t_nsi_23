### file bornée
class File_bornee:
    def __init__(self,c):
        self.contenu = [None]*c
        self.premier = 2
        self.nombre = 0 # nombre d'éléments dans la liste
    
    def est_vide(self):
        return self.nombre == 0
    
    def est_pleine(self):
        return self.nombre == len(self.contenu)
    
    def ajouter(self,e):
        if self.est_pleine():
            raise IndexError('la file est pleine')
        else :
            indice_position = (self.premier + self.nombre) % len(self.contenu)
            self.contenu[indice_position] = e
            self.nombre = self.nombre + 1
                
    def retirer(self):
        if self.est_vide():
            raise IndexError('la file est vide')
        else :
            valeur = self.contenu[self.premier]
            self.contenu[self.premier]= None
            self.premier = (self.premier + 1)% len(self.contenu)
            self.nombre = self.nombre -1
            return valeur
f_b = File_bornee(6)
f_b.ajouter("a")
print(f_b.contenu)
#[None, None, 'a', None, None, None]
f_b.ajouter("b")
print(f_b.contenu)
#[None, None, 'a', 'b', None, None]
f_b.ajouter("c")
print(f_b.contenu)
#[None, None, 'a', 'b', 'c', None]
f_b.ajouter("d")
f_b.ajouter("e")
print(f_b.contenu)
#['e', None, 'a', 'b', 'c', 'd']
print(f_b.nombre)
#5
print()
f_b.retirer()
#'a'
print(f_b.premier)
#3
print(f_b.contenu)
#['e', None, None, 'b', 'c', 'd']
print(f_b.nombre)
#4
