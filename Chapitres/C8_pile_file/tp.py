class Cellule :
    def __init__(self, v, s):
        self.valeur = v
        self.suivante = s

class Pile :
    def __init__(self):
        self.contenu = None
    
    def est_vide(self):
        return self.contenu is None
    
    def empiler(self,e):
        c =  Cellule(e, self.contenu)
        self.contenu = c

    def depiler(self) :
        if self.est_vide() : # on utilise la méthode !!!!
            raise IndexError("dépiler une liste vide")
        else :
            e = self.contenu.valeur
            self.contenu = self.contenu.suivante
            return e
    def __str__(self):
        ch='\n Etat de la pile\n'
        contenu = self.contenu
        while contenu is not None :
            ch = ch + '| \t ' + str(contenu.valeur) + '\t | \n'
            contenu = contenu.suivante
        return ch
    
    def consulter(self):
        if self.est_vide():
            raise IndexError('la pile est vide')
        else:
            return self.contenu.valeur
    def vider(self):
        self.contenu = None
        
    def retourner(self):
        pile_renv= Pile()
        while  not self.est_vide():
            pile_renv.empiler(self.depiler())
        return pile_renv

class File :
    def __init__(self):
        self.tete = None
        self.queue = None
    
    def est_vide(self) :
        # on peut tester tete : les deux doivent être vides
        return self.queue is None 

    def enfile(self,e) :
        '''on enfile sur la queue de la liste
        '''
        c = Cellule(e , None)
        if self.est_vide() :
            self.tete = c
        else :
            self.queue.suivante = c
        self.queue = c
    
    def defile(self) :
        '''on récupère la valeur de la tete mais on l'enlève de la file
        '''
        if self.est_vide() :
            raise IndexError ( "defile une file vide")
        else :
            t = self.tete.valeur
            self.tete = self.tete.suivante
            if self.tete is None :
                self.queue = None
            return t

### algo bon parenthésage ####
# Entrée : chaine
# sortie bool

# pile <-- creation Pile
# POUR elt dans chaine :
#    SI elt = '(' OU '[' :
#       empiler elt
#    SINON SI elt = ')' :
#       SI le sommet pile = '(':
#            rien
#       SINON :
#             renvoie Faux
#       fin SI
#    SINON SI elt = ']' :
#       SI le sommet pile = '[':
#            rien
#       SINON :
#            renvoie Faux
#       fin SI
# fin POUR
# SI pile vide:
#     renvoie Vrai
#fin SI
#
def is_good_par(chaine):
    '''prédicat testant le bon parenthésage d'une expression
    : chaine : (str)
    @return (bool)
    '''
    pile_par = Pile()
    for elt in chaine :
        if elt =='(' or elt == '[':
            pile_par.empiler(elt)
        elif elt == ')':
            if pile_par.depiler() == '(':
                pass
            else :
                return False
        elif elt == ']':
            if pile_par.depiler() == '[':
                pass
            else :
                return False
    if pile_par.est_vide():
        return True

#print(is_good_par('(3*[2+3])'))
exple_pile= Pile()
for i in range(5):
    exple_pile.empiler(i)
# print(exple_pile)
# ret = exple_pile.retourner()
# print(ret)


# NPI
# liste <--- transformer chaine en liste
# pile_calcul <-- creer pile
# POUR elt dans liste
#       SI elt = + ou *:
#           x<-- depiler la pile_calcul
#           y<-- depiler la pile_calcul
#           SI elt = +:
#               empiler(x+y)
#           SINON
#               empiler ( x*y)
#           fin SI
#       SINON
#            empiler elt
#      fin SI
#fin POUR
# renvoyer depiler pile_calcul

def NPI(chaine):
    '''renvoie le résultat d'une chaine
    de calculs en NPI
    : chaine : (str)
    @return (int)
    '''
    liste_calculs = chaine.split(' ')
    pile_calculs = Pile()
    for elt in liste_calculs :
        if elt =='*' or elt == '+':
            x = int(pile_calculs.depiler())
            y = int(pile_calculs.depiler())
            if elt =='*':
                pile_calculs.empiler(x*y)
            else:
                pile_calculs.empiler(x+y)
        else :
            pile_calculs.empiler(elt)
    return pile_calculs.depiler()