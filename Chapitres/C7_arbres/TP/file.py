# cours  file
class Cellule :
    '''cellule d'une liste chaînée
    '''
    def __init__( self, v, s):
        self.valeur = v
        self.suivante = s
           
class File :
    def __init__(self):
        self.tete = None
        self.queue = None
    
    def est_vide(self) :
        return self.tete is None 
    
    def enfile(self,e) :
        '''on enfile sur la queue de la liste
        '''
        c = Cellule(e , None)
        if self.est_vide() :
            self.tete = c
        else :
            self.queue.suivante = c
        self.queue = c
        
    def defile(self) :
        '''on récupère la valeur de la tete mais on l'enlève de la file
        '''
        if self.est_vide() :
            raise IndexError ( "defile une file vide")
        t = self.tete.valeur
        self.tete = self.tete.suivante
        if self.tete is None :
            self.queue = None
        return t
        
def creer_file():
    return File()
    

           
       