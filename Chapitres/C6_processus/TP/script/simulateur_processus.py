"""
NSI lycée Saint-André
TP OS & processus
Script avec menu permettant de visualiser les différents états d'un processus
"""

# Librairies utilisées
import os 
import subprocess
from multiprocessing import Process
from threading import Thread 
import time
import math 


def travailler(debut, fin):
    """Fonction qui fait un travail intense"""
    print("Nouveau travail demandé...")
    print(f"PID  = {os.getpid()}")
    print(f"PPID = {os.getppid()}")
    for i in range(debut, fin):
        x = math.cos( math.factorial(i) % 1000 )


# Boucle de gestion du menu
fin = False
while (fin == False):
    print("*********************************")
    print("*** Simulateur processus v0.1 ***")
    print("*********************************")
    print("Que voulez-vous faire ?")
    print("  1 - Afficher mon PID et PPID")
    print("  2 - Afficher mes variables d'environnement")
    print("  3 - Afficher le chemin où je me trouve")
    print("  4 - Afficher des salutations")    
    print("  5 - Lancer la calculatrice en tâche de fond")
    print("  6 - Faire une sieste de 10s")  
    print("  7 - Faire un travail intense")       
    print("  8 - Créer un nouveau processus fils (pour faire le travail intense)")
    print("  9 - Créer plusieurs processus fils (pour faire le travail intense)")
    print("  666 - Créer un zombie")
    print("  0 - Sortir")

    choix = int(input("> "))

    if choix == 1:
        # Affichage des PID et PPID
        print(f"PID  = {os.getpid()}")
        print(f"PPID = {os.getppid()}")

    elif choix == 2:
        # Affichage des variables d'environnement
        print(f"Variables d'environnement = {os.environ}")

    elif choix == 3:
        # Lancement d'une commande
        os.system("/bin/pwd")

    elif choix == 4:
        # Lancement d'une commande et récupération du résultat
        nom = subprocess.run(["whoami"], capture_output=True)
        print(f"{nom.stdout.decode().strip()} vous salue bien")  

    elif choix == 5:
        # Lancement de la calculatrice en tâche de fond
        os.system("/usr/bin/gnome-calculator &")

    elif choix == 6:
        # Sieste de 10s
        time.sleep(10)

    elif choix == 7:
        # Boucle de travail intense
        t1 = time.perf_counter()
        travailler(0, 9000)
        t2 = time.perf_counter()
        print(f"Temps (en s) => {t2 - t1}")

    elif choix == 8:
        # Création d'un processus fils pour faire le travail intense
        t1 = time.perf_counter()
        mon_fils = Process(target=travailler, args=(0, 9000) )      
        mon_fils.start()
        mon_fils.join()
        t2 = time.perf_counter()
        print(f"Temps (en s) => {t2 - t1}")

    elif choix == 9:
        # Création d'un processus fils pour faire le travail intense
        t1 = time.perf_counter()
        mon_fils1 = Process(target=travailler, args=(0, 3000) ) 
        mon_fils2 = Process(target=travailler, args=(3001, 6000) ) 
        mon_fils3 = Process(target=travailler, args=(6001, 9000) )      
        mon_fils1.start()
        mon_fils2.start()
        mon_fils3.start()
        mon_fils1.join()
        mon_fils2.join()
        mon_fils3.join()
        t2 = time.perf_counter()
        print(f"Temps (en s) => {t2 - t1}")

    elif choix == 666:
        # Création d'un zombi
        un_zombie = Process(target=travailler, args=(0, 1) )      
        un_zombie.start()
        # C'est parce qu'il n'y a pas de join(), que le processus devient zombie

    elif choix == 0:
        fin = True

    else:
        print("/!\\ Choix invalide /!\\")
