---
title: " 17 Programmation dynamique et paradigmes de programmation"
subtitle: "cours"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
|Contenus|Capacités attendues|Commentaires|
|---|---|---|
|Programmation dynamique. |Utiliser la programmation dynamique pour écrire un algorithme. |Les exemples de l’alignement de séquences ou du rendu de monnaie peuvent être présentés.La discussion sur le coût en mémoire peut être développée.|
|Paradigmes de programmation.|Distinguer sur des exemples les paradigmes impératif, fonctionnel et objet.Choisir le paradigme de programmation selon le champ d’application d’un programme.|Avec un même langage de programmation, on peut utiliser des paradigmes différents. Dans un même programme, on peut utiliser des paradigmes différents.|

# Programmation dynamique
## Mise en situation
Dans la méthode __"Diviser pour régner"__, nous avons vu qu'on divise un problème en sous-problèmes indépendants (qui ne chevauchent pas),on résout chaque sous-problème , et on combine les solutions des sous-problèmes pour former une solution au problème initial.

Il y a cependant des cas où les sous problèmes ne sont __pas indépendants__.On peut alors retrouver un même sous-problème dans des appels récursifs, et donc être amené à le résoudre plusieurs fois, ce qui semble être du gaspillage de ressources.  
Prenons en exemple l'étude de la suite de Fibonacci qui est définie par :

$`u_n = \left\{ \begin{array}{cl}
0 &  \ si\  n \ =  0 \\
1 &  \ si\  n \ =  1 \\
u_{n-1} + u_{n-2} & \;  si\  n \ \geqslant 2
\end{array} \right.`$

La fonction récursive qui nous vient rapidement est :
```python
def fibo(n):
    if n == 0 or n == 1 :
        return n
    else :
        return fibo(n-1) + fibo(n-2)
```
Etudions le cas pour n = 5 et essayons de réprésenter les appels successifs de cette fonction sous la forme d'un arbre.  
![](./img/tree-01.png)

On observe ainsi qu'il y a plusieurs appels avec le paramètre 2 ( 3 appels) et avec le paramètre 3 ( 3 appels)

## Méthode de Bellman (1950)
Pour éviter ces appels récurssifs redondants et couteux, l'américain Richard Bellman a eu une idée relativement simple : on va __mémoriser les résultats des sous problèmes__ afin de ne pas les calculer plusieurs fois. Cette technique n'est valable que si les sous-problèmes sont dépendants.  

📢 La mémorisation des sous-problèmes est une sorte de mémoire cache implémentée sous forme de tableau ou de liste. Cette technique porte le nom de programmation dynamique.

### Programmation dynamique de forme récursive appelée __Top Down__
* Celle-ci de déroule du haut vers le bas. 
* On appelle directement la formule de récurrence.  
Lors d'un appel récursif, avant d'effecteur le calcul, on vérifie dans la liste de la mémoire cache si ce calcul n'a pas déjà été fait.  

_Remarque : cette forme est appelée aussi technique de __mémoïsation__._

Dans notre exemple, on va donc mémoriser les termes d'indices 0,1, ...., n. Nous allons stocker ces résultats intermédiaires dans une liste de (n + 1) éléments initialisés à 0.
```python
def fibo_top_down(n):
    stock = [0]* (n+1)
    return fibo_top_down_rec(n, stock)
```
Cette fonction déclare notre liste de mémoire cache et qui réalise le premier appel à la fonction récursive qui effectue le calcul.  

```python
def fibo_top_down_rec(n,stock):
    if n == 0 or n == 1:
        stock[n] = n
        return n
    else : 
        if stock[n] > 0 : 
            return stock[n]
        else : 
            stock[n] = fibo_top_down_rec(n-1,stock) + fibo_top_down_rec(n-2,stock)
            return stock[n]
```
On remarque qu'on a simplement par rapport à  la récursivité pure, la fonctionnalité majeure de l'efficacité  afin d'utiliser les résultats.  
Voir sur [Python Tutor](https://urlz.fr/qnLf) l'exécution.  
On pourra comparer avec le premier code proposé le nombre d'étapes.

![](./img/fibo_top_down.png)  
On remarque sur le screen ci-dessus les appels récursifs de la fonction.

### Programmation dynamique de forme itérative appelée __Bottom Up__  
* Cette fonction est cette fois itérative et se réalise du bas vers le haut  
* on résout en premier les problèmes du plus petit niveau, puis ceux du niveau supérieur et au fur et à mesure, on mémorise ces résultats dans la liste de mémoire cache.  
* on continue jusqu'au niveau qui nous intéresse.

Pour notre exemple sur la suite de Fibonacci, cela donne : 
```python 
def fibo_bottum_up(n):
    stock = [0]*(n+1)
    stock[1] = 1
    for i in range(2, n+1):
        stock[i] = stock[i-1] + stock[i-2]
    return stock[n]
```
Voir sur [Python Tutor](https://urlz.fr/qnMr) l'exécution. Le nombre d'étapes est vraiment limité !  
La liste `Stock` permet bien de mémoriser les étapes de calculs.  

![](./img/fibo_bottum_up.png)  

# Problème du rendu de monnaie  
Ce problème a été traité en 1_NSI avec la notion des algorithmes gloutons. Nous avions obtenu une réponse mais celle-ci n'était pas forcément optimale.  

La question est : __" quel est le nombre minimale de pièces à utiliser pour rendre une somme donnée ?"__
On part d'une liste de pièces ou billets possible dans un système monétaire connu :  
$`L = [p_{1}, p_{2}, p_{3},....., p_{n}]`$  
Par exemple dans la zone Euro : L = [0.01, 0.02, 0.05, 0.10, 0.20, 0.50, 1, 2, 5, 10, 20, 50, 100, 200, 500 ]  

## Relation de recurrence
Etablissons tout d'abord une relation de récurrence :  
pour rendre une somme S, on va noter `nb(S)` ce nombre __minimal de pièces__.  
A partir de S, on peut choisir de rendre d'abord $`p_{1}`$ ou $`p_{2}`$ ou $`p_{n}`$. Les sommes inférieures à S sont donc S - $`p_{1}`$, S - $`p_{2}`$ , ..... S - $`p_{n}`$. On peut alors trouver la récurrence suivante : 

$`nb(S) = \left\{ \begin{array}{cl} 
0 & si\;  S \; = 0  \\ 
1 + min(nb(S - p_{i})) \;  & si \; S \; >  0 \; avec \; i \in [1,n] \; et \; p_{i} < S  \\
\end{array}\right.`$ 



On peut donc proposer la fonction suivante permettant de rendre la somme S avec les pièces ou billets dans la liste L = [1, 2, 5, 10, 20, 50, 100, 200, 500 ]   :
```python
def nb_rendu_monnaie(L, S):
    ''' renvoie le nombre  minimum de pièces de la liste L afin de rendre la somme S
    : L : (list)
    : S : (int or float)
    @return (int)
    '''
    if S == 0 :
        return 0
    else : 
        mini = S+1 # on choisit une valeur min très grande pour le nb de pièces/billets
        for i in range(len(L)):
            if L[i]<=S : # on choisit une pièce/billet de valeur < S
                nb = 1 + nb_rendu_monnaie(L, S-L[i])
                if nb < mini :
                    mini = nb
        return mini
```
On voit ci-dessous le nombre d'appels récursifs nécessaire pour rendre la somme de 5 euros  .   (_on appelle n la fonction pour davantage de visibilité_)  

![](./img/tree-02.png)  

On observe bien la réponse : un billet de 5 euros puisqu'on obtient la feuille avec un appel de somme S = 0. Les différentes profondeurs de n(L,0) donnent les différentes solutions pour rejoindre le cas d'arrêt.

Voir sur [Python Tutor](https://urlz.fr/qnVS) l'exécution.  

## Avec la méthode dynamique
Pour utiliser cette méthode on va mémoriser les résultats pour les sommes 0, 1, .... S.
On stocke ainsi ces résultats intermédiaires dans une liste de S+1 éléments initiés à 0

```python
def rendu_monnaie_top_down(L,S):
    stock = [0]*(S+1)
    return rendu_monnaie_top_down_rec(L,S, stock)

def rendu_monnaie_top_down_rec(L,S, stock):
    if S == 0:
        return 0
    elif stock[S]> 0: #si elle a été remplie
        return stock[S]
    else :
        mini = S + 1
        for i in range(len(L)):
            if L[i]<=S : # pour une pièce ou billet inférieur à S
                nb = 1 + rendu_monnaie_top_down_rec(L, S-L[i],stock)
                if nb < mini :
                    mini = nb
                    stock[i] = mini
        return mini 
```
On observe les appels, bien moins importants que précédemment. Les nombres inscrits sur les arêtes sont les pièces choisies.

![](./img/tree-03.png)

[Python Tutor](https://urlz.fr/qnWo)

On pourrait utiliser une autre variable dans laquelle on stockerait les valeurs des pièces pour réaliser le rendu. Cela peut ressembler à un parcours de graphe ...  

📢 La programmation dynamique est une technique pour améliorer l'efficacité d'un algorithme en évitant les calculs redondants. Pour cela, on stocke dans un tableau les résultats intermédiaires du calcul, afin de les réutiliser au lieu de les recalculer. Les problèmes du rendu de monnaie ou de l'alignement de séquences sont deux exemples où la programmation dynamique améliore grandement l'efficacité par rapport à une solution récursive.


# Les paradigmes de programmation
Un __paradigme__ est un point de vue particulier sur la réalité, un angle d'attaque privilégié sur une classe de problèmes, un état d'esprit. 
Chaque paradigme __privilégie un ensemble de stratégies d'analyse ou de réflexion__.  
La thèse de Turing Church formulée en 1936 met en évidence l'__équivalence__ de l'ensemble de ces paradigmes. Le choix du langage de programmation doit se faire avant tout en fonction de la nature du problème à appréhender.
En voici quelques uns :
## Paradigme impératif 
📢 La programmation impérative décrit les différentes opérations sous forme de suites d'instructions exécutées par la machine sur laquelle est lancé le programme pour modifier l'état du programme.

L'exécution étape par étape est nommée séquentielle.
C'est le type de programmation le plus utilisé. La plupart des _processeurs_ sont construits de manière à exécuter des suites d'instructions. 
On peut observer des effets de bord( modification faite par une fonction sur une dépendance partagée) ce qui peut provoquer de scomportements non prévus.  

LANGAGES : C, Pascal, Python, COBOL, Fortran

## Paradigme fonctionnelle
📢 La programmation fonctionnelle est un concept de programmation ayant pour but principal d'éviter les effets de bord. Pour cela, elle se concentre essentiellement sur les fonctions, d'où son appellation : elle s'interdit toute variable globale pour ne pas faire appel qu'à des fonctions, qui peuvent être imbriquées.

Exemple avec la méthode impérative :  
```python
variable = 0
def plus():
    global variable
    variable = variable + 1
plus()
print(variable)
```

Même exemple avec la méthode fonctionnelle :
```python
def plus(variable):
    return variable + 1

print(plus(0))
```

LANGAGES :  Lisp, LOGO, OCaml, Haskell, Python


## Paradigme logique
📢Ce paradigme est fondé sur la logique formelle définie au début du XXème siècle pour traiter des problèmes posés par les fondements des mathématiques. Le langage emblématique de ce paradigme est Prolog. 

Le programmateur définit un ensemble de faits élémentaires et de règles de logique leur associant des conséquences. Il permet d'écrire des programmes très concis et élégants car proches de la description mathématique de l'algorithme à l'origine du programme. L'usage de ces langages reste toutefois confidentiel.

LANGAGES : Prolog, OZ, CLIPS, PyPy

## Paradigme Diviser pour régner  
📢 Des problèmes complexes se résolvent de manière récursive sur des données de tailles toujours plus inférieures indépendantes les unes des autres,  jusqu'à des problèmes de taille élémentaire, résolvable immédiatement. On combine ensuite les résultats.


## Paradigme programmation orientée objet
Le programmeur identifie les "acteurs" qui composent un problème et détermine ce qu'est et ce que doit savoir chaque acteur. En regroupant les aspects communs puis en les spécialisant, le programmeur établit une _hierarchie_ de classe.  

 📢La POO est un paradigme de programmation qui consiste à considérer un programme comme un ensemble d'objets en interaction.

LANGAGES : SmallTalk, C++, Java, Python



|Désignation|Description|Compléments|
|:---|:---|:---|
|Programmation impérative|suite d'instructions exécutées par la machine|programmation intuitive|
|Programmation fonctionnelle|évite les effets de bords|pas de variables globales|
|Diviser pour régner|découpe un problème en plusieurs sous problèmes indépendants|utilisation de la récursivité|
|Programmmation orientée objet|considère un programme comme une collection d'objets en interaction|utilisation de classes|
|Programmation dynamique|optimise un programme en temps et en mémoire|utilisation de la forme récursive dans le _top down_ ou la forme impérative en _bottom up_|

Sources : 
* NSI J.C. Bonnefoy B. Petit ELLIPSES
* NSI T. Balabonski S Conchon J.C. Filliâtre K. Nguyen ELLIPSES
* NSI S. Pasquet M. Leopoldoff Nathan