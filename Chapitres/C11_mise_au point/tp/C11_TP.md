---
title: " : Langage et programmation : 11 Mise au point et optimisation des programmes"
subtitle: "TP"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Bugs
Trouvez le bug dans chaque programme, d'abord en le lisant, puis en l'exécutant.

```python
#1
x = 0.0
while x != 1.0:
    print(x, "au carré fait environ", x * x)
    x = x + 0.1

#2
def inverse(x):
    return 1 / x

for x in range(-4,5):
    print(x, "a pour inverse", inverse(x))

#3
compteur = 0

def incrementer_compteur():
    compteur = compteur + 1

incrementer_compteur()
incrementer_compteur()
incrementer_compteur()
print(compteur)

#4
def renverser(T):
    i, j = 0, len(T)-1
    while i < j:
        T[i], T[j] = T[j], T[i]
        i, j = i+1, j-1

T = (62, 81, 53, 74, 40)
renverser(T)
print(T)

#5
def cube(a):
    y = a * a * a

x = 5
y = cube(x)
print("Le cube de", x, "est", y)

#6
def decaler(tableau):
    for i in range(1, len(tableau)):
        tableau[i] = tableau[i-1]
    return tableau

print(decaler([1, 2, 3, 4, 5]))

#7
T = [19, 25, 30, 46, 78]
i = 0
while i <= len(T):
    print("T[{}] = {}".format(i, T[i]))
    i = i + 1

#8
def convertir_pouce_en_cm(x):
    return 2.56 * x

mesure_pc = 85
mesure_cm = convertir_pouce_en_cm(mesure_pc)
print(mesure_pc, "pouces", "fait environ", \
      mesure_cm, "cm")

#9
def verifier_mot_de_passe(mdp):
    valide = False
    if mdp == "Deneb":
        va1ide = True
    return valide

while True:
    password = input("Saisir le mot de passe : ")
    if verifier_mot_de_passe(password):
        break

print("Accès autorisé")

#10
def calculer_prix_reduit(tarif, age):
    prix = tarif
    if age < 10:
        reduction = 20
    elif age < 16:
        reduction = 10
    return prix - reduction

tarif = 200
age = int(input("Quel est votre âge ? "))
prix = calculer_prix_reduit(tarif, age)
print("Le prix à payer est de", prix, "€")

#11
def somme_carres(N):
    somme = 0
    for n in range(1, N+1):
        carre = n * n
    somme += carre
    return somme

total = somme_carres(100)
print("La somme des carré des entiers",  "de 1 à 100 est égale à", total)

#12
def reduction(tarif, age):
    if age == 18:
        tarif = tarif - 20 / 100 * tarif
    return tarif

tarif = 25.0
age = input("Quel est votre age ?")
prix = reduction(tarif, age)
print("Vous devez payer", prix, "euros")

#13
def est_present(x, T):
    for i in range(len(T)):
        if x == T[i]:
            return True
    return False

L = [6, 3, 9, 2, 7, 5, 0, 1, 8, 4]
if est_present(L, 5):
    print(5, "est présent dans la liste")
else:
    print(5, "est absent de la liste")

#14
def calculer_tarif(age):
    tarif = 3.60
    if age >= 12:
        tarif = 4.20
    elif age >= 18:
        tarif = 5.80
    return tarif

print("0 à 12 ans", calculer_tarif(10), "€")
print("12 à 18 ans", calculer_tarif(16), "€")
print("18 ans et +", calculer_tarif(20), "€")

#15
def convertir_francs_en_euros(franc):
    return francs / 6.55957

somme_fr = 100
somme_eu = convertir_francs_en_euros(somme_fr)
print(somme_fr, "F fait environ", somme_eu, "€")

#16
def est_present(x, liste):
    for element in liste:
        if x == element:
            return True
        else:
            return False

print(est_present(8, [5,10,15,20]))

#17
def nom_complet(fiche):
    nom = "inconnu"
    if "prenom" in fiche:
        nom = fiche["nom"]
        prenom = fiche["prenom"]
    return prenom + " " + nom

def afficher_nom(fiche):
    nom = nom_complet(fiche)
    print(nom)

fiche1 = {"nom":"Fonce", "prenom":"Al"}
fiche2 = {"prenom":"Bil", "ville":"Brest"}
afficher_nom(fiche1)
afficher_nom(fiche2)

#18
def rechercher(x, T):
    debut, fin = 0, len(T)
    while fin - debut > 1:
        milieu = (debut + fin) // 2
        if T[milieu] <= x:
            debut = milieu
        else:
            fin = milieu
    if T[debut] == x:
        return debut
    return -1


L = [15, 42, 31, 36, 47, 29, 50]
i = rechercher(36, L)
print(36, "a pour indice", i)
```

# Découpages...

On souhaite réaliser une fonction qui renvoie la liste des mots d'une phrase. On considère la proposition de code source suivante.

```python
def decouper(phrase):
    L = []
    debut, fin = 0, 0
    while fin < len(phrase):
        while phrase[fin] != " ":
            fin += 1
        mot = phrase[debut:fin]
        L.append(mot)
        debut, fin = fin + 1, fin + 1
    return L
```

1. Donnez la signification des variables : L , debut, fin, mot.

2. À quoi sert la boucle while la plus imbriquée ?

```

```

3. Appliquez à la main la fonction decouper() sur la chaîne "Un bel été" et notez les valeurs des variables juste après l'exécution de la ligne L.append(mot).

|n° d'itération|debut|fin|mot|L|
|---|---|---|---|---|
|1|||||
|2|||||
|3|||||

4. Quel bug constate-t-on et quelle en est l'origine ?

5. Quelle correction faut-il apporter à la fonction ?
	
6. Testez votre modification en exécutant le programme suivant.

```python
proverbe = "Lorsqu'on s'occupe d'informatique il faut faire comme les canards... Paraître calme en surface et pédaler comme un forcené par en dessous."
mots = decouper(proverbe)
print(mots)
```

# Docstring et annotations de type
1. Améliorer le code suivant avec des annotations de type et une documentation :  

```python
def f(t):
    


    
    a = 0
    for i in range(len(t)):
        a = a + t[i]
    return a


```

2. Écrire le code de la fonction dont la docstring est la suivante :

```python

    '''
    Calcule et renvoie la moyenne des valeurs d'un tableau de nombres
    Parameters:
        t : (list) Liste de nombres
    Returns:
        (int) Moyenne
    '''









```

# Préconditions, postconditions

1. Rappeler ce que sont les préconditions et les postconditions d'une fonction.

```


```

2. Soit le code suivant : 

```python
def f(t):
    a = 0
    for i in range(len(t)):
        if t[i] > t[a]:
            a = i
    return a
```

2.1. Que vaut f([0, 9, 4]) ?

```


```
2.2. Améliorer la lisibilité du code de cette fonction en choisissant des noms parlants, en ajoutant des annotations de type et une docstring complète pour la fonction.
```






```

2.3. Que renvoie la fonction :

    - avec le paramètre t1 = [-9, -4, -1] ?
    - avec le paramètre t2 = [5, 0] ?
    - avec le paramètre t3 = [] ?


2.4. Préciser les préconditions pour la fonction.

'''

'''

# Assertions

On considère la fonction dont le début du code est donné ci-dessous :
```python

from random import randint
def liste_uniques(N, d, f):
    """
    Renvoie une liste de N nombres entiers aléatoires entre d et f sans aucun doublon.
    """










```

1. Quelles sont les préconditions sur les paramètres de cette fonction ?

```


```  

2. Compléter le code de la fonction.

3. Ajouter des assertions au code de la fonction afin que le programme soit interrompu si les préconditions ne sont pas respectées.

# Tests 1

On souhaite écrire une fonction qui prend une liste non vide de nombres entiers en paramètres et renvoie la somme des nombres de cette liste.

1. Proposer une série de tests (en utilisant le module doctest) pour cette fonction.

2. Quel est l'invariant de boucle ?

3. Écrire le code de la fonction (y compris sa docstring avec les tests)  en rajoutant à la fois la précondition et la postcondition ainsi que l'invariant de boucle sous la forme de trois assertions dans le code. (indication : utilisez sum() et le slic ing). Ne pas oublier le programme principal pour valider les tests.  

```










```

# Tests 2 ( Thonny )

La fonction suivante est censée tester si une valeur v est présente dans une liste t.

```python
def est_dans(v, t):
    i = 0
    while i < len(t) - 1 and t[i] != v:
        i = i + 1
    return  i < len(t)
```

1. Rédiger des tests pour cette fonction afin de montrer qu'elle est incorrecte.

2. Corriger la fonction.

# Tests et assertions

On considère la fonction suivante :

```python
def t(b):
    




    
    
    
    a = 0
    for i in range(len(b)):
        a = a + b[i]
    return a

```

1. Améliorer la lisibilité du code de cette fonction.

2. Écrire un programme principal avec une série de tests sous forme d'assertions.

# Somme (Thonny)
On considère le programme ci-dessous :

```python
def somme(n):
    '''
    @param n : (int) un entier positif ou nul.
    '''
    s = 0
    for i in range(0,n+1):
        s = s + i
    return s

# === Programme principal ===
def est_correcte():
    from random import randint
    liste = [randint(0,100) for _ in range(21)]
    for n in liste:
        assert somme(n) == n*(n+1)/2
    return True

if est_correcte():
    print("La fonction donne bien le résultat attendu !")
```
1. Que fait la fonction somme ? Compléter la docstring de cette fonction.

2. Que fait la partie principale du programme ? Ajouter des commentaires au code.

3. Modifier le programme pour que la valeur limite 0 soit systématiquement testée.

# Tri d'une liste  (Thonny)

On considère une fonction `tri(liste)` qui prend une liste d'entiers en paramètre et trie par ordre croissant les éléments de cette liste.

On souhaite écrire une fonction qui automatise le test de la fonction tri().

Ainsi, cette fonction devra :

* générer une liste aléatoire de 10 entiers ;  
* faire une copie de la liste (cela se fait avec le code l2 = l1[:]) ;  
* appliquer la fonction tri() à la liste ;  
* vérifier à l'aide d'une assertion que la liste triée l'est effectivement ;  
* vérifier à l'aide d'une assertion que la liste triée contient les mêmes éléments que la liste initiale.  

Ecrire le code de cette fonction.

# Matplotlib (Thonny)
Copier-coller le programme du cours utilisant le module matplotlib pour le tester.
Modifier le programme pour :

* qu'il affiche simultanément les courbes $`y = x^{2} , y = x\times log_{2}(x), y = x`$ ; 
*  que les valeurs de x varient de 1 en 1 (et non de 5 en 5) ;
* que les points ne soient pas visibles, mais que l'on voit la trace des courbes ;
* que chaque courbe ait une couleur différente ;
* qu'il y ait une légende pour les courbes.

# Mesure de temps d'exécution  (Thonny)

En utilisant la méthode `perf_counter()`du module `time` : 
1. Écrire un programme qui permet de mesurer le temps mis pour créer la liste des nombres entiers de 0 à 9999.
2. Écrire un programme qui permet de mesurer le temps mis pour trier, à l'aide de la méthode sort(), une liste de 1000 nombres entiers aléatoires entre 0 et 99.

# Coût de la création d'une liste  (Thonny)

1.1.  Écrire une fonction mesure_temps_comp(n) qui prend un entier positif n en paramètre et renvoie le temps mis pour créer la liste des nombres entiers de 0 à n par compréhension.

1.2. Écrire un programme qui permet de visualiser sous la forme d'un graphique le temps mis pour créer une liste en compréhension en fonction de la taille de la liste.

1.3. Comment évolue le coût de la création d'une liste en compréhension en fonction de la taille de la liste ? Pouvait-on s'attendre à ce résultat ?

2.Reprendre les trois questions précédentes pour la création de la liste des nombres entiers de 0 à n en utilisant, cette fois, une boucle for et la méthode append()).

3.Écrire un programme qui trace les deux courbes précédentes sur le même graphique.  

# Coûts de la recherche d'une valeur dans une liste triée

## Rappels des algorithmes

1. Algorithme séquentiel

A faire : Écrire une fonction qui prend une liste d'entiers triée et un entier en paramètres, et renvoie la position de l'entier dans la liste (ou None si le nombre n'est pas dans la liste). On n'utilisera pas les méthodes de l'objet list (la fonction len() est autorisée).  

2. Algorithme dichotomique

A faire : Écrire la même fonction que la précédente, mais en utilisant l'algorithme de recherche par dichotomie.

## Comparaison de la complexité des deux méthodes
1. Par une analyse de code, estimer la complexité de chacun des deux algorithmes et préciser si elle est de type $` n^{2} ,\:  n\: ou\: log_{2}(n) `$

2. Écrire une fonction qui prend un nombre entier n en paramètre et renvoie une liste triée de n entiers aléatoires compris entre 0 et 9999.

3. Coût de l'algorithme séquentiel.

3.1. En utilisant les fonctions précédentes , écrire une fonction qui prend un entier n en paramètre et renvoie le temps mis pour rechercher le nombre 9999 dans une liste triée de n nombres entiers compris entre 0 et 9999.

Remarque : on recherche volontairement le nombre 9999 pour avoir le pire cas.

3.2. Écrire le programme principal qui permet de visualiser l'évolution du coût de la recherche séquentielle.

3.3. Les hypothèses faites dans la question 1) sont-elles confirmées ?

4. Coût de l'algorithme dichotomique.

Reprendre les questions précédentes en les appliquant à l'algorithme de recherche dichotomique.

5. Écrire un programme qui permet de visualiser, sur un même graphique, l'évolution du coût de la recherche d'un élément dans une liste par les deux méthodes.

6. Pour aller plus loin : comparaison avec la méthode index.

La fonction ci-dessous permet de faire une recherche d'une valeur dans une liste triée en utilisant la méthode `index()` des listes.

```python
def recherche_index(liste, nb):
    try:
        return liste.index(nb)
    except(ValueError):
        return None
```

6.1. Compléter le programme précédent avec une troisième courbe qui correspond à la recherche utilisant index().

6.2. Comment le coût de la méthode index() évolue-t-il en fonction de la taille des données ?

Remarque : le coût de la méthode index() est moins bon que celui de la recherche dichotomique car la méthode index() fonctionne aussi bien sur les listes triées que sur les listes non triées.

Sources :  

* Cours NSI A Wilm lycée Beaupré Haubourdin
* Cours NSI Jean Diraison  
