---
title: "11 Mise au point et optimisation des programmes"
subtitle: " Langage et programmation : cours"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
![BO](./img/BO.jpg)

Un code bien lisible doit contenir :

* des commentaires (ni trop peu, ni trop !) ;  
* des noms de variables, des noms de fonctions... qui aident à la compréhension du programme ;  
* des annotations de type pour les fonctions ;  
* des docstrings pour les fonctions, pour les classes, pour les modules...  

La gestion des bugs est un des points fondamentaux de l'informatique.  Les ordinateurs sont des objets d'une complexité extrême et concevoir un logiciel complexe (comme un système d'exploitation ou un navigateur internet) sans bugs est une tâche quasiment impossible.

Les bugs font donc partie du quotidien du développeur. Il doit apprendre à composer avec tout en essayant de minimiser leur apparition car ceux-ci peuvent avoir parfois des conséquences tragiques.

Voici cette passionnante introduction de Gérard Berry du Collège de France sur le sujet : [lien vers la vidéo](https://youtu.be/qE0QFxNiQLI)  

# Conventions syntaxiques

La programmation est un art délicat : un simple caractère en trop peut provoquer une erreur pour le code tout entier (penser à un innocent caractère d'espace en début de ligne dans un code Python).

Mais même lorsqu'un code s'exécute sans erreur, il ne faut pas négliger l'aspect purement «esthétique» de celui-ci : il est nécessaire de respecter autant que possible des conventions typographiques, qui vont standardiser le code et le rendre ainsi plus lisible.

Ainsi pour chaque langage, il existe une «bible» de bonnes pratiques de présentation du code, qui visent à l'uniformiser. Pour Python, cette référence s'appelle la __Python Enhancement Proposal 8__, plus connue sous le nom de PEP8.

En voici quelques extraits :

## Les espaces

--> Il faut mettre une espace (oui, en typographie, on dit «une» espace et non pas «un» espace) avant et après chaque opérateur de comparaison, d'affectation, ou mathématique (=, ==, >, +, *, ... )

```python
# PAS BIEN 
a=3

# BIEN 
a = 3

# PAS BIEN
if x>3:
    print("ok")

# BIEN
if x > 3:
    print("ok")
```

--> Pour les opérateurs mathématiques, on essaie de reconstituer les groupes de priorité (lorsqu'il y en a) :

```python
# PAS BIEN
x = 3*2

# BIEN
x = 3 * 2

mais

# PAS BIEN
x = 3 * 2 + 5

# BIEN
x = 3*2 + 5
```

--> On ne met pas d'espace à intérieur des parenthèses, des crochets ou des accolades :

```python
# PAS BIEN
for x in range( 5 ):
    print( 'bonjour' )

# BIEN
for x in range(5):
    print('bonjour')
```

--> Pour les virgules, et les deux points : pas d'espace avant mais une espace après.

```python
# PAS BIEN
if color == (0,255,0) :
    print('vert')

# BIEN
if color == (0, 255, 0):
    print('vert')
```

On peut contrôler si son code vérifie les standards de la PEP8 sur ce site http://pep8online.com/

## Les conventions de nommage

--> Les variables à une lettre (comme i, j, k ) sont réservées aux indices (notamment dans les boucles).

--> Les autres variables doivent avoir des noms explicites, éventuellement écrits en snake_case si plusieurs mots doivent être reliés.

```python
# PAS BIEN
if d == 1:
    cep = cep + vm

# BIEN
if date == 1:
    compte_epargne = compte_epargne + versement_mensuel
```

Rappel des différents types de casse :

* __snake_case__ : les mots sont séparés par des underscores. Conseillé en Python.  
* __camelCase__ : les mots sont séparés par des majuscules mais la 1ère lettre est minuscule. Conseillé en Javascript.  
* __PascalCase__ : les mots sont séparés par des majuscules et la 1ère lettre est majuscule. Conseillé en C.  
* __kebab-case__ : les mots sont séparés par des tirets courts. Conseillé en HTML - CSS.  

--> Cas particulier des classes en Programmation Orientée Objet : leur nom doit commencer par une majuscule.

```python
# PAS BIEN
class voiture:
    def __init__(self, annee, marque, modele):
       #pass

# BIEN
class Voiture:
    def __init__(self, annee, marque, modele):
       #pass
```

# Les bugs
Malgré tous les efforts de rigueur et d'attention, il arrive cependant que le programme ne fonctionne pas comme attendu, en donnant une réponse erronée, en s'arrêtant brusquement (le programme plante) ou en ne répondant plus (il se fige ou ne réagit plus au évènements extérieurs). C'est le signe de la présence d'une erreur de programmation, d'__un bug__.

Voici une liste non exhaustive de bugs auxquels vous pouvez être confronté.

* Les erreurs de syntaxe.
    * oublier une lettre dans le nom d'une fonction : prnt() au lieu de print().
    * confondre les noms de variables temps_0 (zéro) avec temps_O (lettre o).
    * oublier le caractère deux-points : .
    * déplacer une instruction à l'intérieur ou à l'extérieur d'un bloc par erreur.

* Les erreurs d'initialisation de variable.
    * utiliser une variable sans l'avoir initialisé :  utiliser n = n+1 sans avoir n = 0
    * donner une mauvaise valeur au départ : tab = [] suivi d'un tab[0] = 3

* Les erreurs de type.
    * oublier que certains types ne sont pas mutables : p = (4,5) puis p[0] = 8
    * effectuer des opérations qui n'existent pas : chaine = "abc" * 2.5

* Les erreurs de débordement.
    * utiliser un indice trop grand : L = [3, 4] suivi de L[2] = 5
    * utiliser une clé inexistante :  D = {"a":1, "b":2} suivi de n = D["c"]

* Les erreurs de boucles.
    * mauvaise plage d'indice : for i in range(n) au lieu de range(1,n+1)
    * boucle infinie : while condition avec condition toujours à True

* Les erreurs d'instruction conditionnelle.
    * omission d'un cas d'étude : oubli d'un else ou d'un elif.
    * mauvaise formulation de la condition : <= , > , not, and, or, parenthèses

* Les erreurs de calcul.
    * les calculs sur les flottants sont approximatifs : 0.1 + 0.2 != 0.3
    * certaines fonctions mathématiques peuvent échouer : partage = 5 / 0

* Les erreurs d'utilisation des fonctions.
    * mauvais passage d'arguments : s.index(1,5,"a") mais s.index("a",1,5)
    * non prise en compte des préconditions : math.sqrt(-1)

* Les erreurs d'effets de bord.
    * modifier une variable globale dans une fonction.
    * modifier le contenu d'une liste passée en argument sans faire attention.

* Les erreurs d'algorithmes.
    * mauvaise formule mathématique : aire = Pi\*r\*r et pas (2\*Pi\*r)
    * algorithme faux ou mauvaise implémentation.

# Typage des données

Les __annotations de type__ permettent à l'utilisateur de fonction de savoir quels sont les types des paramètres et quel est le type de la valeur renvoyée, et ce de façon concise en écriture.

Remarque : les annotations de type (typing hints, en anglais) ont été introduites dans la versions 3.5 de Python et sont encore en évolution.  

Python en apparence ne __se soucie pas des types__ : lorsqu'on initialise une variable à aucun moment on ne doit déclarer son type et à tout moment, on peut écraser le contenu d'une variable par une valeur d'un autre type.
exemple :  

```python
>>> a="Hello, World"
>>> print(type(a))
<class str>
>>> a = 3 + 2
>>> print(type(a))
<class int>
```

>__📢 Définition  :__
Il est possible de réaliser en Python des __annotations de type__. Ces annotations constituent une indication pour le développeur. Pour l'instant ces annotations ont uniquement un rôle de documentation car l'Environnement de Développement Intégré ( EDI) ne fait aucune vérification de cohérence. Mais c’est clairement la voie à suivre pour enrichir la documentation et l’utilisabilité du code.


```python
def maxi(l:list) -> float:
    if l == []:
        return None
    else:
        nb_max = l[0]
        for i in range(1, len(l)):
            if l[i] > nb_max:
                nb_max = l[i]
        return nb_max
```

# Les docstrings (rappels)

Voici un exemple de code d'un module avec l'emplacement des docstrings :

```python
"""
Docstring du module lorsque le module est importé dans un autre fichier.
"""

class MaClasse(object):
    """
    Docstring de la classe (avec les informations sur les paramètres du constructeur de la classe)
    """
    def __init__(self, args):
        pass

    def methode_de_maclasse(self, args):
        """
        Docstring de la méthode de classe
        """
        pass


def ma_function():
    """
    Docstring de la fonction
    """
    pass
```

>__📢 Définition  :__ On appelle la fonction `help()` en ligne de commande pour afin d'afficher le contenu d'une docstring. On peut aussi utiliser `fonction.__doc__`

```python
>>> len.__doc__
'Return the number of items in a container.'
>>> help(len)
Help on built-in function len in module builtins:

len(obj, /)
    Return the number of items in a container.
```

# Les assertions

## Définition

>__📢 Définition  :__ En informatique, une __assertion__ est une condition qui doit être vraie pour que l'exécution du programme se poursuive. Dans le cas contraire, le programme est arrêté.

Les assertions sont en général utilisées dans les fonctions pour s'assurer que les préconditions sont bien respectées.

## Les assertions en Python

En Python, on utilise l’instruction `assert`

```python
assert condition, 'Texte (facultatif) affiché lorsque le programme est interrompu si la condition n\'est pas respectée'
```

Si la condition n'est pas respectée, le programme est interrompu avec une erreur de type _AssertionError_ et le texte est envoyé pour affichage dans la ligne de commande.

```python
b = 5
assert b == 8, "La variable b n'est pas égale à 8"
print("Le programme se poursuit")
```

Dans cet exemple, la ligne 3 n'est pas exécutée car le programme est interrompu à la ligne 2 car l'affirmation b == 8 n'est pas vérifiée.

# Les tests

L'utilisation de tests, en particulier pour les fonctions, est une étape importante. Elle permet de limiter les risques de bugs lors de l'écriture ou lors de la mise à jour du code.

Il est en général souhaitable d'écrire une série de tests dès que les spécifications de la fonction sont établies, __avant de coder__.

Le choix des tests est important :

* les cas particuliers donnés dans la spécification de la fonction doivent être testés ;
* les cas limites des préconditions de la fonction doivent être testés ;
* les tests doivent êtres variés et prendre en compte l'ensemble des situations correspondant aux préconditions de la fonction.

Par exemple :

* si le paramètre d'une fonction est un entier, on pensera à tester un entier négatif, un entier positif, ainsi que 0 et éventuellement -1 et 1 ;
* si le paramètre d'une fonction est une liste, on pensera à tester la liste vide...

## Les tests avec le module testmod

La fonction mathématique factorielle(n) donne le produit des nombres de 1 à n. De plus factoriel(0) = 1.

Voici un programme avec :

* une fonction dont on a précisé la docstring (spécifications et doctests) et qui permet de calculer la factorielle d'un nombre ;
* un programme principal avec le lancement automatique de la validation des tests.

```python
# Définition des fonctions
def fact(n):
    """
    Calcul et renvoie factoriel de n.
    Paramètre n : (int) un entier positif
    Valeur renvoyée : (int) la factorielle de n.

    Exemples :
    >>> fact(3)
    6
    >>> fact(5)
    120
    """
    pass

# Programme principal
if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose = True)
```

A faire (sur feuille puis sur ordinateur) :

1) Le choix des tests proposés vous semble-t-il judicieux ? Compléter les tests.

```





```

2) Écrire le code de la fonction.
```






```

3) Tester la fonction pour savoir si elle passe les tests.

## Les tests avec les assertions

Celles-ci permet tout d'abord de tester les __préconditions__ : 

```python
def fact(n):
    ''' '''
    assert n >= 0 , 'n doit positif'
    pass
```

Il est parfois nécessaire de procéder à des tests sans modifier une fonction (par exemple, si la fonction est dans un module à part ou si c'est une fonction récursive...). Dans ce cas, l'utilisation du module testmod n'est pas possible directement (remarque : elle pourrait se faire en englobant la fonction dans une autre fonction). Une autre manière de faire des tests est d'utiliser les assertions.  

```python
# Programme principal
if __name__ == '__main__':
    assert fact(0) == 1
    assert fact(1) == 1
    assert fact(3) == 6
    print("Tous les tests sont ok")
```

# Invariant de structure

Un invariant de structure est une propriété qui est toujours vérifiée et qui peut permettre la détection d'erreur ou la compréhension d'un code.

Dans l'algorithme de tri par sélection, au fur à mesure qu'on avance dans la liste, le début de la liste doit être déjà trié. Cette propriété - toujours vraie de par la construction de l'algorithme de sélection du minimum est un invariant de boucle. Le mettre en évidence éclaire la construction de l'algorithme.

En cas de bug, un print à cet endroit - ou mieux : un assert - permet de déceler des erreurs si cet invariant venait à être pris en défaut.

>__📢 A retenir :__ Avoir le réflexe quand on  concoit un algorithme de réfléchir à la présence de telles propriétés invariantes et à les vérifier.

# Notion de complexité

## Rappels : qu'est-ce que la complexité ?

### Valider un algorithme

Lorsqu'on écrit un algorithme, trois questions fondamentales se posent :

* Est-ce qu'il se termine => C'est ce que l'on appelle la __terminaison__.
* Est-ce qu'il donne le résultat attendu => C'est ce que l'on appelle la __correction__.
* Est-ce qu'il donne le résultat en utilisant des ressources (principalement du processeur et de la mémoire...) raisonnables => C'est ce que l'on appelle la __complexité__.

Nous allons reprendre plus en détail la notion de complexité (que l'on appelle aussi le coût) du point de vue des ressources processeur d'un algorithme.

### Définition

>__📢 Définition :__ Le coût (du point de vue des ressources du processeur) d'un algorithme est le nombre d'opérations élémentaires effectuées par un algorithme.

### Évolution du coût en fonction de la taille des données traitées

Connaître le coût d'un algorithme est particulièrement important lorsque les algorithmes traitent des données de taille variable. Dans ce cas, il est important de savoir comment le coût varie en fonction la taille des données.

Les principales évolutions que l'on rencontre sont, en notant $` n `$
la taille des données :

* coût constant ,  
* coût logarithmique : le coût est proportionnel à $` log_{2}(n) `$ ,  
* coût linéaire : le coût est proportionnel à $` n `$ ,  
* coût quadratique : le coût est proportionnel à $` n^{2} `$ ,  
* coût exponentiel : le coût est proportionnel à $` 2^{n} `$ ,  

![](./img/complexite_courbe.png)  

## Outils pour l'étude de la complexité

* Afficher une courbe avec le module matplotlib  

[Module matplotlib pour Python , tuto réalisé par A. Wilm](http://numerique.ostralo.net/python_matplotlib/index.htm)

Exemple : Tracé de la fonction

```python
import matplotlib.pyplot as plt
import math

#création de la liste pour les abscisses
liste_x = [x for x in range(1,101,5)]

#création de la liste pour les ordonnées
liste_y = [math.log2(x) for x in liste_x]

#choix des limites des axes [xmin, xmax, ymin, ymax] (facultatif)
plt.axis([0, 100, 0, 100])

#tracé du graphique
#dans 'rx ' 'r' --> rouge, 'x' --> croix, ' '--> pas de lien
plt.plot(liste_x,liste_y,'rx ')

#génération de la fenêtre du tracé
plt.show()
```

* Mesurer le temps d'exécution : `perf_counter()`  

Une manière d'étudier la complexité d'une instruction est de mesurer son temps d'exécution.

Pour cela, on peut utiliser la méthode `perf_counter()` du module `time`.

Le programme ci-dessous permet d'estimer le temps d'exécution (en s) avec une précision correcte de quelques lignes de codes.

```python
from time import perf_counter

def mesure_temps():
    nb_repetitions=1000
    temps_final = float('inf')
    for _ in range(nb_repetitions):
        debut = perf_counter()
        # ----- Code à mesurer -----

        # --------------------------
        temps = perf_counter() - debut
        if temps < temps_final:
            temps_final = temps
    return temps_final

print(mesure_temps())
```

__Sources :__  

* Cours NSI O Lecluse Lycée Salvador Allende  
* Cours NSI A Wilm Lycée Beaupré Haubourdin  
* Cours NSI G Lassus Lycée F Mauriac Bordeaux  
* NSI 24 lecons Ellipse T Balabonski, S Conchon, JC Filiatre, K Nguyen  
* https://nbhosting.inria.fr/builds/python-slides/handouts/latest/7-4-type-hints.html  

