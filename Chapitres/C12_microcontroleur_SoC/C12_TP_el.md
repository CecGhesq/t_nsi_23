---
title: "Architecture matérielle : 6_Microcontrôleur_SoC"
subtitle: "TP"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---


# Exercice 1 : cours acquis? 🏆  


1. Quelles sont les spécificités et les avantages d'une architecture de Harvard par rapport à celle de von Neumann ?

2. Quels sont les éléments  essentiels d'un microcontrôleur?  

3. Donner quelques critères pour choisir d'utiliser un microcontrôleur.  

4. Quelles sont les spécificités et les avantages d'un système sur puce ?  

# Exercice 2 : QCM 🏆 

Plusieurs propositions de réponses sont faites : une seule est réellement pertinente.

1. Un SoC est :  
- [ ] une puce électronique   
- [ ] une sorte de micro-ordinateur sur une puce électronique  
- [ ] un système d'exploitation sur une puce électronique  
- [ ] une mémoire intégrée à une puce électronique  

2. la fluidité d'une vidéo sur un appareil SoC dépend avant tout :  
- [ ] de la mémoire embarquée dans le SoC  
- [ ] du nombre de coeurs du CPU  
- [ ] de la puce graphique (GPU)  
- [ ] des ISP (Image Signal Processor)   

# Exercice 3 : vrai faux 🏆 

1. dans un soC, le matériel peut être mis à jour  
2. la consommation énergétique d'un SoC est faible  
3. dans un SoC, tous les éléments sont indépendants  

# Activité :  

## Smartphone  
Trouvez pour votre smartphone les informations suivantes concernant le SoC utilisé (le diagramme de bloc pourra vous être utile).  

|Critère|	Valeur|
|:---|:---:|
|Nom, référence||	
|Architecture (ARM, x86, autres)||
|Taille (8, 16, 32, 64, 128 bits)||
|Taille gravure transistors (en nm)||
|Nombre de transistors	||
|Fréquences de travail (en Hz)||
|Nombre de cœurs||
|Puissance électrique consommée (en Watts)||
|Type de mémoire utilisée||	
|Taille de la mémoire utilisée (en octets)||
|Puissance de calcul du GPU (en FLOPS)||
|Score benchmark GeekBench 5||
|Score benchmark AnTuTu 8||  

Comparez vos valeurs avec celles de vos camarades de classe.

## Le Raspberry Pi 3B

Le Raspberry Pi est l’un des plus célèbres mini-ordinateurs. Il est utilisé pour de multiples usages : mini-serveur, console de jeux, lecteur multimédia, caméra, pilotage robot...
Il est équipe du SoC BCM2837 dont voici le diagramme de bloc :  
![diag bloc](./img/BCM2837.png)
![raspberry PI 3B](./img/pi3B.png)  

Repérez les différents composants présents sur ce SoC ainsi que ses principales caractéristiques.
Comparez les aux résultats obtenus avec vos smartphones.

## Le robot Lego Mindstorm EV3  

Le robot Lego Mindstorm EV3 est équipé d’un SoC Texas Instrument Sitara AM1808 dont voici le diagramme :

![diagramme lego](./img/LEGO.png)  

Faire le lien entre les composants du SoC du diagramme ci-dessus avec les composants visibles du robot ci-dessous :

![composants lego](./img/lego_2.png)

Sources :  
* NSI Terminale Ellipses T. Balabonski,S. Conchon, JC. Filliatre, K Nguyen  

* NSI Terminale Nathan les vrais exos S Pasquet M Leopoldoff  

* Cours Système sur puce (SoC) A MAROT D SALLÉ J SIMONNEAU 

