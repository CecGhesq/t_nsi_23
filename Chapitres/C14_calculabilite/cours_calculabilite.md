---
title: "Chapitre 14 : Calculabilité et décidabilité "
subtitle : "Cours"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

||||
|---|---|---|
|Notion de programme en tant que donnée. Calculabilité, décidabilité| Comprendre que tout programme est aussi une donnée. Comprendre que la calculabilité ne dépend pas du langage utilisé. Montrer, sans formalisme théorique, que le problème de l'arrêt est indécidable. | L'utilisation d'un interpréteur ou d'un compilateur, le téléchargement de logiciel, le fonctionnement des systèmes d'exploitation permettent de comprendre un programme comme donnée d'un autre programme|  

# Qu’est-ce qu’un programme informatique ?  

* Un programme source est un code écrit par un informaticien dans un langage de programmation. Il peut être compilé vers une forme binaire, interprété ou traduit dans un autre langage.  
* Un programme binaire décrit les instructions à exécuter par un microprocesseur. Ces instructions sont décrites dans un langage machine.  

[Vidéo d'introduction](https://www.youtube.com/watch?v=yRal0zm_qt0&ab_channel=NOVELCLASS)

 On peut distinguer deux grands types de langages : les langages interprétés et les langages compilés. Pour les langages supportés sur le site on a :

    * langages interprétés : Java (+ JavaScool) et Python ;  
    * langages compilés : C, C++, Pascal et OCaml.  


## Cas des programmes interprétés  
Dans ces langages, le code source (celui que vous écrivez) est interprété, par un logiciel qu'on appelle interpréteur. Celui-ci va utiliser le code source et les données d'entrée pour calculer les données de sortie :  
![](./img/interpret.png)  
L'interprétation du code source est un processus « pas à pas » : l'interpréteur va exécuter les lignes du code une par une, en décidant à chaque étape ce qu'il va faire ensuite. 

## Cas des programmes compilés
Dans ces langages, le code source (celui que vous écrivez) est tout d'abord compilé, par un logiciel qu'on appelle compilateur, en un code binaire qu'un humain ne peut pas lire mais qui est très facile à lire pour un ordinateur. C'est alors directement le système d'exploitation qui va utiliser le code binaire et les données d'entrée pour calculer les données de sortie : 

![](./img/compil.png)

## Principales différences

On pourrait discuter très longtemps des avantages et inconvénients des différents types de langages mais les deux points qui sont les plus intéressants sont les suivants :

* Dans un langage interprété, le même code source pourra marcher directement sur tout ordinateur. Avec un langage compilé, il faudra (en général) tout recompiler à chaque fois ce qui pose parfois des soucis.
* Dans un langage compilé, le programme est directement exécuté sur l'ordinateur, donc il sera en général plus rapide que le même programme dans un langage interprété.

# Calculabilité et décidabilité

## Définition : Algorithme  
> Description non ambiguë et finie d’opérations pour résoudre un problème

On notera que le langage de programmation n’intervient __PAS__ dans cette définition.  

## Les différents langages de programmation permettent-ils de concevoir des algorithmes plus complexes ?  

Tous les langages n’ont pas la même _expressivité_ (capacité d’écrire des algorithmes avec un langage), mais il existe une _expressivité maximale_ qui correspond aux langages dits __Turing-complets__. L’immense majorité des langages de programmation utilisés aujourd’hui sont Turing-complets.  
Ainsi, les différences entre les langages se feront principalement sur leur utilisation (On peut penser à HTML ou SQL qui sont utilisés dans des contextes différents), leur lisibilité et leur efficacité (en temps d'exécution, en mémoire...).

## Histoire et Langages Turing-complets  

La notion d’algorithme existait déjà pendant l’Antiquité. Pendant le XXème siècle, d’immenses progrès ont été faits sur les limites des algorithmes. Entre 1930 et 1940, de nombreux chercheurs travaillent sur les notions d’algorithmes comme un outil mathématique et ont besoin de décrire précisément ce qu’il est possible de faire ou non avec des algorithmes.

Pendant cette période, Alan Turing a imaginé __les Machines de Turing (MT)__ : un modèle de calcul mécanique capable d’effectuer des calculs (et donc d’exécuter des algorithmes).
Un langage est Turing-complet s’il est capable de décrire tout algorithme descriptible avec une MT. La thèse de Church-Turing dit que les MT sont capables de décrire tout les algorithmes imaginables.

> Un langage Turing-complet permet d’écrire tout les algorithmes imaginables.
> Néanmoins, tout les problèmes imaginables ne sont pas résolubles par un algorithme.  

## Calculabilité  
> La calculabilité concerne la question de ce qui peut être calculé par un algorithme ou un programme informatique. Une fonction est dite calculable si elle peut être calculée par une machine de Turing ou par un algorithme.

En d'autres termes, la calculabilité s'intéresse à déterminer si un problème peut être résolu algorithmiquement. Par exemple, des problèmes tels que la multiplication de nombres, la recherche d'un élément dans un tableau trié ou la résolution de certaines équations mathématiques sont calculables, car il existe des algorithmes pour les résoudre efficacement.

## Définition : Décidable

> Fait qu’il existe un algorithme pour résoudre un problème donné.

Alan Turing a démontré que le __problème de l’arrêt__ est un problème __indécidable__.

## Démonstration que le problème de l’arrêt est indécidable


Ce problème peut s’énoncer sous la forme suivante :
“Est-il possible de déterminer si un programme P avec une entrée E va s’arrêter ?”

Le problème est décidable s’il existe un algorithme capable de répondre au problème.

[video_1: animation sur le problème de l'arrêt](https://www.youtube.com/watch?v=92WHN-pAFCs&ab_channel=udiprod)  

[video_2 : formalisme mathématique par Rachid Guerraoui](https://www.youtube.com/watch?v=PsTcL7KlGBg&ab_channel=Wandida%2CEPFL)  

Cette démonstration est une démonstration par _l’absurde_ : on va supposer que le problème de l’arrêt est décidable, donc qu’il existe un algorithme capable de déterminer si un algorithme s’arrête, puis on va montrer une incohérence, ce qui prouvera que cet algorithme n’existe pas.

On imagine 4 programmes :
*  P (comme Photocopie) qui prend en paramètre quelque chose et le duplique  
*  H (comme Halting (arrêt)) qui prend en paramètre un programme et son paramètre et qui renvoie la réponse au problème de l’arrêt (c’est ce programme dont on suppose
l’existence)  
*  N qui prend en paramètre un booléen et renvoie Vrai si le paramètre vaut Faux et qui fait une boucle infinie si le paramètre vaut Vrai.  
* X qui prend en paramètre un programme et qui exécute P, H puis N comme illustré
ci-dessous.  

_Note : On imagine que les programmes qui prennent des programmes en paramètres prennent les codes sources de ces programmes._

![](./img/H.png)

H doit renvoyer Vrai si le programme X avec le paramètre X termine et Faux s’il ne termine pas.  
Si H renvoie Vrai, N va faire une boucle infinie (donc le programme ne termine pas si H répond qu’il termine).  
Si H renvoie Faux, N va renvoyer Vrai et terminer (donc le programme termine si H répond qu’il ne termine pas).  

On a une incohérence donc __H ne peut pas exister__.

Donc il ne peut pas exister d’algorithme qui répond au problème de l’arrêt.
Donc le __problème de l’arrêt est indécidable__.


sources :  
* https://www.france-ioi.org/algo/course.php?idChapter=561&idCourse=2368  
* Cours M Tournebize  
* Turing à la plage Rachid Gerraoui  