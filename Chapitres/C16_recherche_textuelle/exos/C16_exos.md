---
title: " 16 Recherche textuelle"
subtitle: "exercices"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
# Verlaine

Voici la première strophe du poème de Paul Verlaine Chanson d'automne :
Les sanglots longs  
Des violons  
De l'automne  
Blessent mon coeur  
D'une langueur  
Monotone.

|0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
|L|e|s||s|a|n|g|l|o|t|s||l|o|n|g|s||D|e|s||v|i|o|l|o|n|s||D|e||l|'a|u|t|o|m|n|e|

|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
o|l|o|n|s| |D|e||l|'|a|u|t|o|m|n|e||B|l|e|s|s|e|

|50|51|52|53|54|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
n|t| |m|o|n| |c|o|e|u|r| |D |'|u|n|e| |l|a|n|g|u|e|

|75|76|77|78|79|80|81|82|83|84|85|
|---|---|---|---|---|---|---|---|---|---|---|
|u|r||M|o|n|o|t|o|n|e|

1) Recherchez le motif 'uto' dans cette strophe en utilisant l'algorithme de Boyer-Moore-Horspool : 
Compléter le tableau de décalage avec le prétraitement du motif :  

|u|t|o|
|---|---|---|
| | | |

puis réaliser un tableau de suivi en comptant le nombre de comparaisons
```














```


2) Recherchez le motif 'lon' dans cette strophe en utilisant l'algorithme de Boyer-Moore-Horspool : 

Compléter le tableau de décalage avec le prétraitement du motif :  

|l|o|n|
|---|---|---|
| | | |

puis réaliser un tableau de suivi.

```







```

# Algorithme naif

1. Vous devez programmer la recherche d'un mot dans une chaîne en vous basant sur l'algorithme naïf consistant à vérifier pour chaque position dans la chaîne si les caractères du mot se trouvent bien placés dans leur ordre de lecture.
Commencez par écrire une fonction `verifierMot(mot, chaine, position)` qui renvoie True lorsque mot est placé à l'emplacement position dans chaine, et False dans le cas contraire.

```python
>>> verifierMot("mi", "do-ré-mi-fa-sol", 6) 
True
>>> verifierMot("fa", "do-ré-mi-fa-sol", 8) 
False
>>> verifierMot("de", "abcde", 3)
True
>>> verifierMot("des", "abcde", 3)
False
```

2. Puis écrivez la fonction `rechercherMot(mot, chaine, debut)` qui renvoie la première position supérieure ou égale à `debut`, où `mot` apparaît dans `chaine`, et qui renvoie -1 lorsque mot n'apparaît plus dans `chaine` après `debut`.

```python
>>> rechercherMot("si", "la-si-do-si-ré", 0)
3
>>> rechercherMot("si", "la-si-do-si-ré", 3)
3
>>> rechercherMot("si", "la-si-do-si-ré", 4)
9
>>> rechercherMot("si", "la-si-do-si-ré", 10)
-1
```

# Recherche

La fonction recherche ci-dessous :

```python
def recherche(txt, motif):
    NO_CAR = 256
    m = len(motif)
    n = len(...)
    tab_car = [-1]*NO_CAR
    for i in range(...):
        tab_car[ord(motif[i])] = i
    decalage = 0
    res = ...
    while(decalage <= n-m):
        j = m-1
        while j>=0 and motif[j] == txt[decalage+j]:
            j = j - 1
        if j<0:
            res.append(decalage)
            if decalage+m < n :
                decalage = decalage + m-tab_car[ord(txt[decalage+m])]
            else :
                decalage = decalage + 1
        else:
            decalage = decalage + max(1, j-tab_car[ord(txt[decalage+j])])
    return res
```

permet de trouver la __position du motif__  dans le texte txt. Si le motif motif est présent dans texte txt, la fonction recherche renvoie un tableau contenant les indices de positions du motif dans le texte. Dans le cas où le motif n'est pas présent, la fonction recherche renvoie un tableau vide.

Par exemple recherche("AZERTYAZER", "ER") renverra le tableau [2,8], recherche("AZERTYAZER", "AB") renverra le tableau [ ].

Après avoir étudié attentivement cette fonction recherche, vous compléterez cette fonction (remplacez les ...) pour qu'elle fournisse les résultats attendus. Réaliser une docstring et des commentaires dans la fonction. Etudier le déroulement pas à pas avec le débogage.

# Horspool

1. Modifiez la fonction `horspool(mot, chaine, debut)` du [cours](../C16_cours/C16_cours.md) pour pouvoir reprendre la recherche à partir d'une position `debut` quelconque.

```python
chaine = "TCTAGCAGTAGACT"
mot = "TAG"
>>> horspool(mot, chaine, 0)
2
>>> horspool(mot, chaine, 3)
8
>>> horspool(mot, chaine, 9)
-1

```

2. Créez une fonction `getPositions(mot, chaine)` qui renvoie la liste de toutes les positions de la chaîne mot dans la chaîne de caractères chaine, en utilisant la fonction horspool().   

```python
>>> getPositions("TAG", "TAGCTAGCAGTAGACT")
[0, 4, 10]
>>> getPositions("X", "X-XX-X")
[0, 2, 3, 5]
>>> getPositions("abcd", "abcecbcdadabc")
[]

```


__Sources__ : 
* site pixees David Roche  
* Jean Diraison  

