---
title: " 13 Les graphes : parcours"
subtitle: "Structure de données : cours C13_2"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
![](./img/BO2.jpg)

Un parcours de graphe est un algorithme consistant à explorer les sommets d'un graphe de proche en proche à partir d'un sommet initial. Parcourir simplement le dictionnaire ou la matrice d’un graphe comme on peut le faire pour effectuer une copie du graphe n’est pas considéré comme un parcours de graphe.

Tous les parcours suivent plus ou moins le même algorithme de base :  

On visite un sommet s1. On note S l’ensemble des voisins de s1.  

Tant que S n’est pas vide :
-	on choisit un sommet s de S
-	on visite s 
-	on ajoute à S tous les voisins de s pas encore visités  

# Parcours en profondeur

## C'est une pile !
Si on utilise une __pile__ pour S, les sommets enregistrés en dernier vont être visités en premier : on parcourt le graphe en visitant à chaque fois un voisin du __dernier sommet__, sauf si celui-ci n’a pas de voisin non visité, auquel cas on remonte au dernier sommet ayant un voisin non visité. C’est un parcours en profondeur (__DFS, Depth First Search__). C’est le parcours utilisé naturellement par une personne qui explore un labyrinthe.  

![](./img/parcours.png)  

Sur le graphe précédent, A-B-C-D-E-F-G et F-B-C-D-G-E-A sont des parcours en profondeur.  
F-B-C-D-G-A-E n’en est pas un (E a été empilé après A, donc sera dépilé avant).

## Implémentation  

Afin de parcourir le graphe sans le modifier, on peut créer une nouvelle classe objet : `Parcours_profondeur`. En repartant de la classe Graphe_L du cours précédent on obtient :  

```python 
class Graphe_D : 
    '''un graphe comme un dictionnaire d'adjacence à partir d'une liste de sommets'''
  
    def __init__(self, sommets):
        self.dico_sommets = {s: [] for s in sommets}
        self.n = len(sommets)
        self.liste_sommets = sommets

    def ajouter_arete(self, s1, s2):
        self.dico_sommets[s1].append(s2)
        self.dico_sommets[s2].append(s1)

    def successeurs(self, s):
        return self.dico_sommets[s]

class Parcours_profondeur:
    def __init__(self, graphe, sommet_initial):
        self.graphe = graphe
        self.sommet_initial = sommet_initial
        self.visite = dict()
        #pour chacun des sommets on initie leur visite à False
        sommets = graphe.liste_sommets
        for sommet in sommets:
            self.visite[sommet] = False

    def parcourir(self, sommet = None, decalage = ""):
        if sommet is None:
            sommet = self.sommet_initial
        # traitement et marquage vont ensemble
        # on marque avant d'explorer les voisins
        print(decalage + "On visite", str(sommet))
        self.visite[sommet] = True
        for voisin in self.graphe.successeurs(sommet):
            #on ne parcourt que les sommets non marqués
            if not(self.visite[voisin]):
                self.parcourir(voisin, decalage + "  ")
```
Pour observer l'exécution, on crée à nouveau le graphe G1 du cours C13_1 ( les arêtes ne sont redonnées) :  

![](./img/G1.jpg)  

On réalise ensuite une instance de classe du Parcours_profondeur pour le graphe G1 et on appelle la méthode du parcours.
```python 
>>> G1 = Graphe_D(['A','B', 'C', 'D', 'E', 'F'])
>>> p_G1 = Parcours_profondeur(G1, 'A')
>>> p_G1.parcourir()
On visite A
  On visite B
    On visite C
      On visite D
        On visite E
          On visite F
```

Observez le déroulement sur [Python Tutor](https://urlz.fr/hpkL).
On retrouve l'__appel récursif et ainsi le fonctionnement de pile__.  

# Parcours en largeur  

## C'est une file !
Si on utilise une __file__ pour S, les sommets enregistrés dans S en premier vont être visités les premiers. On va donc visiter d’abord les sommets le plus prêts de s1 (ceux à distance 1, puis ceux à distance 2, puis 3…). C’est un parcours en largeur (__BFS, Breadth First Search__). On va utiliser ce parcours pour trouver le sommet le plus prêt de s1 vérifiant une condition donnée ou le chemin le plus court entre s1 et un autre sommet.  

## Implémentation  

Comme précédemment on crée une classe pour le parcours en réutilisant celle du graphe.

```python
class Parcours_largeur:
    def __init__(self, graphe, sommet_initial):
        self.graphe = graphe
        # voici la création de la file
        self.sommets_restant_a_visiter = [sommet_initial]
        self.visite = dict()

        sommets = graphe.liste_sommets
        for sommet in sommets:
            self.visite[sommet] = False

    def parcourir(self):
        while len(self.sommets_restant_a_visiter) > 0:
            sommet_visite = self.sommets_restant_a_visiter.pop(0) # on défile le visité
            if not(self.visite[sommet_visite]):
                print("on visite",sommet_visite)
                self.visite[sommet_visite] = True
                voisins = self.graphe.successeurs(sommet_visite)
                voisins_inconnus = list(filter(lambda n: not(self.visite[n]), voisins))
                # on enfile les sommets non encore visités
                self.sommets_restant_a_visiter = self.sommets_restant_a_visiter \
                + voisins_inconnus
```

Pour observer le fonctionnement de la file : [python Tutor](https://urlz.fr/hpnm)


📝 __Remarque__ : La fonction intégrée `filter()` peut être convertie en une compréhension de liste. Il prend un __prédicat__ comme premier argument et un __itérable__ comme second argument. Il construit un itérateur contenant tous les éléments de la collection initiale qui satisfait la fonction de prédicat. 

```python
Syntax : filter(function , <list type>)
Return Type: a list of computed values
Example :
>>> inp_list = ['t','u','t','o','r','i','a','l']
>>> result = list(filter(lambda x: x!='t' , inp_list))
>>> print(result)
['u', 'o', 'r', 'i', 'a', 'l']

``` 
# Repérer la présence d'un cycle  

Il est habituel d'utiliser un marquage à trois couleurs pour repérer la présence d'un cycle :  

* on marque en blanc les sommets non encore atteints par le parcours en profondeur  
* on marque en gris les sommets déjà atteints dont le parcours est en cours  
* on marque en noir les sommets atteints dont le parcours est terminé.  

![](./img/cycle.jpg)  

Ainsi dans l'exemple ci-dessus : 
* à gauche ce n'est pas un cycle pourtant on retombe un seconde fois sur C  dans le parcours en profondeur cependant il sera noir  
* à droite c'est un cycle  et quand on retombera sur B il sera encore gris puisque le parcours ne sera pas terminé.  

Ainsi l'algorithme est , quand on visite un sommet : 
- si le sommet est gris, c'est qu'on vient de découvrir un cycle  
- s'il est noir, on ne fait rien  
- sinon s'il est blanc  on procède ainsi :
    - on colore le sommet en gris
    - on visite tous les voisins , récursivement
    - enfin on colore le sommet en noir.  

En utlisant  notre classe de graphe ordonné, on crée un cycle de cinq sommets bouclés en un cycle : 

```python
class Graphe_M_ord :
    ''' un graphe ordonné représenté par une matrice d'adjacence, où les sommets sont les entiers 0,1, ..... n-1
    '''
    def __init__ (self, n):
        self.n = n
        self.matrice = [[False] * n for _ in range(n)]
        
    def sommets(self) :
        return  [i for i in range(self.n)]
    
    def ajouter_arc(self, som_1, som_2) :
        self.matrice[som_1][som_2] = True 
            
    def liste_voisins(self, som):
        liste_voisins = []
        for i_voisin in range(self.n) :
            if self.matrice[som][i_voisin]:
                liste_voisins.append(i_voisin)
        return liste_voisins
G2 = Graphe_M_ord(5)
G2.ajouter_arc(0,1)
G2.ajouter_arc(1,2)
G2.ajouter_arc(2,3)
G2.ajouter_arc(3,4)
G2.ajouter_arc(4,0)

BLANC, GRIS, NOIR = 1, 2, 3
def parcours_cycle(graphe, couleur, sommet) :
    '''parcours en profondeur depuis le sommet '''
    if couleur[sommet] == GRIS :
        return True
    if couleur[sommet] == NOIR :
        return False
    couleur[sommet] = GRIS
    for sommet in graphe.liste_voisins(sommet) :
        if parcours_cycle(graphe, couleur, sommet):
            return True
    couleur[sommet] = NOIR
    return False

def cycle(graphe) :
    couleur = {}
    for sommet in graphe.sommets() :
        couleur[sommet] = BLANC
    for sommet in graphe.sommets() :
        if parcours_cycle(graphe, couleur, sommet):
            return True
    return False

>>> cycle(G2)
True
```

# Chercher le chemin  

Pour connaître le chemin entre un sommet de départ et un sommet d'arrivée, on place dans un dictionnaire `vus` qui associe à chaque sommet, le sommet qui a permis de l'atteindre pendant le parcours en profondeur.
Pour le sommet de départ on lui associe None dans ce dictionnaire. Une fois le parcours en profondeur réalisé, on "remonte" dans le dictionnaire en partant de l'arrivée jusqu'au départ. 

```python
def parcours_chemin(graphe, vus, org, S_dep):
    '''parcours depuis le sommmet s_dep en venant de org'''
    if S_dep not in vus :
        vus[S_dep] = org
        for voisin in graphe.dico_sommets[S_dep]:
            parcours_chemin(graphe, vus, S_dep, voisin)

def chemin(graphe, S_dep, S_arr) :
    '''chemin depuis S_dep jusque S_arr, None sinon'''
    vus = {}
    # on crée le dictionnaire avec la clé S_dep ayant pour valeur None :
    parcours_chemin(graphe, vus, None, S_dep)
    # le dictionnaire a été complété avec tous les prédécesseurs
    if S_arr not in vus :
        return None
    chemin = []
    suivant = S_arr
    while suivant is not None : # on remonte le dictionnaire comme une pile
        chemin.append(suivant)
        suivant = vus[suivant]
    chemin.reverse()
    return chemin
    
print(chemin(G3, 'A', 'F') )        
```
Pour voir l'exécution : [Python Tutor](https://urlz.fr/hqFb)

>📢 A retenir :  étant donnés un graphe et un sommet dans ce graphe, __le parcours en profondeur__ et __le parcours en largeur__ sont deux algorithmes fondamentaux pour explorer le graphe en partant de ce sommet.  
> Ces deux parcours déterminent __l'ensemble des sommets accessibles__ depuis le sommet de départ. Le parcours en profondeur permet de __déterminer la présence d'un cycle__ dans un graphe orienté. Le parcours en largeur permet de déterminer la __distance__ entre le sommet de départ et tout sommet accessible, c'est à dire le plus petit nombre d'arcs pour relier ces deux sommets.  

Sources :  
* cours NSI M. Diraison  
* cours NSI lycée de Foix  
* NSI 24 leçons T Balabonski S Conchon JC Filliâtre K Nguyen Ellipses
* DIU Enseigner l'informatique au lycée Sébastien Aubert Bruno Mermet 
