---
title: " 13 Les graphes : implémentation"
subtitle: "Structure de données : cours C13_1"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
![](./img/BO.jpg)

Un graphe est une __structure de données__  à la fois simple et riche dans ses applications.  

# Définitions  

## Sommets, arcs, orientation

>📢 Définition :  
Les nœuds d’un graphe sont aussi appelés __sommets__. Un nœud contient une donnée (ou étiquette). On appelle __ordre__ d'un graphe son nombre de sommets.  
Une __arête__ relie deux sommets, qui sont alors _voisins ou adjacents_.  

![](./img/graph.jpg)  

* Les arêtes peuvent être orientées. Le graphe est alors orienté et les arêtes sont aussi appelées __arcs__. On dit que l’_arête (x, y)_ part du sommet x et arrive au sommet y.

* Dans un __graphe pondéré__, chaque arête porte une _valuation_, aussi appelée _poids ou coût_.

## Chemin dans un graphe  

>📢 Définition :  
Un __chemin__ reliant un sommet _u_ à un sommet _v_ est une séquence finie de sommets reliés deux à deux par des arcs et menant de _u_ à _v_.  

Pour un graphe non orienté, on a un chemin reliant _u_ à _v_ si et seulement si on a un chemin reliant _v_ à _u_.  

![](./img/graph2.jpg)  

* Une __boucle__ est une arête reliant un sommet à lui-même.  
* Des arêtes multiples sont deux arêtes, ou plus, qui joignent les mêmes sommets.  
* Un __cycle__ est un chemin dans un graphe non orienté dont les deux extrémités sont identiques.

Un graphe simple est un graphe ne contenant ni boucle, ni arêtes multiples.  
Le __degré__ d’un sommet est le nombre d’arêtes incidentes à ce sommet, c’est-à-dire ayant ce sommet pour extrémités, les boucles étant comptées deux fois.  
Un sommet de degré zéro est dit isolé.  

>📢 Définition :  
La _longueur d'un chemin_ est définie comme le nombre d'arcs qui constituent le chemin.  
> La  __distance__ entre deux sommets est la longueur du plus court chemin reliant ces deux sommets.  

* On dit qu'un graphe non orienté est __connexe__ si, pour toute paire _u,v_ de sommets il existe un chemin de _u_ à _v_. ( le graphe est en seul morceau).  
Il est qualifié de __fortement connexe__ si pour tous sommets  _u_ et _v_ un chemin les relie.

## Successeurs, prédécesseurs  

* L’ensemble des __successeurs__ d’un sommet x est l’ensemble des sommets y tels qu’il existe une arête allant de x vers y.  
* L’ensemble des __prédécesseurs__ d’un sommet x est l’ensemble des sommets y tels qu’il existe une arête allant de y vers x.  

# Applications  

|Réseaux sociaux ("est ami avec")|Liaisons aériennes|
|:---:|:---:|
|C'est un __graphe orienté__, les relations sont représentées par des arcs fléchés. ![rs](./img/reseau_social.jpg)|C'est un __graphe non orienté__, les relations sont représentées par des arêtes. Ce graphe est non-connexe : il est en 2 parties non reliées (Nantes est toute seule) ![ra](./img/reseau_aerien.jpg)|

|Labyrinthe|Itinéraires routiers|
|:---:|:---:|
|Dans ce graphe on trouve __plusieurs chemins__ menant de l'Entrée à la Sortie, ainsi que __plusieurs cycles__ (des chemins qui bouclent sur eux-mêmes). ![rs](./img/labyrinthe.jpg)|Les arêtes de ce __graphe pondéré__ portent les distances kilométriques séparant les villes.![ra](./img/route.jpg)|

>📢 Définition :  Les graphes sont utilisés :  
-pour représenter une relation entre un ensemble d’objets homogènes : réseaux routiers, réseaux électriques, Internet, réseaux sociaux…  
> -dans des algorithmes : recherche du plus court chemin (protocole de routage ou GPS routier), ordonnancement de tâches …  

Remarque : Les arbres font partie de la grande famille des graphes. Plus précisément, un arbre est un graphe acyclique orienté possédant une seule racine tel que tous les nœuds sauf la racine ont un unique prédécesseur (parent).  

# Implémentations  

Il existe de nombreuses façons de représenter un graphe en machine selon la nature du graphe mais aussi selon la nature des opérations et des algorithmes à effectuer sur ce graphe. Quelle que soit le choix, on souhaite réaliser deux types d'opérations :  

* des opérations de _construction_ de graphes (obtention de graphes vides, ajout de sommets , d'arcs, ...)  
* des opérations de _parcours_  d'un graphe ( parcourir tous les sommets, tous les arcs, ...)  

## Matrice d'adjacence ou dictionnaire ?
On trouve principalement deux représentations des graphes en machine. La matrice d’adjacence utilise les tableaux à deux dimensions ; la liste des successeurs utilise les dictionnaires.  

### Définitions  

|Matrice d'adjacence|Liste|
|---|---|
|Les sommets sont numérotés de 0 à n – 1. Les arêtes sont représentées par __une matrice M, un tableau 2D__.  Avec un graphe non orienté, ce tableau est symétrique : M[i, j] = M[j, i]. | Un __dictionnaire de listes__ permet de représenter le graphe. Les clés du dictionnaire sont les sommets ; la valeur associée à un sommet est la liste des successeurs de ce sommet.  |
|Un coefficient non nul ligne i colonne j correspond à un __arc ou une arête__ allant du sommet i vers le sommet j. Ce coefficient vaut 0 ou 1, ou est égal à la valuation de l’arête dans le cas d’un graphe pondéré.|On peut aussi utiliser la liste de prédécesseurs. Ces deux listes sont identiques dans un graphe non orienté : liste des voisins. Dans le cas d’un graphe pondéré, les listes sont remplacées par des dictionnaires.|
|Les étiquettes des sommets peuvent être données dans une liste. Le __graphe est alors le couple (lst_sommets, matrice_adjacence)__.|  Le graphe est alors __un dictionnaire de dictionnaires__ : le dictionnaire de successeurs a pour clés les étiquettes des sommets successeurs et pour valeurs les valuations des arêtes associées|

### Exemple 

Voici un exemple à partir du graphe ci-dessous :  

![G1](./img/G1.jpg)  

* matrice d’adjacence  

On numérote les sommets dans l’ordre alphabétique. Le chiffre __1__ montre la présence d'une arête reliant les deux somments du graphe , __0__, l'absence d'arête.  

$`\begin{pmatrix}
 0& 1 &1 & 0 & 0 & 0 \\
 1& 0 &1  & 0 & 1 & 1 \\
 1& 1 & 0 & 1 & 0 & 0 \\
 0& 0 & 1 & 0 & 1 & 0 \\
 0& 1 & 0 & 1 & 0 & 1 \\
 0&  1& 0 & 0 & 1 & 0
\end{pmatrix}`$ 

Le __1__ ligne 4 colonne 3 signifie qu’une arête relie le sommet 4 (D) au sommet 3 (C).

* dictionnaire  

```python
G1 = {A : [B, C], 
    B : [A, C, E, F], 
    C : [A, B, D], 
    D : [C, E], 
    E : [B, D, F],
    F : [B, E]}
```

## Matrice d'adjacence  

Dans une version objet, on peut proposer cette implémentation pour un graphe non orienté. Des commentaires sont proposés pour modifier le code en cas de graphe orienté.  

On place ici __False__  lorsqu'il n'y a pas d'arête (arc si orienté) reliant les sommets, __True__ dans le cas inverse.  

```python
class Graphe_M :
    ''' un graphe représenté par une matrice d'adjacence, où les sommets sont les entiers 0,1, ..... n-1
   '''
    def __init__ (self, n):
       self.n = n
       self.matrice = [[False] * n for _ in range(n)]

    def sommets(self) : # utilisé dans les parcours(C13_2)
        return  [i for i in range(self.n)]

    def ajouter_arete(self, som_1, som_2): 
    # ajouter_arc si graphe orienté
        self.matrice[som_1][som_2] = True
    # ligne à enlever si le graphe est orienté 
        self.matrice[som_2][som_1] = True 
    
    def liste_voisins(self, som):
        liste_voisins = []
        for i_voisin in range(self.n) :
            if self.matrice[som][i_voisin]:
                liste_voisins.append(i_voisin)
        return liste_voisins
    
    def __str__(self):
        retour = "Matrice d'adjacence :"
        retour += "\n"
        for i in range(len(self.matrice)) : 
            retour += str(self.matrice[i])
            retour +=  "\n"
        return retour
```

Si on crée le graphe de l'exemple précédent :  

```python
>>> G1 = Graphe_M(6)
>>> G1.ajouter_arete(0,1)
>>> G1.ajouter_arete(0,2)
>>> G1.ajouter_arete(1,2)
>>> G1.ajouter_arete(1,4)
>>> G1.ajouter_arete(1,5)
>>> G1.ajouter_arete(2,3)
>>> G1.ajouter_arete(3,4)
>>> G1.ajouter_arete(4,5)
```

```python
>>> print(G1)
Matrice d'adjacence :
[False, True, True, False, False, False]
[True, False, True, False, True, True]
[True, True, False, True, False, False]
[False, False, True, False, True, False]
[False, True, False, True, False, True]
[False, True, False, False, True, False]
```

```python
>>> print('sommet B', G1.liste_voisins(1)) 
sommet B [0, 2, 4, 5]
```

On remarque que les sommets ne sont pas nommés ici mais désignés par __leurs indices__.  

## Dictionnaire de listes  

Dans cette représentation, un graphe est un _dictionnaire_, qui associe à chaque sommet l'ensemble de ses voisins.  
Ainsi les sommets ne sont pas limités à des entiers : ils représentent les __clés du dictionnaire__.

```python
class Graphe_D : 
    '''un graphe comme un dictionnaire d'adjacence'''
  
    def __init__(self, sommets):
        '''sommets : liste de noms des sommets'''
        self.dico_sommets = {s: [] for s in sommets}
        self.n = len(sommets)
        # nécessaire pour les parcours du C13_2
        self.liste_sommets = sommets 

    def ajouter_arete(self, s1, s2):
    # ajouter_arc si graphe orienté
        self.dico_sommets[s1].append(s2)
    # ligne à enlever si le graphe est orienté
        self.dico_sommets[s2].append(s1)

    def get_sommets(self):
        return list(self.dico_sommets.keys())

    def successeurs(self, s):
        return self.dico_sommets[s]
    
    def __str__(self):
        retour = "dictionnaire d'adjacence :"
        retour += "\n"
        for sommet in self.get_sommets() : 
            retour += str(sommet) + ':'  + str(self.successeurs(sommet))
            retour +=  "\n"
        return retour
```

Si on crée le graphe de l'exemple précédent :  

```python
>>> G1 = Graphe_D(['A','B', 'C', 'D', 'E', 'F'])
>>> G1.ajouter_arete('A','B')
>>> G1.ajouter_arete('A','C')
>>> G1.ajouter_arete('C','B')
>>> G1.ajouter_arete('E','B')
>>> G1.ajouter_arete('F','B')
>>> G1.ajouter_arete('C','D')
>>> G1.ajouter_arete('D','E')
>>> G1.ajouter_arete('E','F')

>>>print(G1)
dictionnaire d'adjacence :
A:['B', 'C']
B:['A', 'C', 'E', 'F']
C:['A', 'B', 'D']
D:['C', 'E']
E:['B', 'D', 'F']
F:['B', 'E']
```

Les sommets sont désignés ici par leur __étiquette ou valeur__.  

Sources :  
* cours NSI M. Diraison  
* cours NSI lycée de Foix  
* NSI 24 leçons T Balabonski S Conchon JC Filliâtre K Nguyen Ellipses
* DIU Enseigner l'informatique au lycée Sébastien Aubert Bruno Mermet  
