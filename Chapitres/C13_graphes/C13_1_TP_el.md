---
title: " 13 Les graphes : implémentation"
subtitle: "Structure de données : TP C13_1"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# QCM 🏆

![](./img/qcm.jpg)  

1. Ce graphe :  
[ ] a pour ordre 4  
[ ] a pour ordre 6  
[ ] est orienté  
[ ] est non orienté  

2. Ce graphe est :  
[ ] est connexe  
[ ] est fortement connexe  
[ ] admet 2 --> 1 comme arête  
[ ] admet 0 --> 1 --> 0 --> 3 comme chemin  

3. La matrice d'adjacence pour ce graphe est :

[ ]  
$`\begin{pmatrix}
0 &1  &0  &0  \\
1 &0  &0  &0  \\
1 & 1 &0  &0  \\
 1&1  &0  &0 
\end{pmatrix}`$ 

[ ]  
$`\begin{pmatrix}
0 &1  &1  &1  \\
1 &0  &1  &1  \\
0 &0  &0  &0  \\
0 &0  &0  &0
\end{pmatrix}`$ 

[ ]  
$`\begin{pmatrix}
0 &1  &1  &1  \\
1 &0  &1  &1  \\
1 &1  &0  &0  \\
1 &1  &0  &0 
\end{pmatrix}`$  

4. La liste des successeurs des sommets de ce graphe peut être représentée par le dictionnaire suivant :  
[ ] {0: [1, 2, 3], 1: [0, 2, 3], 2: [], 3: []}  
[ ] {0: [1, 2, 3], 1: [0, 2, 3], 2: [0, 1], 3: [0, 1]}  
[ ] {0: [1], 1: [0], 2: [0, 1], 3: [0, 1]}  

# Messagerie 🏆

![](./img/messagerie.jpg)  

Pour accéder à sa messagerie, Raphaël a choisi un code qui doit être reconnu par le graphe étiqueté suivant les sommets 1-2-3-4. Une succession des lettres constitue un code possible si ces lettres se succèdent sur un chemin du graphe orienté ci-dessus en partant du sommet 1 et en sortant au sommet 4.  

1. Parmi les trois codes suivants, quel(s) est (sont) le(s) code(s) reconnu(s) par le graphe.  
* SUCCES  
* SCENES  
* SUSPENS  
2. Quelle est la taille du plus petit code possible ? Ce code est-il unique ?  
3. Y-a-t-il une taille maximale.  


# Représentations de graphes 🏆  

On donne le graphe suivant :  
![](./img/grap.jpg)  

1. Donner une représentation de ce graphe au moyen d'un dictionnaire d'adjacence avec les successeurs.  
2. Donner une représentation de ce graphe au moyen d'une matrice d'adjacence.  

# A partir d'une matrice  🏆

Donner le graphe associé à la matrice d'ajacence suivante :  
$`\begin{pmatrix}
0 &1  &0  &0  &0  &0  \\
1 &0  &0  &1  &0  &0  \\
1 &1  &0  &0  &1  &0  \\
0 &0  &1  &0  &0  &0  \\
0& 0 &0  &0  &0  &1  \\
0 &0  &1  &0  &0  &1
\end{pmatrix}`$  

On notera les sommets A, B, C, D, E et F.  

# Représenter sous forme d'un graphe 🏆

On souhaite organiser un tournoi de foot avec 4 équipes (numérotées de 1 à 4). Chaque équipe rencontre une seule fois toutes les autres.

1. Représenter la situation sous forme d'un graphe.  
2. Combien d'arêtes possède-t-il ? En déduire le nombre de matchs au total pour ce tournoi ?  
3. Ce graphe est-il connexe ?  


# Changement de représentation 🏆🏆

On dispose de la classe Graphe_M permettant d'implémenter un graphe non orienté ci- dessous :  

```python
class Graphe_M :
    ''' un graphe représenté par une matrice d'adjacence, où les sommets sont les entiers 0,1, ..... n-1
   '''
    def __init__ (self, n):
       self.n = n
       self.matrice = [[0] * n for _ in range(n)]

    def ajouter_arete(self, som_1, som_2): 
        self.matrice[som_1][som_2] = 1
        self.matrice[som_2][som_1] = 1
```

1. Ecrire une méthode `matrix_to_list` qui permet de passer de la représentation par matrice d'adjacence à la représentation par liste de successeurs.  
Le graphe renvoyé sera codé sous forme de dictionnaire, les clés étant les sommets, numérotés de 0 à n - 1, n étant l'ordre du graphe, et les valeurs associées sont les listes des successeurs du sommet.  

2. Créer le graphe ci-dessous et tester la méthode `matrix_to_list`.  

![](./img/ex2.jpg)

# Passer des successeurs aux prédecesseurs 🏆🏆  

On dispose de la classe Graphe_L permettant d'implémenter un graphe orienté ci- dessous :

```python
class Graphe_L : 
    '''un graphe comme un dictionnaire d'adjacence avec la liste sommets contenant les valeurs des sommets'''
  
    def __init__(self, n):
        self.n = n
        self.dico_sommets = {s: [] for s in range(n)}

    def ajouter_arc(self, s1, s2):
        '''arc orienté de s1 vers s2'''
        self.dico_sommets[s1].append(s2)
        
    def get_sommets(self):
        return list(self.dico_sommets.keys())

     def successeurs(self, s):
        '''renvoie la liste des successeurs du sommet s'''
        return self.dico_sommets[s]
```

1. Créer le graphe orienté suivant :  

![](./img/qcm.jpg)  

2. Ecrire une méthode `predecesseurs(self,s)` qui renvoie une liste contenant les valeurs des prédécesseurs du sommet.

```python
>>> print('prédecesseurs du sommet 2 : ', G.predecesseurs(2))
prédecesseurs du sommet 2 :  [0, 1]
```

3. Ecrire une méthode `dico_predecesseurs` qui renvoie un dictionnaire dont les clés sont les sommets, numérotés de 0 à n-1, et les valeurs associées sont les listes des prédécesseurs du sommet.

