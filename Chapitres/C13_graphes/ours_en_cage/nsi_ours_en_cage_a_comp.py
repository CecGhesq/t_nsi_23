#!/usr/bin/env python3
# coding: utf-8
"""
Programme de mise en oeuvre des graphes, recherche en profondeur

Usage:
======
    python nsi_ours_en_cage.py 

 __authors__ = ("Pascal LUCAS", "Professeur NSI")
__contact__ = ("pascal.lucas@ac-lille.fr")
__version__ = "1.0.0"
__copyright__ = "copyleft"
__date__ = "20210326"

"""

from copy import deepcopy
import networkx as nx
import matplotlib.pyplot as plt

#
#from liste_mots import *

LISTE_MOTS_0 = ["aime", "sure", "jure", "brie"]

LISTE_MOTS = ["aime", "auge", "baie", "brie", "bris", "bure", "cage", "cale", "came", "cape",
                "cime", "cire", "cris", "cure", "dame", "dime", "dire", "ducs", "dues", "duos",
                "dure", "durs", "fart", "fors", "gage", "gaie", "gais", "gale", "gare", "gars",
                "gris", "haie", "hale", "hors", "hure", "iris", "juge", "jure", "kart", "laie",
                "lame", "lime", "lire", "loge", "luge", "mage", "maie", "male", "mare", "mari",
                "mars", "mere", "mers", "mime", "mire", "mors", "muet", "mure", "murs", "nage",
                "orge", "ours", "page", "paie", "pale", "pame", "pane", "pape", "pare", "pari",
                "part", "paru", "pere", "pers", "pipe", "pire", "pore", "prie", "pris", "pues",
                "purs", "rage", "raie", "rale", "rame", "rape", "rare", "rime", "rire", "sage",
                "saie", "sale", "sape", "sari", "scie", "sure", "taie", "tale", "tape", "tare",
                "tari", "tige", "toge", "tore", "tors", "tort", "trie", "tris", "troc", "truc"]

LISTE_MOTS_2 = ["aime", "auge", "baie", "brie", "bris", "bure", "cage", "cale", "came", "cape",
                "cime", "cire", "cris", "cure", "dame", "dime", "dire", "ducs", "dues", "duos",
                "dure", "durs", "fart", "fors", "gage", "gaie", "gais", "gale", "gare", "gars",
                "gris", "haie", "hale", "hors", "hure", "iris", "juge", "jure", "kart", "laie",
                "lame", "lime", "lire", "loge", "luge", "mage", "maie", "male", "mare", "mari",
                "mars", "mere", "mers", "mime", "mire", "mors", "muet", "mure", "murs", "nage",
                "orge", "ours", "page", "paie", "pale", "pame", "pane", "pape", "pare", "pari",
                "part", "paru", "pere", "pers", "pipe", "pire", "pore", "prie", "pris", "pues",
                "purs", "rage", "raie", "rale", "rame", "rape", "rare", "rime", "rire", "sage",
                "saie", "sale", "sape", "sari", "scie", "sure", "taie", "tale", "tape", "tare",
                "tari", "tige", "toge", "tore", "tors", "tort", "trie", "tris", "troc", "truc"]
LISTE_MOTS_1 = ["ours", "auge", "baie", "brie", "bris", "bure", "cage", "cale", "came", "cape",
                "gris", "haie", "titi", "hors", "hure", "iris", "juge", "jure", "kart", "laie",
                "lame", "lime", "lire", "loge", "luge", "mage", "maie", "male", "mare", "mari",
                "saie", "sale", "sape", "sari", "scie", "sure", "taie", "tale", "tape", "toto",
                "cime", "cire", "tata", "cure", "dame", "dime", "dire", "ducs", "dues", "duos"]

def affichage_liste_adjacence(liste_adjacence):
    """
    Retourne une chaine de caractères formatée pour une sortie dans une console
    :param: (dict) dictionnaire des mots et des mots voisins
    :return: (str) chaine de caractères formatée mot5 --> mot1, mot2, mot3, etc... a
    :CU
    
#     >>> liste_adjacence = {'mot5': ['mot1', 'mot2', 'mot3'], 'mot100': ['mot55', 'mot12']}
#     >>> affichage_liste_adjacence(liste_adjacence)
#     mot5 --> mot1 mot2 mot3
#     mot100 --> mot55 mot12
    
    """
    chaine=""
    for mot in liste_adjacence:
        chaine= chaine + mot +" --> "
        for mot_voisins in liste_adjacence[mot]:
            chaine = chaine + mot_voisins + " "
        chaine = chaine + '\n'
    return chaine

def graphe(liste_adjacence):
    """
    Imprime le graphe établi à partir de la liste d'adjacence fournie
    :param: (dict of list) dictionnaire des noeuds et des arcs à partir de ces noeuds
    :return: (obj) Objet Digraph
    :CU
    
    """
    # Instanciation du graphe
    graphe = nx.DiGraph()
    for noeud, aretes in liste_adjacence.items():
        graphe.add_node(noeud)
        for arete in aretes:
            graphe.add_edge(noeud, arete)
    return graphe

def distance(mot_1, mot_2):
    '''
    Renvoie le nombre de lettres apparaissant dans le mot_1 qui ne sont pas présente dans le mot_2
    Si une lettre apparaît deux fois dans le mot_1, elle devra apparaitre 2 fois dans le mot_2
    pour qu'il y ait une différence nulle, 'aabc' et 'abcd' ont une différence de 1 lettre
    :param: (str) chaine de caractères
    :return: (int) nombre de lettres du mot_1 n'apparaissant pas dans le mot_2
    :CU: mot_1 et mot_2 sont des chaines de caractères non nulles, de même longueur
    
    >>> distance("abcd", "abcd")
    0
    >>> distance("abcd", "dcba")
    0
    >>> distance("abcd", "1abc")
    1
    >>> distance("abcd", "a12c")
    2
    >>> distance("abcd", "d123")
    3
    >>> distance("abcd", "aaaa")
    3
    >>> distance("abcd", "1234")
    4
    >>> distance("zizi", "ziz1")
    1
    '''
    assert len(mot_1) == len(mot_2) == 4, 'mot de 4 lettres uniquement'
    liste1 = list(mot_1)
    liste2 = list(mot_2)
    distance = 4
    for i in range(len(mot_1)):
        if liste1[0] in liste2 :
            liste2.remove(liste1[0])
            liste1.pop(0)
            distance = distance - 1
        else :
            liste1.pop(0)
    return distance


def liste_mots_voisins(liste_mots, mot_source):
    '''
    Retourne une liste de tous les mots contenus dans la liste passée en paramètre
    dont une lettre différe du mot passé en paramètre 
    :param: (list of str) liste de mots de 4 lettres
    :param: (str) mot de caractères
    :return: (list of str) liste contenant tous les mots de distance =1
    :CU: la liste existe, les mots ont tous une longueur de 4 caractères
    
    >>> liste_mots_voisins(LISTE_MOTS, 'ours')
    ['duos', 'durs', 'fors', 'hors', 'mors', 'murs', 'purs', 'sure', 'tors']
    
    >>> liste_mots_voisins(LISTE_MOTS, 'cire')
    ['brie', 'cime', 'cris', 'cure', 'dire', 'lire', 'mire', 'pire', 'prie', 'raie', 'rime', 'rire', 'scie', 'trie']
    
    >>> liste_mots_voisins(LISTE_MOTS, 'LEGT')
    []

    '''
    liste_dist_1 = []
    for mot in liste_mots :
        if distance(mot, mot_source)== 1:
            liste_dist_1.append(mot)
    return liste_dist_1

def liste_adjacence(liste_mots):
    """
    Retourne le dictionnaire de la liste des mots liés pour chaque mot de la liste de mots
    {'aime': [], 'sure': ['jure'], 'jure': ['sure'], 'brie': []}
    :param: (list of str) liste de mots de 4 lettres
    :return: (dict of list) dictionnaire contenant pour chaque mot la liste des mots voisins
    :CU
    
    >>> LISTE_MOTS = ["aime", "sure", "jure", "brie"]
    >>> liste = liste_adjacence(LISTE_MOTS)
    >>> sorted(liste.items())
    [('aime', []), ('brie', []), ('jure', ['sure']), ('sure', ['jure'])]
    
    """
    dico = {}
    for mot in liste_mots :
        dico[mot] = liste_mots_voisins(liste_mots,mot)
    return dico
    
def dfs(list_adj, mot_source, mot_destination, chemin, dico_visite):
    '''
    :list_adj: (dict of list) dictionnaire contenant pour chaque mot la liste des mots voisins
    : mot_source , mot_destination : (str) mot de caractères
    : chemin : (list)
    :dico_visite : (dict) sommets visités : 
    :affiche(list) chemin parcouru entre le mot_source et mot_voisin
    '''
    if mot_source == mot_destination :
        print(chemin)
    else :
        dico_visite[mot_source]= True
        for voisin in list_adj[mot_source]:
            if voisin not in dico_visite.keys() and len(list_adj[voisin]) != 0:
                dico_visite[voisin] = True
                chemin.append(voisin)
                dfs(list_adj, voisin, mot_destination, chemin, dico_visite)

liste_adj = liste_adjacence(LISTE_MOTS_1)
dfs(liste_adj, 'ours', 'cage', ['ours'], dico_visite ={})
            




# Programme principal
# Tracé du graphe
graphe_mots = graphe(liste_adjacence(LISTE_MOTS_1))
nx.draw(graphe_mots, with_labels = True ,  node_size = 1000 , node_color = 'lightgrey' , arrowsize = 10)

# Affichage à l'aide de matplotlib
plt.show()



if __name__=='__main__':
    import doctest
    doctest.testmod(verbose=False)
