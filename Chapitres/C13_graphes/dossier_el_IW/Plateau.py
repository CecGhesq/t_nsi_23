import copy
class Case :
    def __init__(self,i,j):
        '''constructeur d'une case de coordonnées i,j
        '''
        self.pos = (i,j)
        self.case_finale = False
        self.joueur = False
        self.mur_N = False
        
        pass
    
    def __repr__(self):
        if self.case_finale :
            return 'F'
        elif self.joueur != 0:
            return str(self.joueur)
        else :
            return ' '

class Plateau :
    def __init__(self, largeur, hauteur):
        '''constructeur du plateau de jeu
        '''
        self.largeur = largeur
        self.hauteur = hauteur
        self.grille = [[Case(i,j) for i in range(self.largeur)] for j in range(self.hauteur)] 
        self.sommets = [(i,j) for i in range(0,self.largeur) for j in range(0,self.hauteur)] # tuples de coordonnées pour les sommets du graphe
        self.murs = {pos : [] for pos in self.sommets} #dictionnaire d'adjacence des murs entre les sommets
        self.pos_joueurs = None   # future liste de couples de coordonnées des positions des joueurs
        self.pos_case_finale = None # futur tuple de sa position
        
    def is_case_finale(self,i,j):
        '''prédicat case finale pour une case de coordonnée i,j
        '''
        pass
    
    def add_case_finale(self,i,j):
        '''ajoute la case finale'''
        pass
    
    def suppr_case_finale(self,i,j) :
        pass
        
    def add_mur(self, i,j, direction):
        '''ajoute un mur sur une direction donnée ( attention lien entre les deux sommets non orientés)
        :i,j : coordonnées de la Case
        :direction : (str) : S pour Sud, N pour Nord, E pour est ,  O ouest
        '''
        pass
    
    def add_tous_murs(self,i,j):
        '''ajoute les murs dans toutes les directions
        :i,j : coordonnées de la Case
        '''
        pass
          
    def suppr_mur(self, i,j, direction):
        '''supprime un mur sur une direction donnée ( dans les deux orientations
        :i,j : coordonnées de la Case
        :direction : (str) : S pour Sud, N pour Nord, E pour est ,  O ouest
        '''
        pass
    
    def suppr_tous_murs(self,i,j):
        '''supprime les murs dans toutes les directions
        :i,j : coordonnées de la Case
        '''
        pass
    
    def set_config(self,tup_pos):
        '''remplit la liste pos_joueurs  grâce au tup_pos et remplit la case.joueur par son numero
        :tup_pos: tuple(tuple) tuples de couples de coordonnées
        '''
        pass
        
    
        
    def get_config(self):
        '''renvoie le tuple des positions des coordonnées
        @return tuple(tuple)
        '''
        pass
    
    
    def get_config_joueur(self, num_joueur):
        '''renvoie le tuple des positions des coordonnées
        @return tuple
        '''
        pass
    
    def set_pos_joueur(self,num_Joueur, new_pos):
        '''met à jourla position du joueur dans la liste  pos_joueurs et dans l'état de la Case
        :num_Joueur: (int) indice de postion dans la liste pos_joueurs
        :new_pos: (tuple) coordonnées de la nouvelle position
        Effet de bord : modification liste pos_joueurs;
        '''
        pass
    
    def suppr_config(self):
        '''réinitie le placement des joueurs
        '''
        pass
    
    def dict_glissade(self,i,j):
        '''renvoie le dictionnaire des positions possibles à partir de la case
        de coord i,j dans les quatres directions 
        :i,j: (int) coordonnée case joueur
        @ return (dict) les clés sont les quatre directions, les valeurs = couple de coord
        '''
        dico_glissade = dict()
        liste_pos_Joueurs = copy.deepcopy(self.pos_joueurs)
        liste_pos_Joueurs.remove((i,j))
        position_cf = self.pos_case_finale #tuple de coordonnées de la case finale
        #direction sud
        kj = j
        while self.grille[kj][i].mur_S == False and kj < self.hauteur-1 and (i,kj) not in liste_pos_Joueurs and (i,kj)!=position_cf:
            kj = kj+1
        dico_glissade['S'] =(i, kj)
        
        pass
        return dico_glissade
    
    def dplct_joueur(self, num_Joueur, direction, gagne= False):  # utiliser fonction suivante pour alléger
        '''déplace le joueur selon la direction choisie, met à jour les coordonnées de la position des joueurs
        :num_joueur: (int) numero du joueur
        :direction :(str)
        '''
        i,j = self.get_config_joueur(num_Joueur)
        
        if num_Joueur == 0 :
            #la case finale n'est pas un obstacle
            pos_cf = self.pos_case_finale
            new_pos = self.dict_glissade(i,j)[direction]
            if new_pos == pos_cf:
                gagne = True
            self.set_pos_joueur(num_Joueur, new_pos) # place les nouvelles coordonnées du joueur, 
            #on remet la case finale comme un obstacle
 
        else :
            #la case finale est un obstacle
            # on place les murs autour de la case finale
#             
            pass
        return gagne
            
    def from_file(fichier):  #méthode statique
        '''crée une instance de Plateau à l'aide des informations extraites dans un fichier txt
        :fichier : fichier.txt
        @ renvoie l'instance Plateau avec les dimensions du plateau, la case finale posée et les joueurs avec leurs murs autour d'eux
        '''
        f = open(fichier,"r")
        ### largeur hauteur extraites
        lh= f.readline()
        l , h  = int(lh.split(",")[0]) , int(lh.split(",")[1])
        plateau = Plateau(l,h)
        
        ### case finale
        cf = f.readline()
        i_cf, j_cf = int(cf.split(",")[0]) , int(cf.split(",")[1])
        plateau.add_case_finale(i_cf,j_cf)
        
        ### place joueurs
        nb_joueur= int(f.readline())
        tuple_pos = tuple([tuple([int(ch.strip("\n")) for ch in f.readline().split(",")]) for i in range(nb_joueur)])
        plateau.set_config(tuple_pos)        
        
        ### 
        liste_murs_txt = f.readlines()
        liste_murs = [mur.strip("\n").split(",") for mur in liste_murs_txt] 
        f.close()
        # création des murs 
        for mur in liste_murs :
            plateau.add_mur(int(mur[0]),int(mur[1]), mur[2])
        # murs enceintes
            # murs nord , sud
        for i in range(l):
            plateau.add_mur(i,0,'N')
            plateau.add_mur(i,h-1,'S')
            # murs est ouest
        for j in range(h):
            plateau.add_mur(0,j, 'O')
            plateau.add_mur(l-1, j,'E')
            # murs 
        return plateau
    
    
    def __str__(self):
        '''renvoie l'affichage graphique
        '''
        copie_murs = copy.deepcopy(self.murs) # copie profonde du dictionnaire d'adjacence
               
        # chaine de caractères
        car = ''
        #coin sup_gauch
        car = car + '+'
        #enceinte du  haut
        for i in range(self.largeur) :
            car = car + '-+'
        car = car + '\n'
        #pour chaque ligne
        for j in range(self.hauteur-1):
            # ligne_A
            car = car + '|'#mur ouest de départ
            for i in range(self.largeur) :
                if (i,j) in copie_murs[(i,j)]and self.grille[j][i].mur_E: 
                    if  (i,j) in self.pos_joueurs :
                        car = car +  str(self.pos_joueurs.index((i,j)))
                    elif self.is_case_finale(i,j):
                        car = car + 'F'
                    else :
                        car = car + ' '
                    car = car + '|'
               
                else :
                    if  (i,j) in self.pos_joueurs :
                        car = car + str(self.pos_joueurs.index((i,j))) +' '# place le numéro du joueur
                    elif self.is_case_finale(i,j):
                        car = car + 'F '
                    else :
                        car = car + '  '
                    
            car = car +'\n'
            
            #ligne_B
            car = car + '+'
            for i in range(self.largeur) :
                if (i,j) in copie_murs[(i,j)]and self.grille[j][i].mur_S :
                    car = car + '-'
                
                else :
                    car = car + ' '
                if  self.grille[j][i].mur_E or self.grille[j+1][i].mur_E or self.grille[j][i+1].mur_S:
                    car = car + '+'
                else :
                    car = car + ' '
                                
            car = car +'\n'
        #dernière ligne:
        j = self.hauteur-1 
        car = car + '|'#mur ouest de départ
        for i in range(self.largeur) :
            if (i,j) in copie_murs[(i,j)]and self.grille[j][i].mur_E: 
                if  (i,j) in self.pos_joueurs :
                        car = car +  str(self.pos_joueurs.index((i,j)))
                elif self.is_case_finale(i,j):
                        car = car + 'F'
                else :
                    car = car + ' '
                car = car + '|'
               
            else :
                if  (i,j) in self.pos_joueurs :
                    car = car + str(self.pos_joueurs.index((i,j))) +' '# place le numéro du joueur
                elif self.is_case_finale(i,j):
                    car = car + 'F '
                else :
                    car = car + '  '
                   
        car = car +'\n'
        #coin inf_gauch
        car = car + '+'
        #mur du  bas
        for _ in range(self.largeur) :
            car = car + '-+'
        return car
        
